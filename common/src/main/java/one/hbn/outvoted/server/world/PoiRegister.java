package one.hbn.outvoted.server.world;

import com.google.common.collect.ImmutableSet;
import net.minecraft.core.Registry;
import net.minecraft.world.entity.ai.village.poi.PoiType;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.state.BlockState;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

import java.util.Set;
import java.util.function.Supplier;

public class PoiRegister {

  public static final RegistrationProvider<PoiType> POI = RegistrationProvider.get(
      Registry.POINT_OF_INTEREST_TYPE_REGISTRY, Constants.MOD_ID);
  public static final RegistryObject<PoiType> BURROW = registerPOI("burrow_poi", () -> new PoiType("burrow_poi",
                                                                                                   statesOf(
                                                                                                       BlockRegister.BURROW.get()),
                                                                                                   0, 1));

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod POI for " + Constants.MOD_ID);
  }

  private static <T extends PoiType> RegistryObject<T> registerPOI(String name, Supplier<T> type) {
    //    return Registry.register(Registry.POINT_OF_INTEREST_TYPE, new ResourceLocation(Constants.MOD_ID, name), type);
    RegistryObject<T> poi = POI.register(name, type);
    return poi;
  }

  private static Set<BlockState> statesOf(Block block) {
    return ImmutableSet.copyOf(block.getStateDefinition().getPossibleStates());
  }
}
