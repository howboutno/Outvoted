package one.hbn.outvoted.server.recipe;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.inventory.CraftingContainer;
import net.minecraft.world.item.BannerItem;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.crafting.CustomRecipe;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntityType;
import one.hbn.outvoted.server.item.ItemRegister;

public class WildfireShieldDecorationRecipe extends CustomRecipe {
  public WildfireShieldDecorationRecipe(ResourceLocation arg) {
    super(arg);
  }

  public boolean matches(CraftingContainer container, Level level) {
    ItemStack itemStack = ItemStack.EMPTY;
    ItemStack itemStack2 = ItemStack.EMPTY;

    for (int i = 0; i < container.getContainerSize(); ++i) {
      ItemStack itemStack3 = container.getItem(i);
      if (!itemStack3.isEmpty()) {
        if (itemStack3.getItem() instanceof BannerItem) {
          if (!itemStack2.isEmpty()) {
            return false;
          }

          itemStack2 = itemStack3;
        } else {
          if (!itemStack3.is(ItemRegister.WILDFIRE_SHIELD.get())) {
            return false;
          }

          if (!itemStack.isEmpty()) {
            return false;
          }

          if (BlockItem.getBlockEntityData(itemStack3) != null) {
            return false;
          }

          itemStack = itemStack3;
        }
      }
    }

    return !itemStack.isEmpty() && !itemStack2.isEmpty();
  }

  public ItemStack assemble(CraftingContainer container) {
    ItemStack itemStack = ItemStack.EMPTY;
    ItemStack itemStack2 = ItemStack.EMPTY;

    for (int i = 0; i < container.getContainerSize(); ++i) {
      ItemStack itemStack3 = container.getItem(i);
      if (!itemStack3.isEmpty()) {
        if (itemStack3.getItem() instanceof BannerItem) {
          itemStack = itemStack3;
        } else if (itemStack3.is(ItemRegister.WILDFIRE_SHIELD.get())) {
          itemStack2 = itemStack3.copy();
        }
      }
    }

    if (itemStack2.isEmpty()) {
      return itemStack2;
    } else {
      CompoundTag compoundTag = BlockItem.getBlockEntityData(itemStack);
      CompoundTag compoundTag2 = compoundTag == null ? new CompoundTag() : compoundTag.copy();
      compoundTag2.putInt("Base", ((BannerItem) itemStack.getItem()).getColor().getId());
      BlockItem.setBlockEntityData(itemStack2, BlockEntityType.BANNER, compoundTag2);
      return itemStack2;
    }
  }

  public boolean canCraftInDimensions(int i, int j) {
    return i * j >= 2;
  }

  public RecipeSerializer<?> getSerializer() {
    return RecipeSerializerRegister.SHIELD_DECO.get();
  }
}
