package one.hbn.outvoted.server.entity.ai;

import net.minecraft.world.entity.ai.goal.Goal;
import one.hbn.outvoted.server.entity.animal.Glare;

import java.util.EnumSet;

public class MoveToDarkestSpotGoal extends Goal {
  int ticks;
  private final Glare mob;

  public MoveToDarkestSpotGoal(Glare mob) {
    this.mob = mob;
    this.ticks = this.mob.level.random.nextInt(10);
    this.setFlags(EnumSet.of(Flag.MOVE));
  }

  public boolean canUse() {
    return this.mob.getDarkPos() != null && !this.mob.hasRestriction() && this.mob.blockPosition() != this.mob.getDarkPos() && !this.mob.isLeashed();
  }

  public boolean canContinueToUse() {
    return this.canUse();
  }

  public void start() {
    this.ticks = 0;
    super.start();
  }

  public void stop() {
    this.ticks = 0;
    this.mob.getNavigation().stop();
    this.mob.getNavigation().resetMaxVisitedNodesMultiplier();
  }

  public void tick() {
    if (this.mob.getDarkPos() != null) {
      ++this.ticks;
      if (this.ticks > 600) {
        this.mob.setDarkPos(null);
      } else if (!this.mob.getNavigation().isInProgress()) {
        if (this.mob.isTooFar(this.mob.getDarkPos())) {
          this.mob.setDarkPos(null);
        } else {
          this.mob.startMovingTo(this.mob.getDarkPos());
        }
      }
    }
  }
}
