package one.hbn.outvoted.server.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.StandingSignBlock;
import net.minecraft.world.level.block.WallSignBlock;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.state.BlockState;
import one.hbn.outvoted.server.block.entity.OutvotedSignBlockEntity;

public abstract class OutvotedSignBlock {
  public static class Standing extends StandingSignBlock {
    public Standing(Properties properties, OutvotedWoodType woodType) {
      super(properties, woodType);
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
      return new OutvotedSignBlockEntity(pos, state);
    }
  }

  public static class Wall extends WallSignBlock {
    public Wall(Properties properties, OutvotedWoodType woodType) {
      super(properties, woodType);
    }

    @Override
    public BlockEntity newBlockEntity(BlockPos pos, BlockState state) {
      return new OutvotedSignBlockEntity(pos, state);
    }
  }
}
