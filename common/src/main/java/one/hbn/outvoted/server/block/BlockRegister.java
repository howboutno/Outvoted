package one.hbn.outvoted.server.block;

import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SignItem;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.*;
import net.minecraft.world.level.block.state.BlockBehaviour;
import net.minecraft.world.level.block.state.BlockBehaviour.Properties;
import net.minecraft.world.level.block.state.properties.WoodType;
import net.minecraft.world.level.material.Material;
import net.minecraft.world.level.material.MaterialColor;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.grower.BaobabTreeGrower;
import one.hbn.outvoted.server.block.grower.PalmTreeGrower;
import one.hbn.outvoted.server.item.ItemRegister;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

import java.util.function.Supplier;

public abstract class BlockRegister {
  public static final RegistrationProvider<Block> BLOCKS =
      RegistrationProvider.get(Registry.BLOCK_REGISTRY, Constants.MOD_ID);

  public static final RegistryObject<BurrowBlock> BURROW =
      registerBlock("burrow", () -> new BurrowBlock(Properties.copy(Blocks.SAND)));
  public static final RegistryObject<BlockItem> BURROW_ITEM = registerBlockItem(BURROW);
  public static final RegistryObject<Block> PALM_PLANKS =
      registerBlock("palm_planks", () -> new Block(Properties.copy(Blocks.ACACIA_PLANKS)));
  public static final RegistryObject<BlockItem> PALM_PLANKS_ITEM = registerBlockItem(PALM_PLANKS);
  public static final RegistryObject<StairBlock> PALM_STAIRS = registerBlock("palm_stairs",
      () -> new StairBlock(PALM_PLANKS.get().defaultBlockState(), Properties.copy(Blocks.ACACIA_STAIRS)));
  public static final RegistryObject<BlockItem> PALM_STAIRS_ITEM = registerBlockItem(PALM_STAIRS);
  public static final RegistryObject<RotatedPillarBlock> PALM_LOG = registerBlock("palm_log",
      () -> createLogBlock(MaterialColor.COLOR_ORANGE, MaterialColor.STONE, OutvotedWoodType.PALM));
  public static final RegistryObject<BlockItem> PALM_LOG_ITEM = registerBlockItem(PALM_LOG);
  public static final RegistryObject<LeavesBlock> PALM_LEAVES =
      registerBlock("palm_leaves", () -> new LeavesBlock(Properties.copy(Blocks.ACACIA_LEAVES)));
  public static final RegistryObject<BlockItem> PALM_LEAVES_ITEM = registerBlockItem(PALM_LEAVES);
  public static final RegistryObject<BaseSaplingBlock> PALM_SAPLING = registerBlock("palm_sapling",
      () -> new BaseSaplingBlock(new PalmTreeGrower(), Properties.copy(Blocks.ACACIA_SAPLING),
          Biome.BiomeCategory.DESERT, Blocks.SAND));
  public static final RegistryObject<BlockItem> PALM_SAPLING_ITEM = registerBlockItem(PALM_SAPLING);
  public static final RegistryObject<RotatedPillarBlock> PALM_WOOD =
      registerBlock("palm_wood", () -> new RotatedPillarBlock(Properties.copy(Blocks.ACACIA_WOOD)));
  public static final RegistryObject<BlockItem> PALM_WOOD_ITEM = registerBlockItem(PALM_WOOD);
  public static final RegistryObject<RotatedPillarBlock> STRIPPED_PALM_LOG =
      registerBlock("stripped_palm_log", () -> new RotatedPillarBlock(Properties.copy(Blocks.STRIPPED_ACACIA_LOG)));
  public static final RegistryObject<BlockItem> STRIPPED_PALM_LOG_ITEM = registerBlockItem(STRIPPED_PALM_LOG);
  public static final RegistryObject<RotatedPillarBlock> STRIPPED_PALM_WOOD =
      registerBlock("stripped_palm_wood", () -> new RotatedPillarBlock(Properties.copy(Blocks.STRIPPED_ACACIA_WOOD)));
  public static final RegistryObject<BlockItem> STRIPPED_PALM_WOOD_ITEM = registerBlockItem(STRIPPED_PALM_WOOD);
  public static final RegistryObject<SlabBlock> PALM_SLAB =
      registerBlock("palm_slab", () -> new SlabBlock(Properties.copy(Blocks.ACACIA_SLAB)));
  public static final RegistryObject<BlockItem> PALM_SLAB_ITEM = registerBlockItem(PALM_SLAB);
  public static final RegistryObject<ButtonBlock> PALM_BUTTON =
      registerBlock("palm_button", () -> new WoodButtonBlock(Properties.copy(Blocks.ACACIA_BUTTON)));
  public static final RegistryObject<BlockItem> PALM_BUTTON_ITEM = registerBlockItem(PALM_BUTTON);
  public static final RegistryObject<PressurePlateBlock> PALM_PRESSURE_PLATE = registerBlock("palm_pressure_plate",
      () -> new PressurePlateBlock(PressurePlateBlock.Sensitivity.EVERYTHING,
          Properties.copy(Blocks.ACACIA_FENCE_GATE)));
  public static final RegistryObject<BlockItem> PALM_PRESSURE_PLATE_ITEM = registerBlockItem(PALM_PRESSURE_PLATE);
  public static final RegistryObject<FenceBlock> PALM_FENCE =
      registerBlock("palm_fence", () -> new FenceBlock(Properties.copy(Blocks.ACACIA_FENCE)));
  public static final RegistryObject<BlockItem> PALM_FENCE_ITEM = registerBlockItem(PALM_FENCE);
  public static final RegistryObject<FenceGateBlock> PALM_FENCE_GATE =
      registerBlock("palm_fence_gate", () -> new FenceGateBlock(Properties.copy(Blocks.ACACIA_FENCE_GATE)));
  public static final RegistryObject<BlockItem> PALM_FENCE_GATE_ITEM = registerBlockItem(PALM_FENCE_GATE);
  public static final RegistryObject<TrapDoorBlock> PALM_TRAPDOOR =
      registerBlock("palm_trapdoor", () -> new TrapDoorBlock(Properties.copy(Blocks.ACACIA_TRAPDOOR)));
  public static final RegistryObject<BlockItem> PALM_TRAPDOOR_ITEM = registerBlockItem(PALM_TRAPDOOR);
  public static final RegistryObject<DoorBlock> PALM_DOOR =
      registerBlock("palm_door", () -> new DoorBlock(Properties.copy(Blocks.ACACIA_DOOR)),
          ItemRegister.ITEM_PROPERTIES.stacksTo(16));
  public static final RegistryObject<BlockItem> PALM_DOOR_ITEM = registerBlockItem(PALM_DOOR);
  public static final RegistryObject<OutvotedSignBlock.Standing> PALM_SIGN = registerBlock("palm_sign",
      () -> new OutvotedSignBlock.Standing(Properties.copy(Blocks.ACACIA_SIGN), OutvotedWoodType.PALM),
      ItemRegister.ITEM_PROPERTIES.stacksTo(16));
  public static final RegistryObject<OutvotedSignBlock.Wall> PALM_WALL_SIGN = registerBlock("palm_wall_sign",
      () -> new OutvotedSignBlock.Wall(Properties.copy(Blocks.ACACIA_WALL_SIGN), OutvotedWoodType.PALM),
      ItemRegister.ITEM_PROPERTIES.stacksTo(16));
  public static final RegistryObject<SignItem> PALM_SIGN_ITEM = registerSignItem(PALM_SIGN);
  public static final RegistryObject<RotatedPillarBlock> BAOBAB_LOG = registerBlock("baobab_log",
      () -> createLogBlock(MaterialColor.COLOR_ORANGE, MaterialColor.STONE, OutvotedWoodType.BAOBAB));
  public static final RegistryObject<BlockItem> BAOBAB_LOG_ITEM = registerBlockItem(BAOBAB_LOG);
  public static final RegistryObject<LeavesBlock> BAOBAB_LEAVES =
      registerBlock("baobab_leaves", () -> new LeavesBlock(Properties.copy(Blocks.ACACIA_LEAVES)));
  public static final RegistryObject<BlockItem> BAOBAB_LEAVES_ITEM = registerBlockItem(BAOBAB_LEAVES);
  public static final RegistryObject<BaseSaplingBlock> BAOBAB_SAPLING = registerBlock("baobab_sapling",
      () -> new BaseSaplingBlock(new BaobabTreeGrower(), Properties.copy(Blocks.ACACIA_SAPLING)));
  public static final RegistryObject<BlockItem> BAOBAB_SAPLING_ITEM = registerBlockItem(BAOBAB_SAPLING);
  public static final RegistryObject<RotatedPillarBlock> BAOBAB_WOOD =
      registerBlock("baobab_wood", () -> new RotatedPillarBlock(Properties.copy(Blocks.ACACIA_WOOD)));
  public static final RegistryObject<BlockItem> BAOBAB_WOOD_ITEM = registerBlockItem(BAOBAB_WOOD);
  public static final RegistryObject<RotatedPillarBlock> STRIPPED_BAOBAB_LOG =
      registerBlock("stripped_baobab_log", () -> new RotatedPillarBlock(Properties.copy(Blocks.STRIPPED_ACACIA_LOG)));
  public static final RegistryObject<BlockItem> STRIPPED_BAOBAB_LOG_ITEM = registerBlockItem(STRIPPED_BAOBAB_LOG);
  public static final RegistryObject<RotatedPillarBlock> STRIPPED_BAOBAB_WOOD =
      registerBlock("stripped_baobab_wood", () -> new RotatedPillarBlock(Properties.copy(Blocks.STRIPPED_ACACIA_WOOD)));
  public static final RegistryObject<BlockItem> STRIPPED_BAOBAB_WOOD_ITEM = registerBlockItem(STRIPPED_BAOBAB_WOOD);
  public static final RegistryObject<CopperButtonBlock.Weathering> COPPER_BUTTON = registerBlock("copper_button",
      () -> new CopperButtonBlock.Weathering(WeatheringCopper.WeatherState.UNAFFECTED,
          Properties.copy(Blocks.COPPER_BLOCK).noCollission().strength(0.5F)));
  public static final RegistryObject<BlockItem> COPPER_BUTTON_ITEM = registerBlockItem(COPPER_BUTTON);
  public static final RegistryObject<CopperButtonBlock> WAXED_COPPER_BUTTON = registerBlock("waxed_copper_button",
      () -> new CopperButtonBlock(WeatheringCopper.WeatherState.UNAFFECTED, Properties.copy(COPPER_BUTTON.get())));
  public static final RegistryObject<BlockItem> WAXED_COPPER_BUTTON_ITEM = registerBlockItem(WAXED_COPPER_BUTTON);
  public static final RegistryObject<CopperButtonBlock.Weathering> EXPOSED_COPPER_BUTTON =
      registerBlock("exposed_copper_button",
          () -> new CopperButtonBlock.Weathering(WeatheringCopper.WeatherState.EXPOSED,
              Properties.copy(Blocks.EXPOSED_COPPER).noCollission().strength(0.5F)));
  public static final RegistryObject<BlockItem> EXPOSED_COPPER_BUTTON_ITEM = registerBlockItem(EXPOSED_COPPER_BUTTON);
  public static final RegistryObject<CopperButtonBlock> WAXED_EXPOSED_COPPER_BUTTON =
      registerBlock("waxed_exposed_copper_button", () -> new CopperButtonBlock(WeatheringCopper.WeatherState.EXPOSED,
          Properties.copy(EXPOSED_COPPER_BUTTON.get())));
  public static final RegistryObject<BlockItem> WAXED_EXPOSED_COPPER_BUTTON_ITEM =
      registerBlockItem(WAXED_EXPOSED_COPPER_BUTTON);
  public static final RegistryObject<CopperButtonBlock.Weathering> WEATHERED_COPPER_BUTTON =
      registerBlock("weathered_copper_button",
          () -> new CopperButtonBlock.Weathering(WeatheringCopper.WeatherState.WEATHERED,
              Properties.copy(Blocks.WEATHERED_COPPER).noCollission().strength(0.5F)));
  public static final RegistryObject<BlockItem> WEATHERED_COPPER_BUTTON_ITEM =
      registerBlockItem(WEATHERED_COPPER_BUTTON);
  public static final RegistryObject<CopperButtonBlock> WAXED_WEATHERED_COPPER_BUTTON =
      registerBlock("waxed_weathered_copper_button",
          () -> new CopperButtonBlock(WeatheringCopper.WeatherState.WEATHERED,
              Properties.copy(WEATHERED_COPPER_BUTTON.get())));
  public static final RegistryObject<BlockItem> WAXED_WEATHERED_COPPER_BUTTON_ITEM =
      registerBlockItem(WAXED_WEATHERED_COPPER_BUTTON);
  public static final RegistryObject<CopperButtonBlock.Weathering> OXIDIZED_COPPER_BUTTON =
      registerBlock("oxidized_copper_button",
          () -> new CopperButtonBlock.Weathering(WeatheringCopper.WeatherState.OXIDIZED,
              Properties.copy(Blocks.OXIDIZED_COPPER).noCollission().strength(0.5F)));
  public static final RegistryObject<BlockItem> OXIDIZED_COPPER_BUTTON_ITEM = registerBlockItem(OXIDIZED_COPPER_BUTTON);
  public static final RegistryObject<CopperButtonBlock> WAXED_OXIDIZED_COPPER_BUTTON =
      registerBlock("waxed_oxidized_copper_button", () -> new CopperButtonBlock(WeatheringCopper.WeatherState.OXIDIZED,
          Properties.copy(OXIDIZED_COPPER_BUTTON.get())));
  public static final RegistryObject<BlockItem> WAXED_OXIDIZED_COPPER_BUTTON_ITEM =
      registerBlockItem(WAXED_OXIDIZED_COPPER_BUTTON);

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Blocks for " + Constants.MOD_ID);
  }

  private static RotatedPillarBlock createLogBlock(MaterialColor topColor, MaterialColor barkColor,
      OutvotedWoodType woodType) {
    WoodType.register(woodType);
    return new RotatedPillarBlock(BlockBehaviour.Properties.of(Material.WOOD, (state) -> state.getValue(RotatedPillarBlock.AXIS) == Direction.Axis.Y ? topColor : barkColor).strength(2.0F).sound(SoundType.WOOD));
  }

  private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block) {
    return registerBlock(name, block, ItemRegister.ITEM_PROPERTIES);
  }

  private static <T extends Block> RegistryObject<T> registerBlock(String name, Supplier<T> block,
      Item.Properties properties) {
    return BLOCKS.register(name, block);
  }

  private static <T extends Block> RegistryObject<BlockItem> registerBlockItem(RegistryObject<T> block) {
    return registerBlockItem(block, ItemRegister.ITEM_PROPERTIES);
  }

  private static <T extends Block> RegistryObject<BlockItem> registerBlockItem(RegistryObject<T> block,
      Item.Properties properties) {
    return ItemRegister.ITEMS.register(block.getId().getPath(), () -> new BlockItem(block.get(), properties));
  }

  //  private static <T extends SignBlock> RegistryObject<SignItem> registerSignItem(RegistryObject<T> standing, RegistryObject<T> wall) {
  //    return ModItems.ITEMS.register(standing.getId().getPath(), () -> new SignItem(ModItems.ITEM_PROPERTIES.stacksTo(16), standing.get(), wall.get()));
  //  }

  private static <T extends SignBlock> RegistryObject<SignItem> registerSignItem(RegistryObject<T> block) {
    return ItemRegister.ITEMS.register(block.getId().getPath(),
        () -> new SignItem(ItemRegister.ITEM_PROPERTIES.durability(0).stacksTo(16), PALM_SIGN.get(),
            PALM_WALL_SIGN.get()));
  }
}
