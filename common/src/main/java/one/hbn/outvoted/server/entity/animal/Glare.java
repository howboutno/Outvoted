package one.hbn.outvoted.server.entity.animal;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.sounds.SoundSource;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Mth;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.FlyingMoveControl;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.TemptGoal;
import net.minecraft.world.entity.ai.navigation.FlyingPathNavigation;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.entity.ai.util.AirRandomPos;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.Vec3;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.EntityUtil;
import one.hbn.outvoted.server.entity.ai.*;

import java.util.Random;
import java.util.function.Predicate;

public class Glare extends PathfinderMob {
  public static final int maxLight = 7;
  public static final Predicate<ItemEntity> PICKABLE_DROP_FILTER;
  private static final EntityDataAccessor<Boolean> ANGRY = SynchedEntityData.defineId(Glare.class,
                                                                                      EntityDataSerializers.BOOLEAN);

  static {
    PICKABLE_DROP_FILTER = (item) -> {
      if (item.getItem().getItem() instanceof BlockItem) {
        BlockState state = ((BlockItem) item.getItem().getItem()).getBlock().defaultBlockState();
        return !item.hasPickUpDelay() && item.isAlive() && state.getLightEmission() > maxLight;
      }
      return false;
    };
  }

  protected BlockPos darkPos;

  public Glare(EntityType<? extends Glare> type, Level worldIn) {
    super(type, worldIn);
    this.moveControl = new FlyingMoveControl(this, 20, true);
    this.setPathfindingMalus(BlockPathTypes.DANGER_FIRE, -1.0F);
    this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F);
    this.setPathfindingMalus(BlockPathTypes.WATER_BORDER, 16.0F);
    this.setPathfindingMalus(BlockPathTypes.COCOA, -1.0F);
    this.setPathfindingMalus(BlockPathTypes.FENCE, -1.0F);
    OutvotedConfig.Glare config = OutvotedConfig.COMMON.MOBS.GLARE;
    this.setCanPickUpLoot(config.shouldInteract.get());
    EntityUtil.setHealthFromConfig(this, config);
  }

  public static AttributeSupplier.Builder setCustomAttributes() {
    return Mob.createMobAttributes().add(Attributes.FLYING_SPEED, 0.4000000238418579D)
        .add(Attributes.MOVEMENT_SPEED, 0.30000001192092896D).add(Attributes.FOLLOW_RANGE, 48.0D);
  }

  public static boolean canSpawn(EntityType<Glare> entity, LevelAccessor world, MobSpawnType spawnReason,
                                 BlockPos blockPos, Random random) {
    return !world.canSeeSky(blockPos) && isSuitableSpawn(world, blockPos,
                                                         random) && blockPos.getY() < 63 && checkMobSpawnRules(entity,
                                                                                                               world,
                                                                                                               spawnReason,
                                                                                                               blockPos,
                                                                                                               random);
  }

  private static boolean isSuitableSpawn(LevelAccessor world, BlockPos pos, Random random) {
    int mult = 5;
    for (BlockPos blockPos : BlockPos.withinManhattan(pos, mult, mult, mult)) {
      BlockState state = world.getBlockState(blockPos);
      if (state.getFluidState().is(FluidTags.LAVA)) {
        return false;
      }
    }
    return true;
  }

  public float getWalkTargetValue(BlockPos pos, LevelReader world) {
    return world.isEmptyBlock(pos) ? 10.0F : 0.0F;
  }

  protected void registerGoals() {
    this.goalSelector.addGoal(0, new TemptGoal(this, 1.5, Ingredient.of(Items.GLOW_BERRIES), false));
    if (OutvotedConfig.COMMON.MOBS.GLARE.shouldInteract.get()) {
      this.goalSelector.addGoal(1, new PickupItemGoal(this));
      this.goalSelector.addGoal(2, new InDarkSpotGoal(this));
    }
    this.goalSelector.addGoal(3, new MoveToDarkestSpotGoal(this));
    this.goalSelector.addGoal(4, new FindDarkestNearbySpotGoal(this));
    this.goalSelector.addGoal(5, new RandomHoverGoal(this));
    this.goalSelector.addGoal(6, new FloatGoal(this));
  }

  protected PathNavigation createNavigation(Level world) {
    FlyingPathNavigation birdNavigation = new FlyingPathNavigation(this, world) {
      public boolean isStableDestination(BlockPos pos) {
        return !this.level.isEmptyBlock(pos.below());
      }
    };
    birdNavigation.setCanOpenDoors(false);
    birdNavigation.setCanFloat(false);
    birdNavigation.setCanPassDoors(true);
    return birdNavigation;
  }

  @Override
  protected void defineSynchedData() {
    super.defineSynchedData();
    this.entityData.define(ANGRY, Boolean.FALSE);
  }

  @Override
  public void tick() {
    super.tick();
    this.setNoGravity(true);
  }

  @Override
  public void addAdditionalSaveData(CompoundTag compound) {
    super.addAdditionalSaveData(compound);
    compound.putBoolean("IsAngry", this.isAngry());
  }

  @Override
  public void readAdditionalSaveData(CompoundTag compound) {
    super.readAdditionalSaveData(compound);
    this.setAngry(compound.getBoolean("IsAngry"));
  }

  @Override
  public void aiStep() {
    super.aiStep();
    if (!this.level.isClientSide) {
      this.setAngry(this.getLight(this.blockPosition()) <= maxLight);
    }
    if (this.isAngry() && this.tickCount % 20 == 0)
      this.level.addParticle(ParticleTypes.ANGRY_VILLAGER, this.getRandomX(0.75D),
                             this.getY() + this.random.nextDouble(), this.getRandomZ(0.75D), this.random.nextDouble(),
                             this.random.nextDouble(), this.random.nextDouble());
    if (this.isAngry() && this.tickCount % 15 == 0)
      this.level.playSound(null, this, SoundEvents.AZALEA_LEAVES_FALL, SoundSource.NEUTRAL, 0.6F,
                           Mth.lerp(this.random.nextFloat(), 0.25F, 0.75F));
  }

  protected void pickUpItem(ItemEntity item) {
    ItemStack itemStack = item.getItem();
    if (this.canHoldItem(itemStack) && PICKABLE_DROP_FILTER.test(item)) {
      int i = itemStack.getCount();
      int inventorySize = OutvotedConfig.COMMON.MOBS.GLARE.inventorySize.get();
      if (i > inventorySize) {
        this.dropItem(itemStack.split(i - inventorySize));
      }

      this.onItemPickup(item);
      this.setItemSlot(EquipmentSlot.MAINHAND, itemStack.split(inventorySize));
      this.handDropChances[EquipmentSlot.MAINHAND.getIndex()] = 2.0F;
      this.take(item, itemStack.getCount());
      item.discard();
    }
  }

  public boolean canHoldItem(ItemStack stack) {
    return this.getItemBySlot(EquipmentSlot.MAINHAND).isEmpty();
  }

  public boolean canTakeItem(ItemStack stack) {
    EquipmentSlot equipmentSlot = Mob.getEquipmentSlotForItem(stack);
    if (!this.getItemBySlot(equipmentSlot).isEmpty()) {
      return false;
    } else {
      return equipmentSlot == EquipmentSlot.MAINHAND && super.canTakeItem(stack);
    }
  }

  private void dropItem(ItemStack stack) {
    ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), stack);
    this.level.addFreshEntity(itemEntity);
  }

  public int getLight(BlockPos pos) {
    return this.level.getRawBrightness(pos, this.level.getSkyDarken());
  }

  public boolean isAngry() {
    return this.entityData.get(ANGRY);
  }

  public void setAngry(boolean angry) {
    this.entityData.set(ANGRY, angry);
  }

  protected void checkFallDamage(double heightDifference, boolean onGround, BlockState landedState,
                                 BlockPos landedPosition) {
  }

  public boolean causeFallDamage(float fallDistance, float damageMultiplier, DamageSource damageSource) {
    return false;
  }

  @Override
  protected float getStandingEyeHeight(Pose pose, EntityDimensions dimensions) {
    return 0.7F;
  }

  public void startMovingTo(BlockPos pos) {
    Vec3 vec3d = Vec3.atBottomCenterOf(pos);
    int i = 0;
    BlockPos blockPos = this.blockPosition();
    int j = (int) vec3d.y - blockPos.getY();
    if (j > 2) {
      i = 4;
    } else if (j < -2) {
      i = -4;
    }

    int k = 6;
    int l = 8;
    int m = blockPos.distManhattan(pos);
    if (m < 15) {
      k = m / 2;
      l = m / 2;
    }

    Vec3 vec3d2 = AirRandomPos.getPosTowards(this, k, l, i, vec3d, 0.3141592741012573D);
    if (vec3d2 != null) {
      this.navigation.setMaxVisitedNodesMultiplier(0.5F);
      this.navigation.moveTo(vec3d2.x, vec3d2.y, vec3d2.z, 1.0D);
    }
  }

  public boolean hasDarkPos() {
    return this.darkPos != null;
  }

  //    boolean isGap(BlockPos pos) {
  //        boolean top = this.world.getBlockState(pos.up()).isAir();
  //        boolean bottom = this.world.getBlockState(pos.down()).isAir();
  //        return this.world.getBlockState(pos).isAir() && (top || bottom);
  //    }
  //
  //    BlockPos modifyPos(BlockPos pos) {
  //        if (isGap(pos)) {
  //            if (this.world.getBlockState(pos.up()).isAir()) {
  //                return pos.up();
  //            }
  //        }
  //        return pos;
  //    }

  public BlockPos getDarkPos() {
    return this.darkPos;
  }

  public void setDarkPos(BlockPos pos) {
    this.darkPos = pos;
  }

  public boolean isDarkSpot(BlockPos pos) {
    return this.getLight(pos) <= maxLight && this.level.isEmptyBlock(pos);
  }

  public boolean isTooFar(BlockPos pos) {
    return !this.isWithinDistance(pos, 32);
  }

  boolean isWithinDistance(BlockPos pos, int distance) {
    return isWithinDistance(pos, (double) distance);
  }

  boolean isWithinDistance(BlockPos pos, double distance) {
    return pos.closerThan(this.blockPosition(), distance);
  }

  //  private final AnimationFactory factory = new AnimationFactory(this);
  //
  //  private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
  //    GeckoLibCache.getInstance().parser.setValue("mult", this.isAngry() ? 0.6F : 0.2F);
  //    event.getController().setAnimation(new AnimationBuilder().addAnimation("living", true));
  //
  //    return PlayState.CONTINUE;
  //  }
  //
  //  @Override
  //  public void registerControllers(AnimationData data) {
  //    AnimationController<Glare> controller = new AnimationController<>(this, "controller", 2, this::predicate);
  //    data.addAnimationController(controller);
  //  }
  //
  //  @Override
  //  public AnimationFactory getFactory() {
  //    return this.factory;
  //  }
}
