package one.hbn.outvoted.server.block;

import com.google.common.base.Suppliers;
import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.ChangeOverTimeBlock;
import net.minecraft.world.level.block.WeatheringCopper;
import net.minecraft.world.level.block.state.BlockState;

import java.util.Optional;
import java.util.function.Supplier;

public interface WeatheringCopperButton extends ChangeOverTimeBlock<WeatheringCopper.WeatherState> {
  Supplier<BiMap<Block, Block>> NEXT_BY_BLOCK = Suppliers.memoize(
      () -> ImmutableBiMap.<Block, Block>builder().putAll(WeatheringCopper.NEXT_BY_BLOCK.get())
          .put(BlockRegister.COPPER_BUTTON.get(), BlockRegister.EXPOSED_COPPER_BUTTON.get())
          .put(BlockRegister.EXPOSED_COPPER_BUTTON.get(), BlockRegister.WEATHERED_COPPER_BUTTON.get())
          .put(BlockRegister.WEATHERED_COPPER_BUTTON.get(), BlockRegister.OXIDIZED_COPPER_BUTTON.get()).build());
  Supplier<BiMap<Block, Block>> PREVIOUS_BY_BLOCK = Suppliers.memoize(() -> ((BiMap) NEXT_BY_BLOCK.get()).inverse());

  static Optional<BlockState> getPrevious(BlockState state) {
    return getPrevious(state.getBlock()).map(block -> block.withPropertiesOf(state));
  }

  static Optional<Block> getPrevious(Block block) {
    return Optional.ofNullable((Block) ((BiMap) PREVIOUS_BY_BLOCK.get()).get(block));
  }

  static BlockState getFirst(BlockState state) {
    return getFirst(state.getBlock()).withPropertiesOf(state);
  }

  static Block getFirst(Block block) {
    Block block2 = block;

    for (Block block3 = (Block) ((BiMap) PREVIOUS_BY_BLOCK.get()).get(
        block); block3 != null; block3 = (Block) ((BiMap) PREVIOUS_BY_BLOCK.get()).get(block3)) {
      block2 = block3;
    }

    return block2;
  }

  @Override
  default Optional<BlockState> getNext(BlockState state) {
    return getNext(state.getBlock()).map(block -> block.withPropertiesOf(state));
  }

  static Optional<Block> getNext(Block block) {
    return Optional.ofNullable((Block) ((BiMap) NEXT_BY_BLOCK.get()).get(block));
  }

  @Override
  default float getChanceModifier() {
    return this.getAge() == WeatheringCopper.WeatherState.UNAFFECTED ? 0.75F : 1.0F;
  }
}
