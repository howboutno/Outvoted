package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.village.poi.PoiManager;
import net.minecraft.world.entity.ai.village.poi.PoiRecord;
import one.hbn.outvoted.server.entity.animal.Meerkat;
import one.hbn.outvoted.server.world.PoiRegister;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class FindBurrowGoal extends Goal {
  protected final Meerkat mob;
  private int ticksLeftToFindBurrow;

  public FindBurrowGoal(Meerkat mob) {
    this.mob = mob;
    this.ticksLeftToFindBurrow = 200;
  }

  public boolean canUse() {
    if (this.ticksLeftToFindBurrow > 0) {
      --this.ticksLeftToFindBurrow;
    } else {
      this.ticksLeftToFindBurrow = 200;
    }
    return this.ticksLeftToFindBurrow == 0 && !this.mob.hasBurrow() && this.mob.canEnterBurrow();
  }

  public void start() {
    this.ticksLeftToFindBurrow = 200;
    List<BlockPos> list = this.getNearbyFreeBurrows();
    if (!list.isEmpty()) {
      Iterator<BlockPos> var2 = list.iterator();

      BlockPos blockPos;
      do {
        if (!var2.hasNext()) {
          this.mob.getMoveToBurrowGoal().clearPossibleBurrows();
          this.mob.setBurrowPos(list.get(0));
          return;
        }

        blockPos = var2.next();
      } while (this.mob.getMoveToBurrowGoal().isPossibleBurrow(blockPos));

      this.mob.setBurrowPos(blockPos);
    }
  }

  private List<BlockPos> getNearbyFreeBurrows() {
    BlockPos blockPos = this.mob.blockPosition();
    PoiManager pointOfInterestStorage = ((ServerLevel) this.mob.level).getPoiManager();
    Stream<PoiRecord> stream = pointOfInterestStorage.getInRange(
        (pointOfInterestType) -> pointOfInterestType == PoiRegister.BURROW, blockPos, 20, PoiManager.Occupancy.ANY);
    return stream.map(PoiRecord::getPos).filter(this.mob::doesBurrowHaveSpace)
        .sorted(Comparator.comparingDouble((blockPos2) -> blockPos2.distSqr(blockPos))).collect(Collectors.toList());
  }

  public int getTicksLeftToFindBurrow() {
    return this.ticksLeftToFindBurrow;
  }
}
