package one.hbn.outvoted.server.block.entity;

import net.minecraft.Util;
import net.minecraft.core.Registry;
import net.minecraft.util.datafix.fixes.References;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.block.entity.BlockEntityType;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

import java.util.function.Supplier;

public class BlockEntityRegister {
  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Block Entities for " + Constants.MOD_ID);
  }

  public static final RegistrationProvider<BlockEntityType<?>> BLOCK_ENTITIES =
      RegistrationProvider.get(Registry.BLOCK_ENTITY_TYPE_REGISTRY, Constants.MOD_ID);

  public static final RegistryObject<BlockEntityType<OutvotedSignBlockEntity>> SIGN = registerBlockEntity("sign",
      () -> BlockEntityType.Builder.of(OutvotedSignBlockEntity::new, BlockRegister.PALM_SIGN.get(),
          BlockRegister.PALM_WALL_SIGN.get()).build(Util.fetchChoiceType(References.BLOCK_ENTITY, "sign")));


  private static <T extends BlockEntity> RegistryObject<BlockEntityType<T>> registerBlockEntity(String name,
      Supplier<BlockEntityType<T>> blockEntityType) {
    //    return Registry.register(Registry.BLOCK_ENTITY_TYPE, new ResourceLocation(Constants.MOD_ID, name), blockEntityType);
    return BLOCK_ENTITIES.register(name, blockEntityType);
  }

  public static final RegistryObject<BlockEntityType<BurrowBlockEntity>> BURROW = registerBlockEntity("burrow",
      () -> BlockEntityType.Builder.of(BurrowBlockEntity::new, BlockRegister.BURROW.get()).build(
          Util.fetchChoiceType(References.BLOCK_ENTITY, "burrow")));
}
