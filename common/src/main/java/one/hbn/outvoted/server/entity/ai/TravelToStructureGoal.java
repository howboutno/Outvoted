package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.level.levelgen.Heightmap;
import one.hbn.outvoted.server.entity.animal.Meerkat;

import java.util.EnumSet;

public class TravelToStructureGoal extends Goal {
  public final double speed;
  protected final Meerkat mob;
  protected BlockPos targetPos;
  private boolean reached = false;

  public TravelToStructureGoal(Meerkat mob, double speed) {
    this.mob = mob;
    this.speed = speed;
    this.setFlags(EnumSet.of(Flag.MOVE, Flag.JUMP, Flag.LOOK));
  }

  public boolean canUse() {
    return this.mob.isTrusting() && this.mob.getStructurePos() != null;
  }

  public boolean canContinueToUse() {
    return super.canContinueToUse() && !this.reached;
  }

  public void start() {
    this.targetPos = this.mob.level.getHeightmapPos(Heightmap.Types.WORLD_SURFACE, this.mob.getStructurePos());
    this.startMovingToTarget();
  }

  public void stop() {
    this.mob.resetStructurePos();
    this.mob.setTrusted(null);
  }

  public void tick() {
    if (this.mob.hasTrusted()) {
      Player trusted = this.mob.level.getPlayerByUUID(this.mob.getTrusted());
      if (this.mob.distanceToSqr(trusted) > 64) {
        this.mob.getNavigation().stop();
        this.mob.getLookControl().setLookAt(trusted.getX(), trusted.getEyeY(), trusted.getZ());
      } else if (this.mob.getNavigation().isDone() || this.mob.getNavigation().getPath() == null) {
        this.startMovingToTarget();
      }
      if (this.mob.distanceToSqr(this.mob.getStructurePos().getX(), this.mob.getY(),
                                 this.mob.getStructurePos().getZ()) <= 100)
        this.reached = true;
    }
  }

  protected void startMovingToTarget() {
    this.mob.getNavigation().moveTo((double) ((float) this.targetPos.getX()) + 0.5D, this.targetPos.getY(),
                                    (double) ((float) this.targetPos.getZ()) + 0.5D, this.speed);
  }
}
