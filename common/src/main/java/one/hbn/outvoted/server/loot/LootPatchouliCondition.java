package one.hbn.outvoted.server.loot;

import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonObject;
import com.google.gson.JsonSerializationContext;
import net.minecraft.util.GsonHelper;
import net.minecraft.world.level.storage.loot.LootContext;
import net.minecraft.world.level.storage.loot.predicates.LootItemCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;
import one.hbn.outvoted.server.config.OutvotedConfig;

public class LootPatchouliCondition implements LootItemCondition {
  final Boolean patchouli;

  public LootPatchouliCondition(Boolean b) {
    this.patchouli = b;
  }

  public static LootItemCondition.Builder patchouli() {
    return patchouli(true);
  }

  public static LootItemCondition.Builder patchouli(Boolean prop) {
    return () -> new LootPatchouliCondition(prop);
  }

  public LootItemConditionType getType() {
    return LootConditionRegister.CONFIG_PATCHOULI.get();
  }

  public boolean test(LootContext lootContext) {
    return this.patchouli == null || this.patchouli == OutvotedConfig.COMMON.MISC.giveBook.get();
  }

  public static class Serializer implements net.minecraft.world.level.storage.loot.Serializer<LootPatchouliCondition> {
    public void serialize(JsonObject jsonObject, LootPatchouliCondition variantCheckLootCondition,
                          JsonSerializationContext jsonSerializationContext) {
      jsonObject.add("patchouli", jsonSerializationContext.serialize(variantCheckLootCondition.patchouli));
    }

    public LootPatchouliCondition deserialize(JsonObject jsonObject,
                                              JsonDeserializationContext jsonDeserializationContext) {
      return new LootPatchouliCondition(
          jsonObject.has("patchouli") ? GsonHelper.getAsBoolean(jsonObject, "patchouli") : null);
    }
  }
}
