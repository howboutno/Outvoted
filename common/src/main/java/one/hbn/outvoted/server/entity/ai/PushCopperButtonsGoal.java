package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.MoveToBlockGoal;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.state.BlockState;
import one.hbn.outvoted.server.block.CopperButtonBlock;
import one.hbn.outvoted.server.entity.animal.CopperGolem;

public class PushCopperButtonsGoal extends MoveToBlockGoal {
  public PushCopperButtonsGoal(PathfinderMob pathAwareEntity, double d, int i) {
    super(pathAwareEntity, d, i);
  }

  public double getDesiredSquaredDistanceToTarget() {
    return 0.75D;
  }

  public boolean canUse() {
    return ((CopperGolem) this.mob).isNotFrozen() && super.canUse();
  }

  protected int nextStartTick(PathfinderMob mob) {
    return 100 + mob.getRandom().nextInt(200);
  }

  public boolean canContinueToUse() {
    return ((CopperGolem) this.mob).isNotFrozen() && super.canContinueToUse() && this.nextStartTick == 0;
  }

  @Override
  protected BlockPos getMoveToTarget() {
    return this.blockPos;
  }

  public void tick() {
    super.tick();
    if (this.isReachedTarget()) {
      ((CopperGolem) this.mob).setPushingState(true);
      this.nextStartTick = this.nextStartTick(this.mob);
      BlockState state = this.mob.level.getBlockState(this.blockPos);
      ((ButtonBlock) state.getBlock()).use(state, this.mob.level, this.blockPos, null, null, null);
    }
  }

  @Override
  protected boolean isValidTarget(LevelReader world, BlockPos pos) {
    BlockState blockState = world.getBlockState(pos);
    return blockState.getBlock() instanceof CopperButtonBlock && pos != BlockPos.ZERO;
  }
}
