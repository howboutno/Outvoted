package one.hbn.outvoted.server.entity;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.attributes.AttributeInstance;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import one.hbn.outvoted.server.config.OutvotedConfig;

public class EntityUtil {
  public static void setHealthFromConfig(LivingEntity entity, OutvotedConfig.Mob mobConfig) {
    double value = mobConfig.spawnConfig.maxHealth.get();
    if (value <= 0)
      return;

    AttributeInstance maxHealthAttr = entity.getAttribute(Attributes.MAX_HEALTH);
    if (maxHealthAttr != null) {
      double difference = value - maxHealthAttr.getBaseValue();
      maxHealthAttr.addTransientModifier(
          new AttributeModifier("Config Max Health", difference, AttributeModifier.Operation.ADDITION));
      entity.setHealth(entity.getMaxHealth());
    }
  }

  public interface IBlazeVariant {
    void outvoted$setVariant(int type);

    int outvoted$getVariant();
  }
}
