package one.hbn.outvoted.server.entity;

import net.minecraft.core.Registry;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.MobCategory;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.entity.animal.CopperGolem;
import one.hbn.outvoted.server.entity.animal.Glare;
import one.hbn.outvoted.server.entity.animal.Meerkat;
import one.hbn.outvoted.server.entity.animal.Ostrich;
import one.hbn.outvoted.server.entity.monster.Barnacle;
import one.hbn.outvoted.server.entity.monster.Glutton;
import one.hbn.outvoted.server.entity.monster.Wildfire;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

import java.util.function.Supplier;

public abstract class EntityRegister {
  public static final RegistrationProvider<EntityType<?>> ENTITIES =
      RegistrationProvider.get(Registry.ENTITY_TYPE_REGISTRY, Constants.MOD_ID);
  public static final RegistryObject<EntityType<Wildfire>> WILDFIRE = registerEntity("wildfire",
      () -> EntityType.Builder.of(Wildfire::new, MobCategory.MONSTER).fireImmune().sized(0.8F, 2.0F).build(
          Constants.loc("wildfire").toString()));
  public static final RegistryObject<EntityType<Glutton>> GLUTTON = registerEntity("glutton",
      () -> EntityType.Builder.of(Glutton::new, MobCategory.CREATURE).sized(1.0F, 1.2F).build(
          Constants.loc("glutton").toString()));
  public static final RegistryObject<EntityType<Barnacle>> BARNACLE = registerEntity("barnacle",
      () -> EntityType.Builder.of(Barnacle::new, MobCategory.MONSTER).sized(0.5F, 0.5F).build(
          Constants.loc("barnacle").toString()));
  public static final RegistryObject<EntityType<Meerkat>> MEERKAT = registerEntity("meerkat",
      () -> EntityType.Builder.of(Meerkat::new, MobCategory.CREATURE).sized(0.9F, 1.4F).build(
          Constants.loc("meerkat").toString()));
  public static final RegistryObject<EntityType<Ostrich>> OSTRICH = registerEntity("ostrich",
      () -> EntityType.Builder.of(Ostrich::new, MobCategory.CREATURE).sized(1.0F, 1.7F).build(
          Constants.loc("ostrich").toString()));
  public static final RegistryObject<EntityType<Glare>> GLARE = registerEntity("glare",
      () -> EntityType.Builder.of(Glare::new, MobCategory.CREATURE).sized(0.8F, 0.95F).build(
          Constants.loc("glare").toString()));
  public static final RegistryObject<EntityType<CopperGolem>> COPPER_GOLEM = registerEntity("copper_golem",
      () -> EntityType.Builder.of(CopperGolem::new, MobCategory.MISC).sized(0.9F, 1.2F).build(
          Constants.loc("copper_golem").toString()));

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Entities for " + Constants.MOD_ID);
  }

  private static <T extends EntityType<?>> RegistryObject<T> registerEntity(String name, Supplier<T> entityType) {
    return ENTITIES.register(name, entityType);
  }
}
