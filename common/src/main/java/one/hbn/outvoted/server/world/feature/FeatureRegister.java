package one.hbn.outvoted.server.world.feature;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Registry;
import net.minecraft.data.worldgen.placement.PlacementUtils;
import net.minecraft.util.valueproviders.ConstantInt;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.levelgen.blockpredicates.BlockPredicate;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import net.minecraft.world.level.levelgen.feature.Feature;
import net.minecraft.world.level.levelgen.feature.configurations.BlockColumnConfiguration;
import net.minecraft.world.level.levelgen.feature.configurations.TreeConfiguration;
import net.minecraft.world.level.levelgen.feature.featuresize.TwoLayersFeatureSize;
import net.minecraft.world.level.levelgen.feature.foliageplacers.AcaciaFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.foliageplacers.RandomSpreadFoliagePlacer;
import net.minecraft.world.level.levelgen.feature.stateproviders.BlockStateProvider;
import net.minecraft.world.level.levelgen.feature.trunkplacers.ForkingTrunkPlacer;
import net.minecraft.world.level.levelgen.feature.trunkplacers.StraightTrunkPlacer;
import net.minecraft.world.level.levelgen.placement.BiomeFilter;
import net.minecraft.world.level.levelgen.placement.InSquarePlacement;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

import java.util.List;
import java.util.function.Supplier;

public class FeatureRegister {
  public static final RegistrationProvider<ConfiguredFeature<?, ?>> CONFIGURED_FEATURES = RegistrationProvider.get(
      Registry.CONFIGURED_FEATURE_REGISTRY, Constants.MOD_ID);
  public static final RegistryObject<ConfiguredFeature<?, ?>> CONFIGURED_BURROW = registerConfiguredFeature("burrow",
                                                                                                            () -> new ConfiguredFeature<>(
                                                                                                                Feature.BLOCK_COLUMN,
                                                                                                                new BlockColumnConfiguration(
                                                                                                                    List.of(
                                                                                                                        BlockColumnConfiguration.layer(
                                                                                                                            ConstantInt.of(
                                                                                                                                1),
                                                                                                                            BlockStateProvider.simple(
                                                                                                                                BlockRegister.BURROW.get()))),
                                                                                                                    Direction.DOWN,
                                                                                                                    BlockPredicate.matchesBlock(
                                                                                                                        Blocks.AIR,
                                                                                                                        BlockPos.ZERO),
                                                                                                                    false)));
  public static final RegistryObject<ConfiguredFeature<?, ?>> CONFIGURED_BAOBAB = registerConfiguredFeature("baobab",
                                                                                                            () -> new ConfiguredFeature<>(
                                                                                                                Feature.TREE,
                                                                                                                (new TreeConfiguration.TreeConfigurationBuilder(
                                                                                                                    BlockStateProvider.simple(
                                                                                                                        BlockRegister.BAOBAB_LOG.get()),
                                                                                                                    new StraightTrunkPlacer(
                                                                                                                        4,
                                                                                                                        2,
                                                                                                                        0),
                                                                                                                    BlockStateProvider.simple(
                                                                                                                        BlockRegister.BAOBAB_LEAVES.get()),
                                                                                                                    new RandomSpreadFoliagePlacer(
                                                                                                                        ConstantInt.of(
                                                                                                                            3),
                                                                                                                        ConstantInt.of(
                                                                                                                            0),
                                                                                                                        ConstantInt.of(
                                                                                                                            2),
                                                                                                                        70),
                                                                                                                    new TwoLayersFeatureSize(
                                                                                                                        0,
                                                                                                                        0,
                                                                                                                        0))).build()));
  public static final RegistryObject<ConfiguredFeature<?, ?>> CONFIGURED_PALM = registerConfiguredFeature("palm",
                                                                                                          () -> new ConfiguredFeature<>(
                                                                                                              Feature.TREE,
                                                                                                              (new TreeConfiguration.TreeConfigurationBuilder(
                                                                                                                  BlockStateProvider.simple(
                                                                                                                      BlockRegister.PALM_LOG.get()),
                                                                                                                  new ForkingTrunkPlacer(
                                                                                                                      5,
                                                                                                                      2,
                                                                                                                      2),
                                                                                                                  BlockStateProvider.simple(
                                                                                                                      BlockRegister.PALM_LEAVES.get()),
                                                                                                                  new AcaciaFoliagePlacer(
                                                                                                                      ConstantInt.of(
                                                                                                                          2),
                                                                                                                      ConstantInt.of(
                                                                                                                          0)),
                                                                                                                  new TwoLayersFeatureSize(
                                                                                                                      1,
                                                                                                                      0,
                                                                                                                      2))).ignoreVines()
                                                                                                                  .build()));
  public static final RegistrationProvider<PlacedFeature> PLACED_FEATURES = RegistrationProvider.get(
      Registry.PLACED_FEATURE_REGISTRY, Constants.MOD_ID);
  // A PlacedFeature is a configured feature and a list of placement modifiers.
  // This is what goes into biomes, the placement modifiers determine where and how often to
  // place the configured feature.
  // Each placed feature in any of the biomes in a chunk generates in that chunk when the chunk generates.
  // When a feature generates in a chunk, the chunk's local 0,0,0 origin coordinate is given to the
  // list of placement modifiers (if any).
  // Each placement modifier converts the input position to zero or more output positions, each of which
  // is given to the next placement modifier.
  // The ConfiguredFeature is generated at the positions generated after all placement modifiers have run.
  public static final RegistryObject<PlacedFeature> PLACED_BURROW = registerPlacedFeature("burrow",
                                                                                          // InSquarePlacement.spread() takes the input position
                                                                                          // and randomizes the X and Z coordinates within the chunk
                                                                                          // PlacementUtils.HEIGHTMAP sets the Y-coordinate of the input position to the heightmap.
                                                                                          // This causes the tnt pile to be generated at a random surface position in the chunk.
                                                                                          () -> new PlacedFeature(
                                                                                              CONFIGURED_BURROW.asHolder(),
                                                                                              List.of(
                                                                                                  InSquarePlacement.spread(),
                                                                                                  PlacementUtils.HEIGHTMAP_WORLD_SURFACE,
                                                                                                  BiomeFilter.biome())));
  //                                    Optional.of(new MangroveRootPlacer(UniformInt.of(1, 3), BlockStateProvider.simple(Blocks.MANGROVE_ROOTS), Optional.of(new AboveRootPlacement(BlockStateProvider.simple(Blocks.MOSS_CARPET), 0.5F)), new MangroveRootPlacement(Registry.BLOCK.getOrCreateTag(BlockTags.MANGROVE_ROOTS_CAN_GROW_THROUGH), HolderSet.direct(Block::builtInRegistryHolder, new Block[]{Blocks.MUD, Blocks.MUDDY_MANGROVE_ROOTS}), BlockStateProvider.simple(Blocks.MUDDY_MANGROVE_ROOTS), 8, 15, 0.2F))), new TwoLayersFeatureSize(2, 0, 2))).decorators(List.of(new LeaveVineDecorator(0.125F), new AttachedToLeavesDecorator(0.14F, 1, 0, new RandomizedIntStateProvider(BlockStateProvider.simple((BlockState)Blocks.MANGROVE_PROPAGULE.defaultBlockState().setValue(MangrovePropaguleBlock.HANGING, true)), MangrovePropaguleBlock.AGE, UniformInt.of(0, 4)), 2, List.of(Direction.DOWN)), BEEHIVE_001)).ignoreVines().build());
  public static final RegistryObject<PlacedFeature> PLACED_BAOBAB = registerPlacedFeature("baobab",
                                                                                          () -> new PlacedFeature(
                                                                                              CONFIGURED_BAOBAB.asHolder(),
                                                                                              List.of(
                                                                                                  PlacementUtils.filteredByBlockSurvival(
                                                                                                      BlockRegister.BAOBAB_SAPLING.get()))));
  public static final RegistryObject<PlacedFeature> PLACED_PALM = registerPlacedFeature("palm", () -> new PlacedFeature(
      CONFIGURED_PALM.asHolder(), List.of(PlacementUtils.filteredByBlockSurvival(BlockRegister.PALM_SAPLING.get()))));

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Features for " + Constants.MOD_ID);
  }

  private static <T extends ConfiguredFeature<?, ?>> RegistryObject<T> registerConfiguredFeature(String name,
                                                                                                 Supplier<T> feature) {
    //    return Registry.register(BuiltinRegistries.CONFIGURED_FEATURE, new ResourceLocation(Constants.MOD_ID, name), feature);
    return CONFIGURED_FEATURES.register(name, feature);
  }

  private static <T extends PlacedFeature> RegistryObject<T> registerPlacedFeature(String name, Supplier<T> item) {
    //    return Registry.register(BuiltinRegistries.PLACED_FEATURE, new ResourceLocation(Constants.MOD_ID, name), item);
    return PLACED_FEATURES.register(name, item);
  }
}
