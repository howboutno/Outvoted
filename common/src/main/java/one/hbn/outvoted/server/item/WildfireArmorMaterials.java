package one.hbn.outvoted.server.item;

import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorMaterial;
import net.minecraft.world.item.crafting.Ingredient;
import one.hbn.outvoted.Constants;

import java.util.function.Supplier;

public enum WildfireArmorMaterials implements ArmorMaterial {
  WILDFIRE("wildfire", 25, new int[]{2, 2, 2, 2}, 0, SoundEvents.ARMOR_EQUIP_NETHERITE, 0.5F, 0.0F,
           () -> Ingredient.EMPTY), WILDFIRE_SOUL("wildfire_soul", 25, new int[]{2, 2, 2, 2}, 0,
                                                  SoundEvents.ARMOR_EQUIP_NETHERITE, 0.5F, 0.0F,
                                                  () -> Ingredient.EMPTY);

  private static final int[] MAX_DAMAGE_ARRAY = new int[]{13, 15, 16, 11};
  private final String name;
  private final int maxDamageFactor; //Durability, Iron=15, Diamond=33, Gold=7, Leather=5
  private final int[] damageReductionAmountArray; //Armor Bar Protection, 1 = 1/2 armor bar
  private final int enchantability;
  private final SoundEvent soundEvent;
  private final float toughness; //Increases Protection, 0.0F=Iron/Gold/Leather, 2.0F=Diamond, 3.0F=Netherite
  private final float knockbackResistance; //1.0F=No Knockback, 0.0F=Disabled
  private final Supplier<Ingredient> repairMaterial;

  WildfireArmorMaterials(String name, int maxDamageFactor, int[] damageReductionAmountArray, int enchantability,
                         SoundEvent soundEvent, float toughness, float knockbackResistance,
                         Supplier<Ingredient> repairMaterial) {
    this.name = name;
    this.maxDamageFactor = maxDamageFactor;
    this.damageReductionAmountArray = damageReductionAmountArray;
    this.enchantability = enchantability;
    this.soundEvent = soundEvent;
    this.toughness = toughness;
    this.repairMaterial = repairMaterial;
    this.knockbackResistance = knockbackResistance;
  }

  @Override
  public int getDurabilityForSlot(EquipmentSlot slotIn) {
    return MAX_DAMAGE_ARRAY[slotIn.getIndex()] * this.maxDamageFactor;
  }

  @Override
  public int getDefenseForSlot(EquipmentSlot slotIn) {
    return this.damageReductionAmountArray[slotIn.getIndex()];
  }

  @Override
  public int getEnchantmentValue() {
    return this.enchantability;
  }

  @Override
  public SoundEvent getEquipSound() {
    return this.soundEvent;
  }

  @Override
  public Ingredient getRepairIngredient() {
    return this.repairMaterial.get();
  }

  @Override
  public String getName() {
    return Constants.MOD_ID + ":" + this.name;
  }

  @Override
  public float getToughness() {
    return this.toughness;
  }

  @Override
  public float getKnockbackResistance() {
    return this.knockbackResistance;
  }
}
