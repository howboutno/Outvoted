package one.hbn.outvoted.server.recipe;

import net.minecraft.core.Registry;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.ShapelessRecipe;
import net.minecraft.world.item.crafting.SimpleRecipeSerializer;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

public class RecipeSerializerRegister {
  public static final RegistrationProvider<RecipeSerializer<?>> RECIPE_SERIALIZER = RegistrationProvider.get(
      Registry.RECIPE_SERIALIZER_REGISTRY, Constants.MOD_ID);
  public static final RegistryObject<SimpleRecipeSerializer<RepairCostRecipe>> REPAIR_COST = registerRecipe(
      "repair_cost", new SimpleRecipeSerializer<>(RepairCostRecipe::new));
  public static final RegistryObject<SimpleRecipeSerializer<WildfireShieldDecorationRecipe>> SHIELD_DECO = registerRecipe(
      "wildfire_shield_deco", new SimpleRecipeSerializer<>(WildfireShieldDecorationRecipe::new));
  public static final RegistryObject<RecipeSerializer<ShapelessRecipe>> PATCHOULI_SHAPELESS = registerRecipe(
      "patchouli_book", new ShapelessRecipe.Serializer());

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Recipes for " + Constants.MOD_ID);
  }

  private static <T extends RecipeSerializer<?>> RegistryObject<T> registerRecipe(String name, T type) {
    //    return Registry.register(Registry.RECIPE_SERIALIZER, new ResourceLocation(Constants.MOD_ID, name), type);
    return RECIPE_SERIALIZER.register(name, () -> type);
  }
}
