package one.hbn.outvoted.server.block;

import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.level.block.ButtonBlock;
import net.minecraft.world.level.block.WeatheringCopper;
import net.minecraft.world.level.block.state.BlockState;

import java.util.Random;

public class CopperButtonBlock extends ButtonBlock {
  private final WeatheringCopper.WeatherState weatherState;

  public CopperButtonBlock(WeatheringCopper.WeatherState oxidizationLevel, Properties settings) {
    super(false, settings);
    this.weatherState = oxidizationLevel;
  }

  @Override
  public int getPressDuration() {
    return 10 * (this.weatherState.ordinal() + 1);
  }

  @Override
  protected SoundEvent getSound(boolean powered) {
    return powered ? SoundEvents.METAL_PRESSURE_PLATE_CLICK_ON : SoundEvents.METAL_PRESSURE_PLATE_CLICK_OFF;
  }

  public static class Weathering extends CopperButtonBlock implements WeatheringCopperButton {
    public Weathering(WeatheringCopper.WeatherState weatherState, Properties settings) {
      super(weatherState, settings);
    }

    @Override
    public boolean isRandomlyTicking(BlockState state) {
      return WeatheringCopperButton.getNext(state.getBlock()).isPresent();
    }

    @Override
    public void onRandomTick(BlockState state, ServerLevel level, BlockPos pos, Random random) {
      if (!state.getValue(POWERED)) {
        WeatheringCopperButton.super.onRandomTick(state, level, pos, random);
      }
    }

    @Override
    public WeatheringCopper.WeatherState getAge() {
      return super.weatherState;
    }

    // Don't know if below is needed at all, but I'll keep it so as not to break anything
    //    public void tick(BlockState state, ServerLevel world, BlockPos pos, Random random) {
    //      if (state.getValue(POWERED)) {
    //        world.setBlock(pos, state.setValue(POWERED, false), 3);
    //        this.updateNeighbours(state, world, pos);
    //        this.playSound(null, world, pos, false);
    //        world.gameEvent(GameEvent.BLOCK_UNPRESS, pos);
    //      }
    //    }
    //
    //    private void updateNeighbours(BlockState state, Level world, BlockPos pos) {
    //      world.updateNeighborsAt(pos, this);
    //      world.updateNeighborsAt(pos.relative(getConnectedDirection(state).getOpposite()), this);
    //    }
  }
}
