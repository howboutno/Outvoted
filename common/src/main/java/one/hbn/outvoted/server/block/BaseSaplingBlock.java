package one.hbn.outvoted.server.block;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.BlockGetter;
import net.minecraft.world.level.biome.Biome;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.SaplingBlock;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.block.state.BlockState;

import java.util.List;

public final class BaseSaplingBlock extends SaplingBlock {
  public List<Block> blocks;
  //  private final Biome.BiomeCategory plantType;

  public BaseSaplingBlock(AbstractTreeGrower treeIn, Properties properties, Block... placeableOn) {
    this(treeIn, properties, Biome.BiomeCategory.PLAINS, placeableOn);
  }

  public BaseSaplingBlock(AbstractTreeGrower treeIn, Properties properties, Biome.BiomeCategory plantType,
                          Block... placeableOn) {
    super(treeIn, properties);
    blocks = List.of(placeableOn);
    //    this.plantType = plantType;
  }

  //  public Biome.BiomeCategory getPlantType(BlockGetter level, BlockPos pos) {
  //    return this.plantType;
  //  }

  @Override
  protected boolean mayPlaceOn(BlockState state, BlockGetter blockGetter, BlockPos pos) {
    return blocks.isEmpty() ? super.mayPlaceOn(state, blockGetter, pos) : blocks.contains(state.getBlock());
  }
}
