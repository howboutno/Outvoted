package one.hbn.outvoted.server.entity.animal;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.particles.ParticleOptions;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.nbt.NbtUtils;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.util.Mth;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffect;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.*;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.util.AirRandomPos;
import net.minecraft.world.entity.animal.Animal;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.entity.BlockEntity;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import one.hbn.outvoted.server.block.BurrowBlock;
import one.hbn.outvoted.server.block.entity.BlockEntityRegister;
import one.hbn.outvoted.server.block.entity.BurrowBlockEntity;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.entity.EntityUtil;
import one.hbn.outvoted.server.entity.ai.EnterBurrowGoal;
import one.hbn.outvoted.server.entity.ai.FindBurrowGoal;
import one.hbn.outvoted.server.entity.ai.MoveToBurrowGoal;
import one.hbn.outvoted.server.entity.ai.TravelToStructureGoal;
import one.hbn.outvoted.server.sounds.SoundRegister;
import one.hbn.outvoted.server.world.TagRegister;

import java.util.Optional;
import java.util.Random;
import java.util.UUID;

public class Meerkat extends Animal {
  private static final Ingredient TAMING_INGREDIENT = Ingredient.of(Items.SPIDER_EYE);
  private static final EntityDataAccessor<Boolean> TRUSTING = SynchedEntityData.defineId(Meerkat.class,
                                                                                         EntityDataSerializers.BOOLEAN);
  private static final EntityDataAccessor<Optional<UUID>> TRUSTED_UUID = SynchedEntityData.defineId(Meerkat.class,
                                                                                                    EntityDataSerializers.OPTIONAL_UUID);
  private BlockPos structurepos = null;
  private final int animtimer = 0;
  private int cannotEnterBurrowTicks;
  private final int ticksLeftToFindBurrow = 0;
  private BlockPos burrowPos = null;
  private FindBurrowGoal findBurrowGoal;
  private MoveToBurrowGoal moveToBurrowGoal;
  private TemptGoal temptGoal;

  public Meerkat(EntityType<? extends Meerkat> type, Level worldIn) {
    super(type, worldIn);
    this.setPathfindingMalus(BlockPathTypes.WATER, -1.0F); // no like da water
    EntityUtil.setHealthFromConfig(this, OutvotedConfig.COMMON.MOBS.MEERKAT);
  }

  public static AttributeSupplier.Builder setCustomAttributes() {
    return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, 0.3D).add(Attributes.ATTACK_DAMAGE)
        .add(Attributes.ATTACK_KNOCKBACK);
  }

  public static boolean canSpawn(EntityType<Meerkat> entity, LevelAccessor world, MobSpawnType spawnReason,
                                 BlockPos blockPos, Random random) {
    return world.getRawBrightness(blockPos, 0) > 8 && checkMobSpawnRules(entity, world, spawnReason, blockPos,
                                                                         random) && world.getBlockState(
        blockPos.below()).is(Blocks.SAND);
  }

  private static float getDistance(int a, int b, int x, int y) {
    int i = x - a;
    int j = y - b;
    return Mth.sqrt((float) (i * i + j * j));
  }

  protected void registerGoals() {
    this.temptGoal = new TemptGoal(this, 0.6D, TAMING_INGREDIENT, false);
    this.goalSelector.addGoal(1, new FloatGoal(this));
    this.goalSelector.addGoal(2, this.temptGoal);
    this.goalSelector.addGoal(3, new TravelToStructureGoal(this, 1.25D));
    this.goalSelector.addGoal(4, new EnterBurrowGoal(this));
    this.goalSelector.addGoal(5, new MeerkatMeleeAttackGoal(this, 1.0D, true));
    this.findBurrowGoal = new FindBurrowGoal(this);
    this.goalSelector.addGoal(6, this.findBurrowGoal);
    this.moveToBurrowGoal = new MoveToBurrowGoal(this);
    this.goalSelector.addGoal(7, this.moveToBurrowGoal);
    this.goalSelector.addGoal(8, new BreedGoal(this, 0.8D));
    this.goalSelector.addGoal(9, new FollowParentGoal(this, 1.25D));
    this.goalSelector.addGoal(10, new WaterAvoidingRandomStrollGoal(this, 1.0D));
    this.goalSelector.addGoal(11, new LookAtPlayerGoal(this, Player.class, 10.0F));
    this.targetSelector.addGoal(1, (new HurtByTargetGoal(this, Player.class, Meerkat.class)));
    this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Monster.class, 10, true, false,
                                                                     e -> e.getMobType() == MobType.ARTHROPOD));
  }  public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason,
                                      SpawnGroupData spawnDataIn, CompoundTag dataTag) {
    if (spawnDataIn == null) {
      spawnDataIn = new AgeableMobGroupData(1.0F);
    }

    return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
  }

  @Override
  protected SoundEvent getAmbientSound() {
    return SoundRegister.MEERKAT_AMBIENT.get();
  }

  @Override
  public boolean doHurtTarget(Entity target) {
    float f = (float) this.getAttributeValue(Attributes.ATTACK_DAMAGE);
    float g = (float) this.getAttributeValue(Attributes.ATTACK_KNOCKBACK);
    if (target instanceof LivingEntity) {
      if (((LivingEntity) target).getMobType() == MobType.ARTHROPOD) {
        f += 5.0F;
      }
    }

    boolean bl = target.hurt(DamageSource.mobAttack(this), f);
    if (bl) {
      if (g > 0.0F && target instanceof LivingEntity) {
        ((LivingEntity) target).knockback(g * 0.5F, Mth.sin(this.getYRot() * 0.017453292F),
                                          -Mth.cos(this.getYRot() * 0.017453292F));
        this.setDeltaMovement(this.getDeltaMovement().multiply(0.6D, 1.0D, 0.6D));
      }

      this.doEnchantDamageEffects(this, target);
      this.setLastHurtMob(target);
    }

    return bl;
  }  protected void defineSynchedData() {
    super.defineSynchedData();
    this.entityData.define(TRUSTING, false);
    this.entityData.define(TRUSTED_UUID, Optional.empty());
  }

  public boolean doesBurrowHaveSpace(BlockPos pos) {
    BlockEntity blockEntity = this.level.getBlockEntity(pos);
    if (blockEntity instanceof BurrowBlockEntity) {
      Direction dir = this.level.getBlockState(pos).getValue(BurrowBlock.FACING);
      return !((BurrowBlockEntity) blockEntity).isFull() && this.level.getBlockState(pos.relative(dir)).isAir();
    } else {
      return false;
    }
  }

  public BlockPos getBurrowOffsetPos() {
    Meerkat mob = Meerkat.this;
    Level world = mob.level;
    BlockPos burrow = mob.getBurrowPos();
    return burrow.relative(world.getBlockState(burrow).getValue(BurrowBlock.FACING));
  }

  public BlockPos getBurrowPos() {
    return this.burrowPos;
  }

  public void setBurrowPos(BlockPos pos) {
    this.burrowPos = pos;
  }

  public MoveToBurrowGoal getMoveToBurrowGoal() {
    return this.moveToBurrowGoal;
  }

  public FindBurrowGoal getFindBurrowGoal() {
    return this.findBurrowGoal;
  }

  public void resetStructurePos() {
    this.structurepos = null;
  }

  public void aiStep() {
    super.aiStep();
    if (!this.level.isClientSide) {
      if (this.cannotEnterBurrowTicks > 0) {
        --this.cannotEnterBurrowTicks;
      }

      if (this.tickCount % 20 == 0 && !this.isBurrowValid()) {
        this.burrowPos = null;
      }
    }

  }

  public void addAdditionalSaveData(CompoundTag tag) {
    super.addAdditionalSaveData(tag);
    if (this.hasStructurePos())
      tag.put("StructPos", NbtUtils.writeBlockPos(this.getStructurePos()));
    tag.putBoolean("Trusting", this.isTrusting());
    if (this.hasTrusted())
      tag.putUUID("Trusted", getTrusted());
    if (this.hasBurrow()) {
      tag.put("BurrowPos", NbtUtils.writeBlockPos(this.getBurrowPos()));
    }
  }

  public void readAdditionalSaveData(CompoundTag tag) {
    this.structurepos = null;
    if (tag.contains("StructPos"))
      this.structurepos = NbtUtils.readBlockPos(tag.getCompound("StructPos"));

    this.burrowPos = null;
    if (tag.contains("BurrowPos")) {
      this.burrowPos = NbtUtils.readBlockPos(tag.getCompound("BurrowPos"));
    }

    super.readAdditionalSaveData(tag);
    this.setTrusting(tag.getBoolean("Trusting"));
    if (tag.contains("Trusted"))
      this.setTrusted(tag.getUUID("Trusted"));
  }

  public boolean isFood(ItemStack stack) {
    return TAMING_INGREDIENT.test(stack);
  }

  @Override
  public InteractionResult mobInteract(Player player, InteractionHand hand) {
    ItemStack itemStack = player.getItemInHand(hand);
    if ((this.temptGoal == null || this.temptGoal.isRunning()) && !this.isTrusting() && this.isFood(
        itemStack) && player.distanceToSqr(this) < 9.0D) {
      this.usePlayerItem(player, hand, itemStack);
      if (!this.level.isClientSide) {
        if (this.random.nextInt(3) == 0) {
          this.setTrusting(true);
          this.showEmoteParticle(true);
          this.level.broadcastEntityEvent(this, (byte) 41);
        } else {
          this.showEmoteParticle(false);
          this.level.broadcastEntityEvent(this, (byte) 40);
        }
      }

      return InteractionResult.sidedSuccess(this.level.isClientSide);
    } else if (this.isTrusting() && player.distanceToSqr(this) < 9.0D) {
      if (itemStack.getItem() == Items.STICK) {
        if (!this.level.isClientSide) {
          structurepos = findStructure();
          setTrusted(player.getUUID());
        }
        return InteractionResult.CONSUME;
      }
    }
    return InteractionResult.PASS;
  }

  private void showEmoteParticle(boolean positive) {
    ParticleOptions particleEffect = ParticleTypes.HEART;
    if (!positive) {
      particleEffect = ParticleTypes.SMOKE;
    }

    for (int i = 0; i < 7; ++i) {
      double d = this.random.nextGaussian() * 0.02D;
      double e = this.random.nextGaussian() * 0.02D;
      double f = this.random.nextGaussian() * 0.02D;
      this.level.addParticle(particleEffect, this.getRandomX(1.0D), this.getRandomY() + 0.5D, this.getRandomZ(1.0D), d,
                             e, f);
    }
  }

  private BlockPos findStructure() {
    if (!this.level.isClientSide) {
      BlockPos blockPos = new BlockPos(this.blockPosition());
      if (this.getServer().overworld() != null) {
        ServerLevel serverLevel = this.getServer().overworld();
        return serverLevel.findNearestMapFeature(TagRegister.IN_DESERT, blockPos, 100, false);
      }
    }
    return null;
  }

  public void handleEntityEvent(byte status) {
    if (this.level.isClientSide()) {
      if (status == 41) {
        this.showEmoteParticle(true);
      } else if (status == 40) {
        this.showEmoteParticle(false);
      } else {
        super.handleEntityEvent(status);
      }
    }
  }

  private boolean hasStructurePos() {
    return this.structurepos != null;
  }

  public BlockPos getStructurePos() {
    return this.structurepos;
  }

  public boolean isTrusting() {
    return this.entityData.get(TRUSTING);
  }

  private void setTrusting(boolean trusting) {
    this.entityData.set(TRUSTING, trusting);
  }

  public boolean hasTrusted() {
    return getTrusted() != null;
  }

  public UUID getTrusted() {
    return (UUID) ((Optional) this.entityData.get(TRUSTED_UUID)).orElse(null);
  }

  public void setTrusted(UUID trusted) {
    this.entityData.set(TRUSTED_UUID, Optional.ofNullable(trusted));
  }

  private boolean isBurrowValid() {
    if (!this.hasBurrow()) {
      return false;
    } else {
      BlockEntity blockEntity = this.level.getBlockEntity(this.burrowPos);
      return blockEntity != null && blockEntity.getType() == BlockEntityRegister.BURROW.get();
    }
  }

  public boolean hasBurrow() {
    return this.burrowPos != null;
  }

  @Override
  public boolean canBeAffected(MobEffectInstance effect) {
    MobEffect statusEffect = effect.getEffect();
    if (statusEffect == MobEffects.POISON) {
      return false;
    }
    return super.canBeAffected(effect);
  }

  @Override
  protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
    return SoundRegister.MEERKAT_HURT.get();
  }

  @Override
  protected SoundEvent getDeathSound() {
    return SoundRegister.MEERKAT_DEATH.get();
  }

  @Override
  public float getStandingEyeHeight(Pose pose, EntityDimensions dimensions) {
    if (pose == Pose.CROUCHING) {
      return 0.5F;
    }
    return super.getStandingEyeHeight(pose, dimensions);
  }

  public boolean canEnterBurrow() {
    return this.getTarget() == null;
  }

  public boolean isTooFar(BlockPos pos) {
    return !this.isWithinDistance(pos, 32);
  }

  public boolean isWithinDistance(BlockPos pos, int distance) {
    return pos.closerThan(this.blockPosition(), distance);
  }

  public void startMovingTo(BlockPos pos) {
    Vec3 vec3d = Vec3.atBottomCenterOf(pos);
    int i = 0;
    BlockPos blockPos = this.blockPosition();
    int j = (int) vec3d.y - blockPos.getY();
    if (j > 2) {
      i = 4;
    } else if (j < -2) {
      i = -4;
    }

    int k = 6;
    int l = 8;
    int m = blockPos.distManhattan(pos);
    if (m < 15) {
      k = m / 2;
      l = m / 2;
    }

    Vec3 vec3d2 = AirRandomPos.getPosTowards(this, k, l, i, vec3d, 0.3141592741012573D);
    if (vec3d2 != null) {
      this.navigation.setMaxVisitedNodesMultiplier(0.5F);
      this.navigation.moveTo(vec3d2.x + 0.5, vec3d2.y, vec3d2.z + 0.5, 1.0D);
    }
  }

  @Override
  public void setPose(Pose pose) {
    if (pose != this.getPose()) {
      super.setPose(pose);
      this.setBoundingBox(calcBox());
    }
  }

  @Override
  public Vec3 getLeashOffset() {
    return new Vec3(0.0D, (0.5F * this.getEyeHeight()), (this.getBbWidth() * 0.05F));
  }

  private AABB calcBox() {
    EntityDimensions entityDimensions = this.getDimensions(Pose.STANDING);
    boolean bl = this.getPose() == Pose.STANDING;
    double f = (double) entityDimensions.width / (bl ? 4.0F : 2.0F);
    double height = (double) entityDimensions.height / (bl ? 1.0F : 2.0F);
    Vec3 vec3d = new Vec3(this.getX() - f, this.getY(), this.getZ() - f);
    Vec3 vec3d2 = new Vec3(this.getX() + f, this.getY() + height, this.getZ() + f);
    return new AABB(vec3d, vec3d2);
  }

  class MeerkatMeleeAttackGoal extends MeleeAttackGoal {
    public MeerkatMeleeAttackGoal(Meerkat mob, double speed, boolean pauseWhenMobIdle) {
      super(mob, speed, pauseWhenMobIdle);
    }

    @Override
    public boolean canUse() {
      return super.canUse() && this.mob.getHealth() > 3.0D;
    }

    @Override
    public boolean canContinueToUse() {
      return super.canContinueToUse() && this.mob.getHealth() > 3.0D;
    }
  }



  @Override
  public Meerkat getBreedOffspring(ServerLevel world, AgeableMob entity) {
    Meerkat meerkat = EntityRegister.MEERKAT.get().create(world);
    meerkat.finalizeSpawn(world, world.getCurrentDifficultyAt(meerkat.blockPosition()), MobSpawnType.BREEDING, null,
                          null);
    return meerkat;
  }




  //  private final AnimationFactory factory = new AnimationFactory(this);
  //
  //  public <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
  //    if (event.getController().getAnimationState().equals(AnimationState.Stopped) || (animtimer == 10 && this.isOnGround() && !this.isInWaterOrBubble() && !event.isMoving())) {
  //      event.getController().setAnimation(new AnimationBuilder().addAnimation("stand"));
  //      this.setPose(Pose.STANDING);
  //    } else if (event.isMoving()) {
  //      event.getController().setAnimation(new AnimationBuilder().addAnimation("walk"));
  //      this.setPose(Pose.CROUCHING);
  //      animtimer = 0;
  //    }
  //    if (animtimer < 10) animtimer++;
  //    return PlayState.CONTINUE;
  //  }
  //
  //  @Override
  //  public void registerControllers(AnimationData data) {
  //    AnimationController<Meerkat> controller = new AnimationController<>(this, "controller", 2, this::predicate);
  //    data.addAnimationController(controller);
  //  }
  //
  //  @Override
  //  public AnimationFactory getFactory() {
  //    return this.factory;
  //  }


}

