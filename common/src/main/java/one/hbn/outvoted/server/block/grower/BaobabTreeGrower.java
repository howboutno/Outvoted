package one.hbn.outvoted.server.block.grower;


import net.minecraft.core.Holder;
import net.minecraft.world.level.block.grower.AbstractTreeGrower;
import net.minecraft.world.level.levelgen.feature.ConfiguredFeature;
import one.hbn.outvoted.server.world.feature.FeatureRegister;

import java.util.Random;

public class BaobabTreeGrower extends AbstractTreeGrower {
  protected Holder<? extends ConfiguredFeature<?, ?>> getConfiguredFeature(Random random, boolean bl) {
    return FeatureRegister.CONFIGURED_BAOBAB.asHolder();
  }
}
