package one.hbn.outvoted.server.entity.ai;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.GameRules;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.animal.Glare;

import java.util.EnumSet;
import java.util.List;

public class PickupItemGoal extends Goal {
  private final Glare mob;

  public PickupItemGoal(Glare mob) {
    this.mob = mob;
    this.setFlags(EnumSet.of(Flag.MOVE));
  }

  public boolean canUse() {
    if (!OutvotedConfig.COMMON.MOBS.GLARE.shouldInteract.get() || !this.mob.level.getGameRules()
        .getBoolean(GameRules.RULE_MOBGRIEFING))
      return false;
    if (this.mob.getTarget() == null && this.mob.getLastHurtByMob() == null) {
      if (this.mob.getRandom().nextInt(10) != 0) {
        return false;
      } else {
        List<ItemEntity> list = this.mob.level.getEntitiesOfClass(ItemEntity.class,
                                                                  this.mob.getBoundingBox().inflate(8.0D, 8.0D, 8.0D),
                                                                  Glare.PICKABLE_DROP_FILTER);
        return !list.isEmpty() && this.mob.getItemBySlot(EquipmentSlot.MAINHAND).isEmpty();
      }
    }

    return false;
  }

  public void start() {
    List<ItemEntity> list = this.mob.level.getEntitiesOfClass(ItemEntity.class,
                                                              this.mob.getBoundingBox().inflate(8.0D, 8.0D, 8.0D),
                                                              Glare.PICKABLE_DROP_FILTER);
    if (!list.isEmpty()) {
      this.mob.getNavigation().moveTo(list.get(0), 1.2D);
    }
  }

  public void tick() {
    List<ItemEntity> list = this.mob.level.getEntitiesOfClass(ItemEntity.class,
                                                              this.mob.getBoundingBox().inflate(8.0D, 8.0D, 8.0D),
                                                              Glare.PICKABLE_DROP_FILTER);
    ItemStack itemStack = this.mob.getItemBySlot(EquipmentSlot.MAINHAND);
    if (itemStack.isEmpty() && !list.isEmpty()) {
      this.mob.getNavigation().moveTo(list.get(0), 1.2D);
    }
  }
}
