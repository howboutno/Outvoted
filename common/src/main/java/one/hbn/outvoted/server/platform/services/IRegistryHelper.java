package one.hbn.outvoted.server.platform.services;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.item.SpawnEggItem;
import one.hbn.outvoted.server.registry.RegistryObject;

public interface IRegistryHelper {
  /**
   * Creates a spawn egg item.
   *
   * @param type The {@link EntityType} the egg will spawn.
   * @param bgColor An RGB value determining the background color.
   * @param hlColor An RGB value determining the spot color.
   * @return The created {@link SpawnEggItem}.
   */
  SpawnEggItem createSpawnEgg(RegistryObject<? extends EntityType<? extends Mob>> type, int bgColor, int hlColor);
}
