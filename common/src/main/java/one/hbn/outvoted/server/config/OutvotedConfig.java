package one.hbn.outvoted.server.config;

import java.util.List;
import java.util.function.Supplier;

public abstract class OutvotedConfig {
  public static Common COMMON;
  public static Client CLIENT;

  public static class SpawnConfig {
    public Supplier<Integer> spawnRate;
    public Supplier<Double> maxHealth;
    public Supplier<List<? extends String>> allowedBiomes;
  }

  public abstract static class Mob {
    public SpawnConfig spawnConfig;
  }

  public static class Wildfire extends Mob {
    public Supplier<Integer> fireballCount;
    public Supplier<Double> fireballOffsetAngle;
    public Supplier<Double> fireballMaxDepressAngle;
    public Supplier<Boolean> fireballExplode;
    public Supplier<Double> fireballExplodePower;
  }

  public static class Glutton extends Mob {
    public Supplier<Boolean> stealEnchants;
    public Supplier<Boolean> capEnchants;
    public Supplier<Integer> maxEnchants;
  }

  public static class Barnacle extends Mob {
  }

  public static class Meerkat extends Mob {
    public Supplier<Integer> burrowGenerationRate;
  }

  public static class Ostrich extends Mob {
  }

  public static class CopperGolem extends Mob {
    public Supplier<Double> oxidationRate;
  }

  public static class Glare extends Mob {
    public Supplier<Boolean> shouldInteract;
    public Supplier<Integer> inventorySize;
  }

  public static class Mobs {
    public Wildfire WILDFIRE;
    public Barnacle BARNACLE;
    public Glutton GLUTTON;
    public Meerkat MEERKAT;
    public Ostrich OSTRICH;
    public CopperGolem COPPER_GOLEM;
    public Glare GLARE;
  }

  public static class Misc {
    public Supplier<Boolean> giveBook;
    public Supplier<Integer> helmetFireTicks;
  }

  public static class Common {
    public Mobs MOBS;
    public Misc MISC;
  }

  public static class Client {
    public Supplier<Boolean> variants;
  }
}
