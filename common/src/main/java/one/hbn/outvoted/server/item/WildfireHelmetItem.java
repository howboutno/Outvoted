package one.hbn.outvoted.server.item;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.ArmorItem;

public class WildfireHelmetItem extends ArmorItem {
  public WildfireHelmetItem(boolean soul) {
    super(soul ? WildfireArmorMaterials.WILDFIRE_SOUL : WildfireArmorMaterials.WILDFIRE, EquipmentSlot.HEAD,
          ItemRegister.ITEM_PROPERTIES.fireResistant());
  }
}
