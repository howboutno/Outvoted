package one.hbn.outvoted.server.sounds;

import net.minecraft.core.Registry;
import net.minecraft.sounds.SoundEvent;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

public class SoundRegister {
  public static final RegistrationProvider<SoundEvent> SOUNDS = RegistrationProvider.get(Registry.SOUND_EVENT_REGISTRY,
                                                                                         Constants.MOD_ID);
  public static final RegistryObject<SoundEvent> WILDFIRE_AMBIENT = registerSound("wildfire_ambient", new SoundEvent(
      Constants.loc("wildfire_ambient")));
  public static final RegistryObject<SoundEvent> WILDFIRE_HURT = registerSound("wildfire_hurt", new SoundEvent(
      Constants.loc("wildfire_hurt")));
  public static final RegistryObject<SoundEvent> WILDFIRE_DEATH = registerSound("wildfire_death", new SoundEvent(
      Constants.loc("wildfire_death")));
  public static final RegistryObject<SoundEvent> WILDFIRE_BURN = registerSound("wildfire_burn", new SoundEvent(
      Constants.loc("wildfire_burn")));
  public static final RegistryObject<SoundEvent> WILDFIRE_SHOOT = registerSound("wildfire_shoot", new SoundEvent(
      Constants.loc("wildfire_shoot")));
  public static final RegistryObject<SoundEvent> GLUTTON_AMBIENT = registerSound("glutton_ambient", new SoundEvent(
      Constants.loc("glutton_ambient")));
  public static final RegistryObject<SoundEvent> GLUTTON_HURT = registerSound("glutton_hurt", new SoundEvent(
      Constants.loc("glutton_hurt")));
  public static final RegistryObject<SoundEvent> GLUTTON_DEATH = registerSound("glutton_death", new SoundEvent(
      Constants.loc("glutton_death")));
  public static final RegistryObject<SoundEvent> GLUTTON_BITE = registerSound("glutton_bite", new SoundEvent(
      Constants.loc("glutton_bite")));
  public static final RegistryObject<SoundEvent> GLUTTON_SPIT = registerSound("glutton_spit", new SoundEvent(
      Constants.loc("glutton_spit")));
  public static final RegistryObject<SoundEvent> GLUTTON_EAT = registerSound("glutton_eat", new SoundEvent(
      Constants.loc("glutton_eat")));
  public static final RegistryObject<SoundEvent> GLUTTON_DIG = registerSound("glutton_dig", new SoundEvent(
      Constants.loc("glutton_dig")));
  public static final RegistryObject<SoundEvent> GLUTTON_DIG_SAND = registerSound("glutton_dig_sand", new SoundEvent(
      Constants.loc("glutton_dig_sand")));
  public static final RegistryObject<SoundEvent> BARNACLE_AMBIENT = registerSound("barnacle_ambient", new SoundEvent(
      Constants.loc("barnacle_ambient")));
  public static final RegistryObject<SoundEvent> BARNACLE_HURT = registerSound("barnacle_hurt", new SoundEvent(
      Constants.loc("barnacle_hurt")));
  public static final RegistryObject<SoundEvent> BARNACLE_DEATH = registerSound("barnacle_death", new SoundEvent(
      Constants.loc("barnacle_death")));
  public static final RegistryObject<SoundEvent> BARNACLE_FLOP = registerSound("barnacle_flop", new SoundEvent(
      Constants.loc("barnacle_flop")));
  public static final RegistryObject<SoundEvent> MEERKAT_AMBIENT = registerSound("meerkat_ambient", new SoundEvent(
      Constants.loc("meerkat_ambient")));
  public static final RegistryObject<SoundEvent> MEERKAT_HURT = registerSound("meerkat_hurt", new SoundEvent(
      Constants.loc("meerkat_hurt")));
  public static final RegistryObject<SoundEvent> MEERKAT_DEATH = registerSound("meerkat_death", new SoundEvent(
      Constants.loc("meerkat_death")));
  public static final RegistryObject<SoundEvent> OSTRICH_AMBIENT = registerSound("ostrich_ambient", new SoundEvent(
      Constants.loc("ostrich_ambient")));

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Sounds for " + Constants.MOD_ID);
  }

  private static <T extends SoundEvent> RegistryObject<T> registerSound(String name, T type) {
    return SOUNDS.register(name, () -> type);
  }
}
