package one.hbn.outvoted.server.loot;

import net.minecraft.core.Registry;
import net.minecraft.world.level.storage.loot.predicates.LootItemConditionType;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

public class LootConditionRegister {
  public static final RegistrationProvider<LootItemConditionType> CONDITIONS = RegistrationProvider.get(
      Registry.LOOT_CONDITION_TYPE, Constants.MOD_ID);
  public static final RegistryObject<LootItemConditionType> CONFIG_PATCHOULI = registerCondition("config_patchouli",
                                                                                                 new LootItemConditionType(
                                                                                                     new LootPatchouliCondition.Serializer()));

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Conditions for " + Constants.MOD_ID);
  }

  private static <T extends LootItemConditionType> RegistryObject<T> registerCondition(String name, T type) {
    return CONDITIONS.register(name, () -> type);
  }
}
