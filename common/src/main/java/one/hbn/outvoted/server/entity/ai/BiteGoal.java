package one.hbn.outvoted.server.entity.ai;

import net.minecraft.world.Difficulty;
import net.minecraft.world.entity.ai.goal.MeleeAttackGoal;
import one.hbn.outvoted.server.entity.monster.Glutton;

public class BiteGoal extends MeleeAttackGoal {
  private final Glutton mob;

  public BiteGoal(Glutton entityIn, double speedIn, boolean useMemory) {
    super(entityIn, speedIn, useMemory);
    this.mob = entityIn;
  }

  public boolean canUse() {
    this.mob.setAggressive(this.mob.getTarget() != null && !this.mob.isBurrowed());
    return super.canUse() && this.mob.level.getDifficulty() != Difficulty.PEACEFUL && !this.mob.isBurrowed();
  }

  public boolean canContinueToUse() {
    return super.canContinueToUse() && this.mob.level.getDifficulty() != Difficulty.PEACEFUL && !this.mob.isBurrowed();
  }

  public void start() {
    super.start();
    this.mob.setAggressive(true);
  }

  public void stop() {
    super.stop();
    this.mob.setAggressive(this.mob.getTarget() != null);
  }
}