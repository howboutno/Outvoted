package one.hbn.outvoted.server.world;

import net.minecraft.core.Registry;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.Item;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.levelgen.feature.ConfiguredStructureFeature;
import one.hbn.outvoted.Constants;

public final class TagRegister {
  //Blocks
  public static final TagKey<Block> GLUTTON_CAN_BURROW = TagKey.create(Registry.BLOCK_REGISTRY,
                                                                       Constants.loc("glutton_can_burrow"));
  public static final TagKey<Block> PALM_LOGS = TagKey.create(Registry.BLOCK_REGISTRY, Constants.loc("palm_logs"));
  public static final TagKey<Block> BAOBAB_LOGS = TagKey.create(Registry.BLOCK_REGISTRY, Constants.loc("baobab_logs"));
  public static final TagKey<Block> COPPER_BUTTONS = TagKey.create(Registry.BLOCK_REGISTRY,
                                                                   Constants.loc("copper_buttons"));
  //Items
  public static final TagKey<Item> PALM_LOGS_ITEM = TagKey.create(Registry.ITEM_REGISTRY, Constants.loc("palm_logs"));
  public static final TagKey<Item> BAOBAB_LOGS_ITEM = TagKey.create(Registry.ITEM_REGISTRY,
                                                                    Constants.loc("baobab_logs"));
  //Structures
  public static final TagKey<ConfiguredStructureFeature<?, ?>> IN_DESERT = TagKey.create(
      Registry.CONFIGURED_STRUCTURE_FEATURE_REGISTRY, Constants.loc("in_desert"));

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Tags for " + Constants.MOD_ID);
  }
}

