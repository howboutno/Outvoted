package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import one.hbn.outvoted.server.entity.animal.Glare;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class FindDarkestNearbySpotGoal extends Goal {
  int ticks;
  private final Glare mob;

  public FindDarkestNearbySpotGoal(Glare mob) {
    this.mob = mob;
    this.ticks = this.mob.getLevel().random.nextInt(30);
  }

  public boolean canUse() {
    ++this.ticks;
    return (!this.mob.hasDarkPos() || !this.mob.isDarkSpot(this.mob.getDarkPos())) && this.ticks > 40;
  }

  public boolean canContinueToUse() {
    return false;
  }

  public void start() {
    this.ticks = 0;
    BlockPos darkestSpot = getDarkestSpot();
    if (darkestSpot != null) {
      this.mob.setDarkPos(darkestSpot);
    }
  }

  private BlockPos getDarkestSpot() {
    Map<BlockPos, Integer> map = getNearbyDarkSpots();
    BlockPos returnPos = null;
    if (!map.isEmpty()) {
      Map.Entry<BlockPos, Integer> min = Collections.min(map.entrySet(), Map.Entry.comparingByValue());
      returnPos = min.getKey();
    }
    return returnPos;
  }

  private Map<BlockPos, Integer> getNearbyDarkSpots() {
    int mult = 5;
    Map<BlockPos, Integer> map = new HashMap<>();
    for (BlockPos blockPos : BlockPos.withinManhattan(this.mob.blockPosition(), mult, mult - 2, mult)) {
      if (this.mob.isDarkSpot(blockPos))
        map.put(blockPos.immutable(), this.mob.getLight(blockPos));
    }
    return map;
  }
}
