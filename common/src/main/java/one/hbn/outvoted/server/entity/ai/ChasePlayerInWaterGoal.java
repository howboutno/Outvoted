package one.hbn.outvoted.server.entity.ai;

import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.MoveTowardsTargetGoal;
import one.hbn.outvoted.server.entity.monster.Barnacle;

public class ChasePlayerInWaterGoal extends MoveTowardsTargetGoal {
  private final Barnacle mob;
  private final double speed;

  public ChasePlayerInWaterGoal(Barnacle barnacle, double speedIn, float maxDistanceIn) {
    super(barnacle, speedIn, maxDistanceIn);
    this.mob = barnacle;
    this.speed = speedIn;
  }

  public boolean canUse() {
    LivingEntity target = this.mob.getTarget();
    return target != null && super.canUse() && this.mob.waterCheck(target) && this.mob.isWithinRestriction();
  }

  public boolean canContinueToUse() {
    LivingEntity target = this.mob.getTarget();
    return target != null && super.canContinueToUse() && this.mob.waterCheck(target) && this.mob.isWithinRestriction();
  }

  public void stop() {
  }

  public void start() {
  }

  public void tick() {
    LivingEntity target = this.mob.getTarget();
    if (target != null) {
      this.mob.getNavigation().moveTo(target, this.speed);
    }
  }
}
