package one.hbn.outvoted.server.block.entity;

import net.minecraft.core.BlockPos;
import net.minecraft.world.level.block.entity.BlockEntityType;
import net.minecraft.world.level.block.entity.SignBlockEntity;
import net.minecraft.world.level.block.state.BlockState;

public class OutvotedSignBlockEntity extends SignBlockEntity {
  public OutvotedSignBlockEntity(BlockPos arg, BlockState arg2) {
    super(arg, arg2);
  }

  @Override
  public BlockEntityType<?> getType() {
    return BlockEntityRegister.SIGN.get();
  }
}
