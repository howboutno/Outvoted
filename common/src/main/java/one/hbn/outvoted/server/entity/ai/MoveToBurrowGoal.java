package one.hbn.outvoted.server.entity.ai;

import com.google.common.collect.Lists;
import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.pathfinder.Path;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.entity.animal.Meerkat;
import org.jetbrains.annotations.Nullable;

import java.util.EnumSet;
import java.util.List;

public class MoveToBurrowGoal extends Goal {
  private final List<BlockPos> possibleBurrows;
  protected Meerkat mob;
  private int ticks;
  @Nullable
  private Path path;
  private int ticksUntilLost;

  public MoveToBurrowGoal(Meerkat mob) {
    this.mob = mob;
    this.ticks = this.mob.level.random.nextInt(10);
    this.possibleBurrows = Lists.newArrayList();
    this.path = null;
    this.setFlags(EnumSet.of(Flag.MOVE));
  }

  public boolean isPossibleBurrow(BlockPos pos) {
    return this.possibleBurrows.contains(pos);
  }

  public void clearPossibleBurrows() {
    this.possibleBurrows.clear();
  }  public boolean canUse() {
    Meerkat mob = this.mob;
    Level world = mob.level;
    BlockPos burrow = mob.getBurrowPos();
    return mob.hasBurrow() && !mob.hasRestriction() && mob.canEnterBurrow() && !this.isCloseEnough(
        burrow) && world.getBlockState(burrow).is(BlockRegister.BURROW.get()) && world.getBlockState(
        mob.getBurrowOffsetPos()).isAir();
  }



  public boolean canContinueToUse() {
    return this.canUse();
  }

  public void start() {
    this.ticks = 0;
    this.ticksUntilLost = 0;
    super.start();
  }

  public void stop() {
    this.ticks = 0;
    this.ticksUntilLost = 0;
    this.mob.getNavigation().stop();
    this.mob.getNavigation().resetMaxVisitedNodesMultiplier();
  }

  public void tick() {
    if (this.mob.hasBurrow()) {
      BlockPos blockPos = this.mob.getBurrowOffsetPos();
      ++this.ticks;
      if (this.ticks > 600) {
        this.makeChosenBurrowPossibleBurrow();
      } else if (!this.mob.getNavigation().isInProgress()) {
        if (!this.mob.isWithinDistance(blockPos, 16)) {
          if (this.mob.isTooFar(blockPos)) {
            this.setLost();
          } else {
            this.mob.startMovingTo(blockPos);
          }
        } else {
          boolean bl = this.startMovingToFar(blockPos);
          if (!bl) {
            this.makeChosenBurrowPossibleBurrow();
          } else if (this.path != null && this.mob.getNavigation().getPath().sameAs(this.path)) {
            ++this.ticksUntilLost;
            if (this.ticksUntilLost > 60) {
              this.setLost();
              this.ticksUntilLost = 0;
            }
          } else {
            this.path = this.mob.getNavigation().getPath();
          }

        }
      }
    }
  }

  private boolean startMovingToFar(BlockPos pos) {
    this.mob.getNavigation().setMaxVisitedNodesMultiplier(10.0F);
    this.mob.getNavigation().moveTo(pos.getX() + 0.5, pos.getY(), pos.getZ() + 0.5, 1.0D);
    return this.mob.getNavigation().getPath() != null && this.mob.getNavigation().getPath().canReach();
  }


  private void addPossibleBurrow(BlockPos pos) {
    this.possibleBurrows.add(pos);

    while (this.possibleBurrows.size() > 3) {
      this.possibleBurrows.remove(0);
    }

  }


  private void makeChosenBurrowPossibleBurrow() {
    if (this.mob.hasBurrow()) {
      this.addPossibleBurrow(this.mob.getBurrowPos());
    }

    this.setLost();
  }

  private void setLost() {
    this.mob.setBurrowPos(null);
    //    this.mob.getFindBurrowGoal().ticksLeftToFindBurrow = 200;
  }

  private boolean isCloseEnough(BlockPos pos) {
    if (pos.closerThan(this.mob.blockPosition(), 1.0D)) {
      return true;
    } else {
      Path path = this.mob.getNavigation().getPath();
      return path != null && path.getTarget().equals(pos) && path.canReach() && path.isDone();
    }
  }
}
