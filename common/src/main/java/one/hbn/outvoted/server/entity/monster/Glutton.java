package one.hbn.outvoted.server.entity.monster;

import com.google.common.collect.ImmutableList;
import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.core.Direction8;
import net.minecraft.core.NonNullList;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.Component;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundSource;
import net.minecraft.util.Mth;
import net.minecraft.world.DifficultyInstance;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.effect.MobEffectInstance;
import net.minecraft.world.effect.MobEffects;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.goal.FloatGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.WaterAvoidingRandomStrollGoal;
import net.minecraft.world.entity.ai.goal.target.HurtByTargetGoal;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.monster.Creeper;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.AirItem;
import net.minecraft.world.item.EnchantedBookItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.enchantment.*;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.ServerLevelAccessor;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.state.BlockState;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.EntityUtil;
import one.hbn.outvoted.server.entity.ai.BiteGoal;
import one.hbn.outvoted.server.entity.ai.BuryAndEnchantItemsGoal;
import one.hbn.outvoted.server.entity.ai.FindSpotToBuryGoal;
import one.hbn.outvoted.server.item.ItemRegister;
import one.hbn.outvoted.server.sounds.SoundRegister;
import one.hbn.outvoted.server.world.TagRegister;
import org.apache.commons.lang3.tuple.MutablePair;
import org.jetbrains.annotations.Nullable;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

public class Glutton extends Monster {
  private static final EntityDataAccessor<Boolean> BURROWED;
  private static final EntityDataAccessor<Boolean> ATTACKING;
  private static final EntityDataAccessor<Boolean> ENCHANTING;
  private static final EntityDataAccessor<Integer> VARIANT;

  static {
    BURROWED = SynchedEntityData.defineId(Glutton.class, EntityDataSerializers.BOOLEAN);
    ATTACKING = SynchedEntityData.defineId(Glutton.class, EntityDataSerializers.BOOLEAN);
    ENCHANTING = SynchedEntityData.defineId(Glutton.class, EntityDataSerializers.BOOLEAN);
    VARIANT = SynchedEntityData.defineId(Glutton.class, EntityDataSerializers.INT);
  }

  protected Map<Enchantment, Integer> storedEnchants = new HashMap<>();

  public Glutton(EntityType<? extends Glutton> type, Level worldIn) {
    super(type, worldIn);
    this.xpReward = 5;
  }

  public static AttributeSupplier.Builder setCustomAttributes() {
    return Mob.createMobAttributes().add(Attributes.ATTACK_DAMAGE, 12.0D).add(Attributes.MOVEMENT_SPEED, 0.25D);
  }

  public static boolean canSpawn(EntityType<Glutton> entity, LevelAccessor world, MobSpawnType spawnReason,
                                 BlockPos blockPos, Random random) {
    return world.getRawBrightness(blockPos, 0) > 8 && checkMobSpawnRules(entity, world, spawnReason, blockPos,
                                                                         random) && world.getBlockState(
        blockPos.below()).is(TagRegister.GLUTTON_CAN_BURROW);
  }

  protected void registerGoals() {
    this.goalSelector.addGoal(1, new FloatGoal(this));
    this.goalSelector.addGoal(2, new BiteGoal(this, 1.0D, false));
    this.goalSelector.addGoal(3, new BuryAndEnchantItemsGoal(this));
    this.goalSelector.addGoal(4, new FindSpotToBuryGoal(this, 1.0D));
    this.goalSelector.addGoal(5, new GluttonRandomStrollGoal(this));
    this.goalSelector.addGoal(6, new GluttonRandomLookAroundGoal(this));
    this.targetSelector.addGoal(1, (new HurtByTargetGoal(this)));
    this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Glutton.class, true));
    this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, LivingEntity.class, 10, true, false,
                                                                     (entity) -> this.distanceToSqr(
                                                                         entity) < 2 && !(entity instanceof Creeper)));
    EntityUtil.setHealthFromConfig(this, OutvotedConfig.COMMON.MOBS.GLUTTON);
  }

  @Override
  protected void defineSynchedData() {
    super.defineSynchedData();
    this.entityData.define(BURROWED, Boolean.FALSE);
    this.entityData.define(ATTACKING, Boolean.FALSE);
    this.entityData.define(ENCHANTING, Boolean.FALSE);
    this.entityData.define(VARIANT, 0);
  }

  protected SoundEvent getAmbientSound() {
    return SoundRegister.GLUTTON_AMBIENT.get();
  }

  public void addAdditionalSaveData(CompoundTag compound) {
    super.addAdditionalSaveData(compound);
    compound.putInt("Variant", this.getVariant());

    ItemStack item = ItemStack.EMPTY; // Store enchantments in an empty ItemStack
    EnchantmentHelper.setEnchantments(storedEnchants, item);
    CompoundTag compoundNBT = new CompoundTag();
    item.save(compoundNBT);
    compound.put("Enchantments", compoundNBT);
  }

  public void readAdditionalSaveData(CompoundTag compound) {
    super.readAdditionalSaveData(compound);
    this.setVariant(compound.getInt("Variant"));

    ItemStack item = ItemStack.of(compound.getCompound("Enchantments"));
    storedEnchants = EnchantmentHelper.deserializeEnchantments(item.getEnchantmentTags());
  }

  public int getMaxSpawnClusterSize() {
    return 1;
  }

  @Nullable
  public SpawnGroupData finalizeSpawn(ServerLevelAccessor worldIn, DifficultyInstance difficultyIn, MobSpawnType reason,
                                      @Nullable SpawnGroupData spawnDataIn, @Nullable CompoundTag dataTag) {
    Block block = worldIn.getBlockState(this.getBlockPosBelowThatAffectsMyMovement()).getBlock();
    this.setVariant(block == Blocks.SAND ? 0 : block == Blocks.RED_SAND ? 1 : 2);

    return super.finalizeSpawn(worldIn, difficultyIn, reason, spawnDataIn, dataTag);
  }

  public int getVariant() {
    return this.entityData.get(VARIANT);
  }

  public void setVariant(int type) {
    this.entityData.set(VARIANT, type);
  }

  public SoundSource getSoundSource() {
    return SoundSource.HOSTILE;
  }

  /**
   * Called frequently so the entity can update its state every tick as required. For example, zombies and skeletons
   * use this to react to sunlight and start to burn.
   */
  public void aiStep() {
    if (this.isAlive()) {
      this.setInvulnerable(this.isBurrowed());
    }
    super.aiStep();
  }

  @Override
  protected boolean shouldDespawnInPeaceful() {
    return false;
  }

  protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
    return SoundRegister.GLUTTON_HURT.get();
  }

  protected SoundEvent getDeathSound() {
    return SoundRegister.GLUTTON_DEATH.get();
  }

  @Override
  public float getWalkTargetValue(BlockPos pos, LevelReader world) {
    return 0.0F;
  }

  public boolean isBurrowed() {
    return this.entityData.get(BURROWED);
  }

  public void setBurrowed(boolean burrowed) {
    this.entityData.set(BURROWED, burrowed);
  }

  @Override
  protected Component getTypeName() {
    return switch (getVariant()) {
      case 1 -> new TranslatableComponent("entity.outvoted.glutton_r");
      case 2 -> new TranslatableComponent("entity.outvoted.glutton_s");
      default -> super.getTypeName();
    };
  }  public void setAggressive(boolean attacking) {
    this.entityData.set(ATTACKING, attacking);
  }

  /**
   * Returns whether this Entity is invulnerable to the given DamageSource.
   */
  @Override
  public boolean isInvulnerableTo(DamageSource source) {
    return super.isInvulnerableTo(source) && !source.msgId.equals(
        "wither") && !source.isMagic() && !source.isExplosion();
  }

  public boolean isEnchanting() {
    return this.entityData.get(ENCHANTING);
  }  public boolean isAggressive() {
    return this.entityData.get(ATTACKING);
  }

  public void setEnchanting(boolean enchanting) {
    this.entityData.set(ENCHANTING, enchanting);
  }

  public MutablePair<Integer, ItemStack> modifyEnchantments(ItemStack stack, int damage, int count) {
    ItemStack itemstack = stack.copy();
    Map<Enchantment, Integer> cacheEnchants = new ConcurrentHashMap<>(storedEnchants);
    itemstack.removeTagKey("Enchantments");
    itemstack.removeTagKey("StoredEnchantments");
    if (damage > 0) {
      itemstack.setDamageValue(damage);
    } else {
      itemstack.removeTagKey("Damage");
    }

    if (itemstack.getItem() == Items.BOOK) {
      itemstack = new ItemStack(Items.ENCHANTED_BOOK);
    }

    /*
     * left: Status of item, 0 = accepted, 1 = rejected, 2 = enchanted
     * right: Item to return
     */
    MutablePair<Integer, ItemStack> pair = new MutablePair<>(0, itemstack);
    if (storedEnchants.size() <= OutvotedConfig.COMMON.MOBS.GLUTTON.maxEnchants.get()) {
      itemstack.setCount(count);
      final boolean[] hasCurses = {false};
      Map<Enchantment, Integer> map = EnchantmentHelper.getEnchantments(stack).entrySet().stream().filter((enchant) -> {
        if (enchant.getKey().isCurse())
          hasCurses[0] = true;
        return !enchant.getKey().isCurse();
        //return true;
      }).collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

      if (hasCurses[0]) {
        this.addEffect(new MobEffectInstance(MobEffects.WITHER, 600, 1));
        pair.setLeft(1);
        return pair;
      } else if (itemstack.getTag() != null && itemstack.getTag()
          .getBoolean("Bitten") && OutvotedConfig.COMMON.MOBS.GLUTTON.capEnchants.get()) {
        pair.setLeft(1);
        return pair;
      } else if (!(itemstack.isEnchantable() || itemstack.isEnchanted() || itemstack.getItem() instanceof EnchantedBookItem)) {
        pair.setRight(ItemStack.EMPTY);
        return pair;
      } else if (itemstack.getItem().equals(ItemRegister.VOID_HEART)) {
        return pair;
      }

      if (!map.isEmpty()) {
        for (Map.Entry<Enchantment, Integer> entry : map.entrySet()) {
          Enchantment enchantment = entry.getKey();
          Integer level = entry.getValue();
          if (cacheEnchants.containsKey(enchantment)) {
            if (enchantment.getMaxLevel() != 1 && (!OutvotedConfig.COMMON.MOBS.GLUTTON.capEnchants.get() || cacheEnchants.get(
                enchantment) < enchantment.getMaxLevel() + 1)) {
              if (level == enchantment.getMaxLevel() && cacheEnchants.get(enchantment) == enchantment.getMaxLevel()) {
                for (Enchantment ench : cacheEnchants.keySet()) {
                  if (cacheEnchants.get(ench) == ench.getMaxLevel() + 1) {
                    pair.setLeft(1);
                    return pair;
                  }
                }
                cacheEnchants.put(enchantment, enchantment.getMaxLevel() + 1);
              } else if (level.equals(cacheEnchants.get(enchantment))) {
                cacheEnchants.put(enchantment, level + 1);
              } else if (level > cacheEnchants.get(enchantment)) {
                cacheEnchants.put(enchantment, level);
              }
            } else {
              pair.setLeft(1);
              return pair;
            }
          } else if (!storedEnchants.isEmpty()) {
            if (storedEnchants.size() < OutvotedConfig.COMMON.MOBS.GLUTTON.maxEnchants.get()) {
              for (Enchantment ench : storedEnchants.keySet()) {
                if (enchantment instanceof ProtectionEnchantment && ench instanceof ProtectionEnchantment) {
                  if (((ProtectionEnchantment) enchantment).checkCompatibility(ench)) {
                    cacheEnchants.put(enchantment, level);
                  } else {
                    pair.setLeft(1);
                    return pair;
                  }
                } else if (enchantment instanceof ArrowInfiniteEnchantment || enchantment instanceof MendingEnchantment) {
                  cacheEnchants.put(enchantment, level);
                } else if (enchantment instanceof DamageEnchantment && ench instanceof DamageEnchantment) {
                  cacheEnchants.put(enchantment, level);
                } else if (enchantment.isCompatibleWith(ench)) {
                  cacheEnchants.put(enchantment, level);
                } else {
                  pair.setLeft(1);
                  return pair;
                }
              }
            } else {
              pair.setLeft(1);
              return pair;
            }
          } else {
            cacheEnchants.put(enchantment, level);
          }
        }
      } else if (!storedEnchants.isEmpty()) {
        pair.setLeft(2);
        map = storedEnchants;
        storedEnchants = new HashMap<>();
        EnchantmentHelper.setEnchantments(map, itemstack);
        return pair;
      } else {
        pair.setRight(ItemStack.EMPTY);
      }
      storedEnchants = cacheEnchants;
    } else {
      pair.setLeft(1);
    }
    return pair;
  }

  @Override
  public void knockback(double strength, double ratioX, double ratioZ) {
    super.knockback(this.isBurrowed() ? 0 : strength, ratioX, ratioZ);
  }

  @Override
  protected float getSoundVolume() {
    return this.isBurrowed() ? 0.25F : super.getSoundVolume();
  }

  @Override
  public boolean isPushable() {
    return !this.isBurrowed() && super.isPushable();
  }

  /* Determines whether a 3x3 section of ground below is suitable for burrowing */
  public boolean isSuitable(Glutton gluttonIn, @Nullable BlockPos pos) {
    if (this.getEffect(MobEffects.WITHER) != null)
      return false;
    Level world = gluttonIn.level;
    double posX;
    double posY;
    double posZ;
    if (pos == null) {
      posX = gluttonIn.getX();
      posY = gluttonIn.getY();
      posZ = gluttonIn.getZ();
    } else {
      posX = pos.getX();
      posY = pos.getY();
      posZ = pos.getZ();
    }
    boolean ret = true;
    for (double k = posX - 1; k <= posX + 1; ++k) {
      if (ret) {
        for (double l = posZ - 1; l <= posZ + 1; ++l) {
          BlockState block = world.getBlockState(new BlockPos(k, posY - 1, l));
          if (block.is(TagRegister.GLUTTON_CAN_BURROW) && !gluttonIn.isInWater()) {
            if (ret) {
              ret = !gluttonIn.isLeashed();
            }
          } else {
            ret = false;
            break;
          }
        }
      } else {
        break;
      }
    }
    return ret;
  }

  /**
   * Creates a vector based on caclulated direction of one of the 8 cardinal directions the entity is facing
   */
  public net.minecraft.world.phys.Vec3 directionVector() {
    net.minecraft.world.phys.Vec3 vec3d = net.minecraft.world.phys.Vec3.ZERO;
    double rotation = this.getYRot() - 180;
    if (rotation < 0)
      rotation += 360;
    int ordinal = Mth.floor(rotation / 45.0D + 0.5D) & 7;
    for (Direction direction : Direction8.values()[ordinal].getDirections()) {
      vec3d = vec3d.add(direction.getStepX(), direction.getStepY(), direction.getStepZ());
    }
    return vec3d;
  }

  static class GluttonRandomLookAroundGoal extends RandomLookAroundGoal {
    private final Glutton mob;

    public GluttonRandomLookAroundGoal(Glutton entitylivingIn) {
      super(entitylivingIn);
      this.mob = entitylivingIn;
    }

    public boolean canUse() {
      return super.canUse() && !this.mob.isBurrowed();
    }

    public boolean canContinueToUse() {
      return super.canContinueToUse() && !this.mob.isBurrowed();
    }
  }

  static class GluttonRandomStrollGoal extends WaterAvoidingRandomStrollGoal {
    private final Glutton mob;

    public GluttonRandomStrollGoal(Glutton entityIn) {
      super(entityIn, 1.0D, 0.1F);
      this.mob = entityIn;
    }

    public boolean canUse() {
      return super.canUse() && !this.mob.isBurrowed() && !this.mob.isSuitable(this.mob, null);
    }

    public boolean canContinueToUse() {
      return super.canContinueToUse() && !this.mob.isBurrowed() && !this.mob.isSuitable(this.mob, null);
    }

    public void tick() {
      if (this.mob.isBurrowed() || this.mob.isSuitable(this.mob, null))
        this.mob.getNavigation().stop();
    }
  }

  @Override
  public boolean doHurtTarget(Entity entityIn) {
    boolean exec = super.doHurtTarget(entityIn);
    if (exec && entityIn instanceof Player player && OutvotedConfig.COMMON.MOBS.GLUTTON.stealEnchants.get()) {
      List<NonNullList<ItemStack>> allInventories = ImmutableList.of(player.getInventory().items,
                                                                     player.getInventory().armor,
                                                                     player.getInventory().offhand);
      List<ItemStack> enchantedItems = new ArrayList<>();
      for (NonNullList<ItemStack> inv : allInventories) {
        enchantedItems.addAll(inv.stream().filter((item) -> !EnchantmentHelper.getEnchantments(item).isEmpty())
                                  .collect(Collectors.toList()));
      }
      enchantedItems.removeIf((item) -> item.getItem() instanceof AirItem);
      if (!enchantedItems.isEmpty()) {
        ItemStack item = enchantedItems.get(this.random.nextInt(enchantedItems.size()));
        Map<Enchantment, Integer> enchantments = EnchantmentHelper.getEnchantments(item);
        item.removeTagKey("Enchantments");
        item.removeTagKey("StoredEnchantments");
        Object[] enchants = enchantments.keySet().toArray();
        Enchantment enchant = (Enchantment) enchants[this.random.nextInt(enchants.length)];
        if (enchantments.get(enchant) > 1) {
          enchantments.put(enchant, enchantments.get(enchant) - 1);
        } else {
          enchantments.remove(enchant);
        }
        if (enchantments.isEmpty() && item.getItem() instanceof EnchantedBookItem) {
          for (NonNullList<ItemStack> inv : allInventories) {
            if (inv.contains(item)) {
              inv.set(inv.indexOf(item), new ItemStack(Items.BOOK));
            }
          }
        } else {
          EnchantmentHelper.setEnchantments(enchantments, item);
        }
      }
    }
    return exec;
  }






  //  private final AnimationFactory factory = new AnimationFactory(this);
  //
  //  private <E extends IAnimatable> PlayState predicate(AnimationEvent<E> event) {
  //    String animname = event.getController().getCurrentAnimation() != null ? event.getController().getCurrentAnimation().animationName : "";
  //    if (this.isBurrowed()) {
  //      if (this.isEnchanting()) {
  //        event.getController().setAnimation(new AnimationBuilder().addAnimation("bite").addAnimation("biteloop", true));
  //      } else {
  //        if (event.getController().getCurrentAnimation() != null) {
  //          if (animname.equals("idle") || animname.equals("attacking") || animname.equals("chomp") || animname.equals("burrow")) {
  //            event.getController().setAnimation(new AnimationBuilder().addAnimation("burrow").addAnimation("burrowed", true));
  //          } else {
  //            event.getController().setAnimation(new AnimationBuilder().addAnimation("burrowed", true));
  //          }
  //        } else {
  //          event.getController().setAnimation(new AnimationBuilder().addAnimation("burrow").addAnimation("burrowed", true));
  //        }
  //      }
  //    } else {
  //      if (event.getController().getCurrentAnimation() == null || animname.equals("idle") || animname.equals("attacking")) {
  //        if (this.isAggressive()) {
  //          event.getController().setAnimation(new AnimationBuilder().addAnimation("attacking"));
  //        } else {
  //          event.getController().setAnimation(new AnimationBuilder().addAnimation("idle"));
  //        }
  //      } else {
  //        if (this.isAggressive()) {
  //          event.getController().setAnimation(new AnimationBuilder().addAnimation("chomp").addAnimation("attacking"));
  //        } else {
  //          event.getController().setAnimation(new AnimationBuilder().addAnimation("chomp").addAnimation("idle"));
  //        }
  //      }
  //    }
  //    return PlayState.CONTINUE;
  //  }
  //
  //  private <E extends IAnimatable> void soundListener(SoundKeyframeEvent<E> event) {
  //    if (event.sound.equals("chomp")) {
  //      level.playLocalSound(this.getX(), this.getY(), this.getZ(), ModSounds.GLUTTON_BITE, this.getSoundSource(), 1.0F, 1.0F, false);
  //    } else if (event.sound.equals("dig")) {
  //      BlockState block = level.getBlockState(new BlockPos(this.getX(), this.getY() - 0.5D, this.getZ()));
  //      if (block.is(BlockTags.SAND)) {
  //        level.playLocalSound(this.getX(), this.getY(), this.getZ(), ModSounds.GLUTTON_DIG_SAND, SoundSource.BLOCKS, 1.0F, 1.0F, false);
  //      } else {
  //        level.playLocalSound(this.getX(), this.getY(), this.getZ(), ModSounds.GLUTTON_DIG, SoundSource.BLOCKS, 1.0F, 1.0F, false);
  //      }
  //    }
  //  }
  //
  //  private <E extends IAnimatable> void particleListener(ParticleKeyFrameEvent<E> event) {
  //    if (event.effect.equals("dig")) {
  //      for (int i = 0; i < 2; ++i) {
  //        BlockPos blockpos = new BlockPos(this.getX(), this.getY() - 0.5D, this.getZ());
  //        BlockState blockstate = this.level.getBlockState(blockpos);
  //        this.level.addParticle(new BlockParticleOption(ParticleTypes.BLOCK, blockstate), this.getRandomX(0.5D), this.getY(0), this.getRandomZ(0.5D), (this.random.nextDouble() - 0.5D) * 2.0D, this.random.nextDouble(), (this.random.nextDouble() - 0.5D) * 2.0D);
  //      }
  //    }
  //  }
  //
  //  @Override
  //  public void registerControllers(AnimationData data) {
  //    AnimationController<Glutton> controller = new AnimationController<>(this, "controller", 2, this::predicate);
  //    controller.registerSoundListener(this::soundListener);
  //    controller.registerParticleListener(this::particleListener);
  //    data.addAnimationController(controller);
  //  }
  //
  //  @Override
  //  public AnimationFactory getFactory() {
  //    return this.factory;
  //  }
}
