package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.block.entity.BlockEntity;
import one.hbn.outvoted.server.block.entity.BurrowBlockEntity;
import one.hbn.outvoted.server.entity.animal.Meerkat;

public class EnterBurrowGoal extends Goal {
  protected final Meerkat mob;

  public EnterBurrowGoal(Meerkat mob) {
    this.mob = mob;
  }

  public boolean canUse() {
    if (this.mob.hasBurrow() && this.mob.canEnterBurrow()) {
      BlockEntity blockEntity = this.mob.level.getBlockEntity(this.mob.getBurrowPos());
      if (blockEntity instanceof BurrowBlockEntity burrowBlockEntity && this.mob.getBurrowOffsetPos()
          .closerToCenterThan(this.mob.position(), 1.0D)) {
        return !burrowBlockEntity.isFull();
      }
    }

    return false;
  }

  public void start() {
    Level world = this.mob.level;
    BlockEntity blockEntity = world.getBlockEntity(this.mob.getBurrowPos());
    BlockPos pos = this.mob.getBurrowOffsetPos();
    boolean bl = this.mob.distanceToSqr(pos.getX(), pos.getY(), pos.getZ()) < 1.0;
    if (blockEntity instanceof BurrowBlockEntity burrowBlockEntity && bl) {
      burrowBlockEntity.tryEnterBurrow(this.mob, false);
    }
  }
}
