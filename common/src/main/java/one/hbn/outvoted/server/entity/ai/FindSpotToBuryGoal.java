package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.ai.goal.Goal;
import one.hbn.outvoted.server.entity.monster.Glutton;
import org.jetbrains.annotations.Nullable;

import java.util.EnumSet;
import java.util.Random;

public class FindSpotToBuryGoal extends Goal {
  protected final Glutton mob;
  private final double movementSpeed;
  private double spotX;
  private double spotY;
  private double spotZ;

  public FindSpotToBuryGoal(Glutton theCreatureIn, double movementSpeedIn) {
    this.mob = theCreatureIn;
    this.movementSpeed = movementSpeedIn;
    this.setFlags(EnumSet.of(Flag.MOVE));
  }

  public boolean canUse() {
    return this.isPossibleSpot() && this.mob.getTarget() == null && !this.mob.isBurrowed() && !this.mob.isSuitable(
        this.mob, null);
  }

  protected boolean isPossibleSpot() {
    net.minecraft.world.phys.Vec3 vector3d = this.findPossibleSpot();
    if (vector3d == null) {
      return false;
    } else {
      this.spotX = vector3d.x;
      this.spotY = vector3d.y;
      this.spotZ = vector3d.z;
      return true;
    }
  }

  @Nullable
  protected net.minecraft.world.phys.Vec3 findPossibleSpot() {
    Random random = this.mob.getRandom();
    BlockPos blockpos = this.mob.blockPosition();

    for (int i = 0; i < 10; ++i) {
      BlockPos blockpos1 = blockpos.offset(random.nextInt(20) - 10, random.nextInt(3) - 1, random.nextInt(20) - 10);
      if (this.mob.isSuitable(this.mob, blockpos1) && this.mob.getWalkTargetValue(blockpos1) < 0.0F) {
        return net.minecraft.world.phys.Vec3.atBottomCenterOf(blockpos1);
      }
    }

    return null;
  }

  public boolean canContinueToUse() {
    return !this.mob.getNavigation().isDone() && !this.mob.isBurrowed();
  }

  public void start() {
    this.mob.getNavigation().moveTo(this.spotX, this.spotY, this.spotZ, this.movementSpeed);
  }

  public void stop() {
    this.mob.getNavigation().stop();
  }
}
