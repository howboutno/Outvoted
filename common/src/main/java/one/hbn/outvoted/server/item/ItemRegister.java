package one.hbn.outvoted.server.item;

import net.minecraft.core.Registry;
import net.minecraft.world.food.FoodProperties;
import net.minecraft.world.item.CreativeModeTab;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.SpawnEggItem;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.platform.Services;
import one.hbn.outvoted.server.registry.RegistrationProvider;
import one.hbn.outvoted.server.registry.RegistryObject;

import java.util.function.Supplier;

public abstract class ItemRegister {
  public static final RegistrationProvider<Item> ITEMS = RegistrationProvider.get(Registry.ITEM_REGISTRY,
                                                                                  Constants.MOD_ID);
  public static final RegistryObject<SpawnEggItem> WILDFIRE_SPAWN_EGG = registerItem("wildfire_spawn_egg",
                                                                                     () -> Services.REGISTRY.createSpawnEgg(
                                                                                         EntityRegister.WILDFIRE,
                                                                                         0xF6B201, 0x000000));
  public static final RegistryObject<SpawnEggItem> GLUTTON_SPAWN_EGG = registerItem("glutton_spawn_egg",
                                                                                    () -> Services.REGISTRY.createSpawnEgg(
                                                                                        EntityRegister.GLUTTON,
                                                                                        0xF0D786, 0x000000));
  public static final RegistryObject<SpawnEggItem> BARNACLE_SPAWN_EGG = registerItem("barnacle_spawn_egg",
                                                                                     () -> Services.REGISTRY.createSpawnEgg(
                                                                                         EntityRegister.BARNACLE,
                                                                                         0x5B872E, 0x000000));
  public static final RegistryObject<SpawnEggItem> GLARE_SPAWN_EGG = registerItem("glare_spawn_egg",
                                                                                  () -> Services.REGISTRY.createSpawnEgg(
                                                                                      EntityRegister.GLARE, 0x325F03,
                                                                                      0x97D756));
  public static final RegistryObject<SpawnEggItem> MEERKAT_SPAWN_EGG = registerItem("meerkat_spawn_egg",
                                                                                    () -> Services.REGISTRY.createSpawnEgg(
                                                                                        EntityRegister.MEERKAT,
                                                                                        0xC19773, 0x000000));
  public static final RegistryObject<SpawnEggItem> OSTRICH_SPAWN_EGG = registerItem("ostrich_spawn_egg",
                                                                                    () -> Services.REGISTRY.createSpawnEgg(
                                                                                        EntityRegister.OSTRICH,
                                                                                        0xFED39B, 0x000000));
  public static final RegistryObject<WildfireHelmetItem> WILDFIRE_HELMET = registerItem("wildfire_helmet",
                                                                                        () -> new WildfireHelmetItem(
                                                                                            false));
  public static final Item.Properties ITEM_PROPERTIES = new Item.Properties().tab(
      new CreativeModeTab(CreativeModeTab.TABS.length, "outvotedtab") {
        @Override
        public ItemStack makeIcon() {
          return ItemRegister.WILDFIRE_HELMET.get().getDefaultInstance();
        }
      });
  public static final RegistryObject<WildfireShieldItem> WILDFIRE_SHIELD = registerItem("wildfire_shield",
                                                                                        WildfireShieldItem::new);
  public static final RegistryObject<Item> WILDFIRE_SHIELD_PART = registerItem("wildfire_shield_part", ModItem::fire);
  public static final RegistryObject<Item> WILDFIRE_PIECE = registerItem("wildfire_piece", ModItem::fire);
  public static final RegistryObject<Item> VOID_HEART = registerItem("void_heart", ModItem::new);
  public static final RegistryObject<Item> BARNACLE_TOOTH = registerItem("barnacle_tooth", ModItem::new);
  public static final RegistryObject<Item> PRISMARINE_ROD = registerItem("prismarine_rod", ModItem::new);
  public static final RegistryObject<Item> BAOBAB_FRUIT = registerItem("baobab_fruit", () -> new Item(
      ITEM_PROPERTIES.food(new FoodProperties.Builder().nutrition(4).saturationMod(0.6F).build())));
  public static final RegistryObject<WildfireHelmetItem> WILDFIRE_HELMET_SOUL = registerItem("wildfire_helmet_soul",
                                                                                             () -> new WildfireHelmetItem(
                                                                                                 true));

  public static void loadClass() {
    Constants.LOG.debug("Registering Mod Items for " + Constants.MOD_ID);
  }

  public static <T extends Item> RegistryObject<T> registerItem(String name, Supplier<T> item) {
    return ITEMS.register(name, item);
  }

  static class ModItem extends Item {
    ModItem() {
      this(ITEM_PROPERTIES);
    }

    ModItem(Properties properties) {
      super(properties);
    }

    static ModItem fire() {
      return new ModItem(ITEM_PROPERTIES.fireResistant());
    }
  }
}
