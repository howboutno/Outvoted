package one.hbn.outvoted.server.item;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.ShieldItem;

public class WildfireShieldItem extends ShieldItem {
  public WildfireShieldItem() {
    super(ItemRegister.ITEM_PROPERTIES.fireResistant().durability(750));
  }

  public boolean isValidRepairItem(ItemStack stack, ItemStack ingredient) {
    return ingredient.getItem() == ItemRegister.WILDFIRE_PIECE;
  }
}
