package one.hbn.outvoted.server.entity.ai;

import net.minecraft.sounds.SoundEvents;
import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.level.Explosion;
import net.minecraft.world.level.GameRules;
import net.minecraft.world.phys.AABB;
import one.hbn.outvoted.server.entity.monster.Glutton;
import one.hbn.outvoted.server.item.ItemRegister;
import one.hbn.outvoted.server.sounds.SoundRegister;
import org.apache.commons.lang3.tuple.MutablePair;

import java.util.List;

public class BuryAndEnchantItemsGoal extends Goal {
  private final Glutton mob;
  private int tick = 0;
  private ItemStack cacheitem = ItemStack.EMPTY;

  public BuryAndEnchantItemsGoal(Glutton entityIn) {
    this.mob = entityIn;
  }

  public boolean canUse() {
    return !this.mob.isAggressive() && this.mob.isSuitable(this.mob, null);
  }

  public void start() {
    this.mob.setBurrowed(true);
  }

  public void stop() {
    this.tick = 0;
    this.mob.setBurrowed(false);
  }

  public void tick() {
    net.minecraft.world.phys.Vec3 vec3d = this.mob.directionVector().scale(0.6D);
    AABB boundingBox = this.mob.getBoundingBox().expandTowards(vec3d).expandTowards(vec3d.scale(-1.0D));
    List<Entity> entities = this.mob.level.getEntities(this.mob, boundingBox);
    if (!entities.isEmpty()) {
      if (!this.mob.isAggressive() && !this.mob.isEnchanting()) {
        for (Entity entity : entities) {
          if (boundingBox.contains(entity.position())) {
            if (entity instanceof ItemEntity) {
              ItemStack item = ((ItemEntity) entity).getItem();
              if (((ItemEntity) entity).getThrower() != this.mob.getUUID()) {
                this.cacheitem = item.copy();
                this.mob.level.playSound(null, this.mob.getX(), this.mob.getY(), this.mob.getZ(),
                                         SoundRegister.GLUTTON_EAT.get(), this.mob.getSoundSource(), 0.8F, 0.9F);
                entity.discard();
                this.mob.setEnchanting(true);
                break;
              }
            } else if (entity instanceof LivingEntity) {
              if (entity.isAlive() && this.mob.canAttack((LivingEntity) entity) && !this.mob.isAggressive()) {
                this.mob.setBurrowed(false);
                this.mob.setAggressive(true);
                this.mob.doHurtTarget(entity);
              }
            }
          }
        }
      }
    }
    if (this.mob.isEnchanting()) {
      this.tick++;

      if (this.tick % 16 == 0) {
        MutablePair<Integer, ItemStack> pair = this.mob.modifyEnchantments(cacheitem, cacheitem.getDamageValue(), 1);
        ItemStack item = pair.getRight();
        if (cacheitem.getItem().equals(ItemRegister.VOID_HEART)) {
          Explosion.BlockInteraction explosion$mode = this.mob.level.getGameRules()
                                                          .getBoolean(GameRules.RULE_MOBGRIEFING) ?
                                                      Explosion.BlockInteraction.DESTROY :
                                                      Explosion.BlockInteraction.NONE;
          this.mob.level.explode(this.mob, this.mob.getX(), this.mob.getY(), this.mob.getZ(), 2.0F, explosion$mode);
          this.mob.discard();
        }
        if (pair.getLeft() == 0) {
          if (item != ItemStack.EMPTY) {
            this.mob.level.playSound(null, this.mob.getX(), this.mob.getY(), this.mob.getZ(),
                                     SoundEvents.EXPERIENCE_ORB_PICKUP, this.mob.getSoundSource(), 0.8F, 0.6F);
          }
        } else if (pair.getLeft() == 1) {
          this.mob.level.playSound(null, this.mob.getX(), this.mob.getY(), this.mob.getZ(),
                                   SoundRegister.GLUTTON_SPIT.get(), this.mob.getSoundSource(), 0.8F, 0.8F);
          ItemEntity newitem = new ItemEntity(this.mob.level, this.mob.getX(), this.mob.getY(), this.mob.getZ(),
                                              cacheitem);
          newitem.setThrower(this.mob.getUUID());
          this.mob.level.addFreshEntity(newitem);
        } else {
          this.mob.level.playSound(null, this.mob.getX(), this.mob.getY(), this.mob.getZ(), SoundEvents.PLAYER_LEVELUP,
                                   this.mob.getSoundSource(), 0.8F, 0.6F);
          item.getOrCreateTag().putInt("Bitten", 1);
          ItemEntity newitem = new ItemEntity(this.mob.level, this.mob.getX(), this.mob.getY(), this.mob.getZ(), item);
          newitem.setThrower(this.mob.getUUID());
          this.mob.level.addFreshEntity(newitem);
        }
        this.cacheitem = ItemStack.EMPTY;
        this.tick = 0;
        this.mob.setEnchanting(false);
      }
    }
  }
}
