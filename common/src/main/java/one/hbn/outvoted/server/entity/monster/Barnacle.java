package one.hbn.outvoted.server.entity.monster;

import net.minecraft.core.BlockPos;
import net.minecraft.core.particles.ParticleTypes;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.tags.FluidTags;
import net.minecraft.util.Mth;
import net.minecraft.world.Container;
import net.minecraft.world.Containers;
import net.minecraft.world.Difficulty;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.LookControl;
import net.minecraft.world.entity.ai.control.MoveControl;
import net.minecraft.world.entity.ai.goal.*;
import net.minecraft.world.entity.ai.goal.target.NearestAttackableTargetGoal;
import net.minecraft.world.entity.ai.navigation.PathNavigation;
import net.minecraft.world.entity.ai.navigation.WaterBoundPathNavigation;
import net.minecraft.world.entity.animal.Dolphin;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.entity.npc.Villager;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.entity.vehicle.Boat;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelAccessor;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraft.world.level.pathfinder.BlockPathTypes;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.EntityUtil;
import one.hbn.outvoted.server.entity.ai.AttackPlayerInWaterGoal;
import one.hbn.outvoted.server.entity.ai.ChasePlayerInWaterGoal;
import one.hbn.outvoted.server.sounds.SoundRegister;

import java.util.EnumSet;
import java.util.Random;

public class Barnacle extends Monster {
  private static final EntityDataAccessor<Integer> ATTACKING = SynchedEntityData.defineId(Barnacle.class,
                                                                                          EntityDataSerializers.INT);
  private static final EntityDataAccessor<Boolean> DATA_ID_MOVING = SynchedEntityData.defineId(Barnacle.class,
                                                                                               EntityDataSerializers.BOOLEAN);
  private int clientSideAttackTime;
  private boolean clientSideTouchedGround;
  private float clientSideTailAnimation;
  private float clientSideTailAnimationO;
  private float clientSideTailAnimationSpeed;
  private RandomStrollGoal wander;
  private boolean initAttack = false;
  private int attackCounter = 0;

  public Barnacle(EntityType<? extends Barnacle> type, Level worldIn) {
    super(type, worldIn);
    this.xpReward = 10;
    this.setPathfindingMalus(BlockPathTypes.WATER, 0.0F);
    this.moveControl = new BarnacleMoveControl(this);
    this.clientSideTailAnimationO = this.clientSideTailAnimation = this.random.nextFloat();
    EntityUtil.setHealthFromConfig(this, OutvotedConfig.COMMON.MOBS.BARNACLE);
  }

  public static AttributeSupplier.Builder setCustomAttributes() {
    return Monster.createLivingAttributes().add(Attributes.ATTACK_DAMAGE, 6.0D).add(Attributes.MOVEMENT_SPEED, 0.1D)
        .add(Attributes.FOLLOW_RANGE, 48.0D);
  }

  public static boolean canSpawn(EntityType<Barnacle> entity, LevelAccessor world, MobSpawnType spawnReason,
                                 BlockPos blockPos, Random random) {
    boolean yAtOrAboveWorldHeight = blockPos.getY() >= world.getHeight(Heightmap.Types.OCEAN_FLOOR, blockPos.getX(),
                                                                       blockPos.getZ());
    boolean notPeaceful = world.getDifficulty() != Difficulty.PEACEFUL;
    boolean yAtOrBelow45 = blockPos.getY() <= 45.0;
    boolean fromSpawner = spawnReason == MobSpawnType.SPAWNER;
    boolean fromWater = world.getFluidState(blockPos).is(FluidTags.WATER);

    return yAtOrAboveWorldHeight && notPeaceful && yAtOrBelow45 && (fromSpawner || fromWater);
  }

  protected void registerGoals() {
    MoveTowardsRestrictionGoal movetowardsrestrictiongoal = new MoveTowardsRestrictionGoal(this, 1.0D);
    this.wander = new RandomStrollGoal(this, 1.0D, 80);
    this.goalSelector.addGoal(3, new AttackPlayerInWaterGoal(this));
    this.goalSelector.addGoal(4, new ChasePlayerInWaterGoal(this, 6.0D, 48.0F));
    this.goalSelector.addGoal(5, new AvoidEntityGoal<>(this, Barnacle.class, 72.0F, 4.0D, 4.0D));
    this.goalSelector.addGoal(6, movetowardsrestrictiongoal);
    this.goalSelector.addGoal(7, this.wander);
    this.goalSelector.addGoal(8, new LookAtPlayerGoal(this, Player.class, 8.0F));
    this.goalSelector.addGoal(9, new RandomLookAroundGoal(this));
    this.wander.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
    movetowardsrestrictiongoal.setFlags(EnumSet.of(Goal.Flag.MOVE, Goal.Flag.LOOK));
    this.targetSelector.addGoal(1, new NearestAttackableTargetGoal<>(this, Player.class, true));
    this.targetSelector.addGoal(2, new NearestAttackableTargetGoal<>(this, Dolphin.class, true));
    this.targetSelector.addGoal(3, new NearestAttackableTargetGoal<>(this, Villager.class, true));
  }

  protected PathNavigation createNavigation(Level worldIn) {
    return new WaterBoundPathNavigation(this, worldIn);
  }

  @Override
  protected void defineSynchedData() {
    super.defineSynchedData();
    this.entityData.define(ATTACKING, 0);
    this.entityData.define(DATA_ID_MOVING, false);
  }

  @Override
  public int getAmbientSoundInterval() {
    return 160;
  }

  public void baseTick() {
    int i = this.getAirSupply();
    super.baseTick();
    this.updateAir(i);
  }

  protected void updateAir(int air) {
    if (!this.isAlive() || this.isInWaterOrBubble()) {
      this.setAirSupply(300);
      return;
    }

    this.setAirSupply(air - 1);
    if (this.getAirSupply() <= -20) {
      this.setAirSupply(0);
      this.hurt(DamageSource.DROWN, 5.0F);
    }
  }

  @Override
  protected SoundEvent getAmbientSound() {
    return SoundRegister.BARNACLE_AMBIENT.get();
  }

  protected void customServerAiStep() {
    super.customServerAiStep();
    if (!this.hasRestriction()) {
      this.restrictTo(this.blockPosition(), 96);
    }
  }

  public boolean checkSpawnObstruction(LevelReader worldIn) {
    return !worldIn.getEntityCollisions(this, new AABB(this.getOnPos())).isEmpty();
  }

  @Override
  public int getMaxSpawnClusterSize() {
    return 1;
  }

  public RandomStrollGoal getWander() {
    return this.wander;
  }

  @Override
  public boolean canBreatheUnderwater() {
    return true;
  }

  /**
   * Called when the entity is attacked.
   */
  public boolean hurt(DamageSource source, float amount) {
    if (this.wander != null) {
      this.wander.trigger();
    }

    return super.hurt(source, amount);
  }

  @Override
  public void knockback(double strength, double x, double z) {
    super.knockback(strength / 4, x, z);
  }

  @Override
  public MobType getMobType() {
    return MobType.WATER;
  }

  public void travel(Vec3 travelVector) {
    if (!this.isEffectiveAi() || !this.isInWater())
      super.travel(travelVector);

    this.moveRelative(0.1F, travelVector);
    this.move(MoverType.SELF, this.getDeltaMovement());
    this.setDeltaMovement(this.getDeltaMovement().scale(0.9D));
    LivingEntity target = this.getTarget();
    if (this.getSpeed() == 0.0F && target == null) {
      this.setDeltaMovement(this.getDeltaMovement().add(0.0D, -0.001D, 0.0D));
    } else if (target != null) {
      this.setDeltaMovement(this.getDeltaMovement().add(0.0D, -0.005D, 0.0D));
    }
  }

  @Override
  protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn) {
    return sizeIn.height * 0.4F;
  }

  @Override
  public void aiStep() {
    if (this.isAlive()) {
      if (this.level.isClientSide) {
        Vec3 vector3d;
        this.clientSideTailAnimationO = this.clientSideTailAnimation;
        if (!this.isInWater()) {
          this.clientSideTailAnimationSpeed = 0.7f;
          vector3d = this.getDeltaMovement();
          if (vector3d.y > 0.0D && this.clientSideTouchedGround && !this.isSilent()) {
            this.level.playLocalSound(this.getX(), this.getY(), this.getZ(), this.getFlopSound(), this.getSoundSource(),
                                      1.0F, 1.0F, false);
          }

          this.clientSideTouchedGround = vector3d.y < 0.0D && this.level.loadedAndEntityCanStandOn(
              this.blockPosition().below(), this);
        } else {
          this.clientSideTailAnimationSpeed = 0.1f;
        }
        this.clientSideTailAnimation += this.clientSideTailAnimationSpeed;
        if (this.isMoving() && this.isInWater()) {
          vector3d = this.getViewVector(0.0f);
          for (int i = 0; i < 2; ++i) {
            this.level.addParticle(ParticleTypes.BUBBLE, this.getRandomX(0.5) - vector3d.x * 1.5,
                                   this.getRandomY() - vector3d.y * 1.5, this.getRandomZ(0.5) - vector3d.z * 1.5, 0.0,
                                   0.0, 0.0);
          }
        }
      }
      if (this.hasTargetedEntity()) {
        if (this.clientSideAttackTime < this.getAttackDuration()) {
          ++this.clientSideAttackTime;
        }
        this.setYRot(this.yHeadRot);
        LivingEntity livingentity = this.getTargetedEntity();
        if (livingentity != null) {
          this.getLookControl().setLookAt(livingentity, 90.0F, 90.0F);
          this.getLookControl().tick();
          double d5 = this.getAttackAnimationScale(0.0F);
          double d0 = livingentity.getX() - this.getX();
          double d1 = livingentity.getY(0.5D) - this.getEyeY();
          double d2 = livingentity.getZ() - this.getZ();
          double d3 = Math.sqrt(d0 * d0 + d1 * d1 + d2 * d2);
          d0 = d0 / d3;
          d1 = d1 / d3;
          d2 = d2 / d3;
          double d4 = this.random.nextDouble();
          if (this.getAttackPhase() == 0) {
            this.setAttacking(1);
          }

          while (d4 < d3) {
            d4 += 1.8D - d5 + this.random.nextDouble() * (1.7D - d5);
            livingentity.moveTo(this.getX() + d0 * d3, this.getEyeY() + d1, this.getZ() + d2 * d3,
                                livingentity.getYRot(), livingentity.getXRot());
            livingentity.setSwimming(false);
            livingentity.updateSwimming();
            if (!this.level.isClientSide) {
              if (livingentity.isPassenger() && livingentity.getRootVehicle() instanceof Boat) {
                Entity boat = livingentity.getRootVehicle();
                livingentity.stopRiding();
                boat.spawnAtLocation(((Boat) boat).getDropItem());
                try {
                  Containers.dropContents(boat.level, boat, (Container) boat);
                } catch (Exception ignored) {
                }
                boat.discard();
              }
            }
            if (this.getAttackPhase() != 0) {
              if (attackCounter > 10) {
                if (!initAttack) {
                  livingentity.hurt(DamageSource.mobAttack(this), 0.1F);
                  initAttack = true;
                }
              } else {
                attackCounter++;
              }
              livingentity.push(-d0 / 50, -d1 / 50, -d2 / 50);
            } else if (initAttack) {
              initAttack = false;
            } else {
              attackCounter = 0;
            }
          }
        }
      }

      if (this.isInWaterOrBubble()) {
        this.setAirSupply(300);
      } else if (this.onGround) {
        this.setDeltaMovement(this.getDeltaMovement().add((this.random.nextFloat() * 2.0F - 1.0F) * 0.1F, 0.5D,
                                                          (this.random.nextFloat() * 2.0F - 1.0F) * 0.1F));
        this.setYRot(this.random.nextFloat() * 360.0F);
        this.onGround = false;
        this.hasImpulse = true;
      }
      super.aiStep();
    }
  }

  @Override
  protected SoundEvent getHurtSound(DamageSource damageSourceIn) {
    return SoundRegister.BARNACLE_HURT.get();
  }

  @Override
  protected SoundEvent getDeathSound() {
    return SoundRegister.BARNACLE_DEATH.get();
  }

  @Override
  public float getWalkTargetValue(BlockPos pos, LevelReader worldIn) {
    return worldIn.getFluidState(pos).is(FluidTags.WATER) ?
           10.0F + worldIn.getBrightness(pos) - 0.5F :
           super.getWalkTargetValue(pos, worldIn);
  }

  protected SoundEvent getFlopSound() {
    return SoundRegister.BARNACLE_FLOP.get();
  }

  public boolean isMoving() {
    return this.entityData.get(DATA_ID_MOVING);
  }

  public boolean hasTargetedEntity() {
    return this.getTarget() != null;
  }

  public int getAttackDuration() {
    return 80;
  }

  public LivingEntity getTargetedEntity() {
    if (!this.hasTargetedEntity())
      return null;
    return this.getTarget();
  }

  public float getAttackAnimationScale(float f) {
    return ((float) this.clientSideAttackTime + f) / (float) this.getAttackDuration();
  }

  public int getAttackPhase() {
    return this.entityData.get(ATTACKING);
  }

  public void setAttacking(int attacking) {
    this.entityData.set(ATTACKING, attacking);
  }

  void setMoving(boolean moving) {
    this.entityData.set(DATA_ID_MOVING, moving);
  }

  public float getTailAnimationSpeed() {
    return this.clientSideTailAnimationSpeed;
  }

  public float getTailAnimation(float partialTick) {
    return Mth.lerp(partialTick, this.clientSideTailAnimationO, this.clientSideTailAnimation);
  }

  public boolean waterCheck(LivingEntity livingentity) {
    Entity vehicle = livingentity.getVehicle();
    if (vehicle != null)
      return vehicle.isInWater();
    return livingentity.isInWater();
  }

  class BarnacleMoveControl extends MoveControl {
    private final Barnacle mob;

    public BarnacleMoveControl(Barnacle entity) {
      super(entity);
      this.mob = entity;
    }

    public void tick() {
      if (this.operation != Operation.MOVE_TO || this.mob.getNavigation().isDone()) {
        this.mob.setSpeed(0.0F);
        this.mob.setMoving(false);
        return;
      }

      Vec3 vector3d = new Vec3(this.wantedX - this.mob.getX(), this.wantedY - this.mob.getY(),
                               this.wantedZ - this.mob.getZ());
      double d0 = vector3d.length();
      double d1 = vector3d.x / d0;
      double d2 = vector3d.y / d0;
      double d3 = vector3d.z / d0;
      float f = (float) (Mth.atan2(vector3d.z, vector3d.x) * (double) (180F / Mth.PI)) - 90.0F;
      this.mob.setYRot(this.rotlerp(this.mob.getYRot(), f, 90.0F));
      this.mob.yBodyRot = this.mob.getYRot();
      float f1 = (float) (this.speedModifier * this.mob.getAttributeValue(Attributes.MOVEMENT_SPEED));
      float f2 = Mth.lerp(0.125F, this.mob.getSpeed(), f1);
      this.mob.setSpeed(f2);
      double d4 = Math.sin((double) (this.mob.tickCount + this.mob.getId()) * 0.5D) * 0.05D;
      double d5 = Math.cos(this.mob.getYRot() * (Mth.PI / 180F));
      double d6 = Math.sin(this.mob.getYRot() * (Mth.PI / 180F));
      double d7 = Math.sin((double) (this.mob.tickCount + this.mob.getId()) * 0.75D) * 0.05D;
      this.mob.setDeltaMovement(
          this.mob.getDeltaMovement().add(d4 * d5, d7 * (d6 + d5) * 0.25D + (double) f2 * d2 * 0.1D, d4 * d6));
      LookControl lookController = this.mob.getLookControl();
      double d8 = this.mob.getX() + d1 * 2.0D;
      double d9 = this.mob.getEyeY() + d2 / d0;
      double d10 = this.mob.getZ() + d3 * 2.0D;
      double d11 = lookController.getWantedX();
      double d12 = lookController.getWantedY();
      double d13 = lookController.getWantedZ();
      if (!lookController.isLookingAtTarget()) {
        d11 = d8;
        d12 = d9;
        d13 = d10;
      }

      lookController.setLookAt(Mth.lerp(0.125D, d11, d8), Mth.lerp(0.125D, d12, d9), Mth.lerp(0.125D, d13, d10), 10.0F,
                               40.0F);
    }
  }
}