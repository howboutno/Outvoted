package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.core.Direction;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.item.BlockItem;
import net.minecraft.world.item.context.BlockPlaceContext;
import net.minecraft.world.phys.BlockHitResult;
import net.minecraft.world.phys.Vec3;
import one.hbn.outvoted.server.entity.animal.Glare;

public class InDarkSpotGoal extends Goal {
  private int tick;
  private final Glare mob;

  public InDarkSpotGoal(Glare mob) {
    this.mob = mob;
  }

  public boolean canUse() {
    if (this.mob.isDarkSpot(this.mob.blockPosition()))
      this.mob.setDarkPos(this.mob.blockPosition());
    return this.mob.getDarkPos() != null && !this.mob.hasRestriction() && this.mob.blockPosition() == this.mob.getDarkPos();
  }

  public boolean canContinueToUse() {
    return this.canUse();
  }

  public void tick() {
    if (this.mob.getDarkPos() != null && tick >= 5) {
      if (!this.mob.getMainHandItem().isEmpty() && this.mob.getMainHandItem()
          .getItem() instanceof BlockItem blockItem && this.mob.isDarkSpot(this.mob.blockPosition())) {
        BlockPlaceContext placeContext = new BlockPlaceContext(this.mob.level, null, this.mob.getUsedItemHand(),
                                                               this.mob.getMainHandItem(), new BlockHitResult(
            Vec3.atCenterOf(this.mob.blockPosition()), this.mob.getMotionDirection(), this.mob.blockPosition(),
            !this.mob.level.isEmptyBlock(this.mob.blockPosition())));
        InteractionResult actionResult = blockItem.place(placeContext);
        if (!actionResult.consumesAction()) {
          for (Direction direction : Direction.values()) {
            BlockPos blockPos = this.mob.blockPosition().relative(direction);
            if (this.mob.level.isEmptyBlock(blockPos) && this.mob.isDarkSpot(blockPos)) {
              placeContext = new BlockPlaceContext(this.mob.level, null, this.mob.getUsedItemHand(),
                                                   this.mob.getMainHandItem(),
                                                   new BlockHitResult(Vec3.atCenterOf(blockPos),
                                                                      this.mob.getMotionDirection(), blockPos,
                                                                      !this.mob.level.isEmptyBlock(blockPos)));
              actionResult = blockItem.place(placeContext);
              if (actionResult.consumesAction())
                break;
            }
          }
        }
        if (actionResult.consumesAction()) {
          this.mob.getMainHandItem().shrink(1);
          tick = 0;
        }
      } else if (!(this.mob.getMainHandItem().getItem() instanceof BlockItem)) {
        this.mob.spawnAtLocation(this.mob.getMainHandItem().getItem());
        this.mob.getMainHandItem().setCount(0);
      }
    }
    ++tick;
  }
}
