package one.hbn.outvoted.server.block;

import net.minecraft.world.level.block.state.properties.WoodType;

public class OutvotedWoodType extends WoodType {
  public static final OutvotedWoodType PALM = new OutvotedWoodType("palm");
  public static final OutvotedWoodType BAOBAB = new OutvotedWoodType("baobab");

  OutvotedWoodType(String name) {
    super(name);
  }
}
