package one.hbn.outvoted.server.entity.animal;

import net.minecraft.core.BlockPos;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.syncher.EntityDataAccessor;
import net.minecraft.network.syncher.EntityDataSerializers;
import net.minecraft.network.syncher.SynchedEntityData;
import net.minecraft.server.level.ServerLevel;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.sounds.SoundEvent;
import net.minecraft.sounds.SoundEvents;
import net.minecraft.util.Mth;
import net.minecraft.world.InteractionHand;
import net.minecraft.world.InteractionResult;
import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.*;
import net.minecraft.world.entity.ai.attributes.AttributeSupplier;
import net.minecraft.world.entity.ai.attributes.Attributes;
import net.minecraft.world.entity.ai.control.LookControl;
import net.minecraft.world.entity.ai.goal.LookAtPlayerGoal;
import net.minecraft.world.entity.ai.goal.RandomLookAroundGoal;
import net.minecraft.world.entity.ai.goal.RandomStrollGoal;
import net.minecraft.world.entity.animal.AbstractGolem;
import net.minecraft.world.entity.item.ItemEntity;
import net.minecraft.world.entity.player.Player;
import net.minecraft.world.item.AxeItem;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.*;
import net.minecraft.world.level.block.LevelEvent;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.gameevent.GameEvent;
import net.minecraft.world.level.material.Fluids;
import net.minecraft.world.phys.AABB;
import net.minecraft.world.phys.Vec3;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.entity.EntityUtil;
import one.hbn.outvoted.server.entity.ai.PushCopperButtonsGoal;

import java.util.Optional;
import java.util.Random;

public class CopperGolem extends AbstractGolem {
  protected static final EntityDataAccessor<Integer> OXIDIZATION_LEVEL = SynchedEntityData.defineId(CopperGolem.class,
                                                                                                    EntityDataSerializers.INT);
  protected static final EntityDataAccessor<Boolean> WAXED = SynchedEntityData.defineId(CopperGolem.class,
                                                                                        EntityDataSerializers.BOOLEAN);
  protected static final EntityDataAccessor<CompoundTag> ROTATIONS = SynchedEntityData.defineId(CopperGolem.class,
                                                                                                EntityDataSerializers.COMPOUND_TAG);
  protected boolean pushingState = false;

  public CopperGolem(EntityType<? extends CopperGolem> type, Level worldIn) {
    super(type, worldIn);
    this.lookControl = new CopperGolemLookControl(this);
    EntityUtil.setHealthFromConfig(this, OutvotedConfig.COMMON.MOBS.COPPER_GOLEM);
  }

  public static AttributeSupplier.Builder setCustomAttributes() {
    return Mob.createMobAttributes().add(Attributes.MOVEMENT_SPEED, 0.3D).add(Attributes.KNOCKBACK_RESISTANCE, 0.9D);
  }

  public static boolean canSpawn(EntityType<CopperGolem> entity, LevelAccessor world, MobSpawnType spawnReason,
                                 BlockPos blockPos, Random random) {
    return checkMobSpawnRules(entity, world, spawnReason, blockPos, random);
  }

  protected void registerGoals() {
    this.goalSelector.addGoal(1, new PushCopperButtonsGoal(this, 0.8D, 10));
    this.goalSelector.addGoal(2, new CopperGolemRandomStrollGoal(this, 0.7D, 75));
    this.goalSelector.addGoal(3, new CopperGolemLookAtPlayerGoal(this, Player.class, 6.0F));
    this.goalSelector.addGoal(4, new CopperGolemRandomLookAroundGoal(this));
  }

  protected void defineSynchedData() {
    super.defineSynchedData();
    this.entityData.define(OXIDIZATION_LEVEL, 0);
    this.entityData.define(WAXED, false);
    this.entityData.define(ROTATIONS, new CompoundTag());
  }

  @Override
  public void tick() {
    super.tick();
    if (this.level.isClientSide || this.isWaxed() || this.random.nextFloat() >= OutvotedConfig.COMMON.MOBS.COPPER_GOLEM.oxidationRate.get())
      return;

    if (this.tickCount % (int) Math.max(
        (35 * Math.sqrt(this.level.getGameRules().getInt(GameRules.RULE_RANDOMTICKING))) / 3, 1) == 0) {
      this.oxidize();
    }
  }

  public void addAdditionalSaveData(CompoundTag compound) {
    super.addAdditionalSaveData(compound);
    compound.putInt("Oxidization", this.getOxidizationLevel());
    compound.putBoolean("Waxed", this.isWaxed());

    compound.putLongArray("Rotations", this.getRotations().clone());
  }

  public long[] getRotations() {
    long[] rots = this.entityData.get(ROTATIONS).getLongArray("Rotations");
    if (rots.length < 7)
      rots = new long[7];
    return rots;
  }

  public void setRotations(long[] inp) {
    if (inp == null)
      inp = new long[7];
    CompoundTag compound = new CompoundTag();
    compound.putLongArray("Rotations", inp);
    this.entityData.set(ROTATIONS, compound);
  }

  public void readAdditionalSaveData(CompoundTag compound) {
    super.readAdditionalSaveData(compound);
    this.setOxidizationLevel(compound.getInt("Oxidization"));
    this.setWaxed(compound.getBoolean("Waxed"));

    this.setRotations(compound.getLongArray("Rotations"));
  }

  @Override
  public void aiStep() {
    super.aiStep();
    if (this.getOxidizationLevel() == 2) {
      this.setRotations(this.createRot());
    } else if (!this.isNotFrozen()) {
      this.setRot(this.getRotations()[6], this.getXRot());
    }
  }

  public long[] createRot() {
    float limbSwing = this.animationPosition;
    float limbSwingAmount = this.animationSpeed;
    float oxidizeMult = this.getOxidizationMultiplier();
    if (oxidizeMult < 1 && oxidizeMult > 0)
      oxidizeMult += 0.25F;
    int partialTicks = 0;
    float f = Mth.rotLerp(partialTicks, this.yBodyRotO, this.yBodyRot);
    float f1 = Mth.rotLerp(partialTicks, this.yHeadRotO, this.yHeadRot);
    long[] rot = new long[7];
    rot[0] = (long) Mth.lerp(partialTicks, this.xRotO, this.getXRot());
    rot[1] = (long) (f1 - f);
    rot[2] = (long) (Mth.cos(limbSwing * 1.0F * oxidizeMult) * 2.0F * limbSwingAmount);
    rot[3] = (long) (Mth.cos(limbSwing * 1.0F * oxidizeMult + Mth.PI) * 2.0F * limbSwingAmount);
    rot[4] = (long) (Mth.cos(limbSwing * 1.0F * oxidizeMult + Mth.PI) * 2.0F * limbSwingAmount);
    rot[5] = (long) (Mth.cos(limbSwing * 1.0F * oxidizeMult) * 2.0F * limbSwingAmount);
    rot[6] = (long) this.getYRot();

    return rot;
  }

  public boolean isNotFrozen() {
    return this.getOxidizationLevel() < 3;
  }

  @Override
  public void setRot(float yaw, float pitch) {
    super.setRot(yaw, pitch);
    if (!this.isNotFrozen()) {
      this.yHeadRot = yaw;
      this.yBodyRot = yaw;
    }
  }

  public float getOxidizationMultiplier() {
    float mult = switch (this.getOxidizationLevel()) {
      default -> 1.0F;
      case 1 -> 0.75F;
      case 2 -> 0.5F;
      case 3 -> 0.0F;
    };
    return mult;
  }

  @Override
  protected float nextStep() {
    return (float) ((int) this.moveDist + 1);
  }

  protected void playStepSound(BlockPos pos, BlockState state) {
    this.playSound(SoundEvents.IRON_GOLEM_STEP, 1.0F, 1.0F);
  }

  public void thunderHit(ServerLevel world, LightningBolt lightning) {
    this.unFreeze();
    this.setOxidizationLevel(0);
  }

  @Override
  public Vec3 getLeashOffset() {
    return new Vec3(0.0D, this.getEyeHeight() - 0.1D, 0.0D);
  }

  public void unFreeze() {
    this.setNoAi(false);
    this.getNavigation().setSpeedModifier(1);
  }

  public boolean checkSpawnObstruction(LevelReader world) {
    BlockPos blockPos = this.blockPosition();
    BlockPos blockPos2 = blockPos.below();
    BlockState blockState = world.getBlockState(blockPos2);
    if (!blockState.entityCanStandOn(world, blockPos2, this)) {
      return false;
    } else {
      for (int i = 1; i < 2; ++i) {
        BlockPos blockPos3 = blockPos.above(i);
        BlockState blockState2 = world.getBlockState(blockPos3);
        if (!NaturalSpawner.isValidEmptySpawnBlock(world, blockPos3, blockState2, blockState2.getFluidState(),
                                                   EntityRegister.COPPER_GOLEM.get())) {
          return false;
        }
      }

      return NaturalSpawner.isValidEmptySpawnBlock(world, blockPos, world.getBlockState(blockPos),
                                                   Fluids.EMPTY.defaultFluidState(),
                                                   EntityRegister.COPPER_GOLEM.get()) && !world.getEntityCollisions(
          this, new AABB(this.getOnPos())).isEmpty();
    }
  }

  protected InteractionResult mobInteract(Player player, InteractionHand hand) {
    ItemStack itemStack = player.getItemInHand(hand);

    if (player.isCrouching()) {
      boolean is_copper_ingot = itemStack.is(Items.COPPER_INGOT);
      boolean is_axe_item = itemStack.getItem() instanceof AxeItem;
      boolean is_honeycomb = itemStack.is(Items.HONEYCOMB);

      if (!is_copper_ingot && !is_axe_item && !is_honeycomb)
        return InteractionResult.PASS;

      if (is_copper_ingot) {
        float f = this.getHealth();
        this.heal(this.getMaxHealth() / 4);
        if (this.getHealth() == f) {
          return InteractionResult.PASS;
        } else {
          float g = 1.0F + (this.random.nextFloat() - this.random.nextFloat()) * 0.2F;
          this.playSound(SoundEvents.IRON_GOLEM_REPAIR, 1.0F, g);
        }
      } else if (is_axe_item) {
        if (this.isWaxed()) {
          this.setWaxed(false);
          this.playSound(SoundEvents.AXE_WAX_OFF, 1.0F, 1.0F);
          this.level.levelEvent(LevelEvent.PARTICLES_WAX_OFF, this.blockPosition(), 0);
        } else if (this.getOxidizationLevel() > 0) {
          this.deOxidize();
          this.playSound(SoundEvents.AXE_SCRAPE, 1.0F, 1.0F);
          this.level.levelEvent(LevelEvent.PARTICLES_SCRAPE, this.blockPosition(), 0);
        } else {
          return InteractionResult.PASS;
        }
      } else if (!this.isWaxed()) {
        this.setWaxed(true);
        this.playSound(SoundEvents.HONEYCOMB_WAX_ON, 1.0F, 1.0F);
        this.level.levelEvent(LevelEvent.PARTICLES_AND_SOUND_WAX_ON, this.blockPosition(), 0);
      } else {
        return InteractionResult.PASS;
      }
      this.gameEvent(GameEvent.MOB_INTERACT, this.eyeBlockPosition());
      if (!player.isCreative()) {
        if (is_copper_ingot || is_honeycomb)
          itemStack.shrink(1);
        else if (!this.level.isClientSide)
          itemStack.hurt(1, this.random, (ServerPlayer) player);
      }

      return InteractionResult.sidedSuccess(this.level.isClientSide);
    }
    if (!this.level.isClientSide) {
      float playerRot = getYRotD(player).get();
      float rotDiff = playerRot - this.getYRot();
      this.gameEvent(GameEvent.MOB_INTERACT, this.eyeBlockPosition());

      if (rotDiff > 0) {
        if (this.hasItemInSlot(EquipmentSlot.MAINHAND))
          if (!player.isCreative() || this.level.getGameRules().getBoolean(GameRules.RULE_DOMOBLOOT))
            dropItem(this.getMainHandItem());
        this.setItemInHand(InteractionHand.MAIN_HAND, itemStack);
      } else {
        if (!player.isCreative() || this.level.getGameRules().getBoolean(GameRules.RULE_DOMOBLOOT))
          dropItem(this.getOffhandItem());
        this.setItemInHand(InteractionHand.OFF_HAND, itemStack);
      }
    }
    if (!player.isCreative())
      itemStack.shrink(1);

    return InteractionResult.sidedSuccess(this.level.isClientSide);
  }

  public void deOxidize() {
    if (this.getOxidizationLevel() - 1 == 2)
      this.unFreeze();
    this.setOxidizationLevel(this.getOxidizationLevel() - 1);
  }

  protected Optional<Float> getYRotD(Player player) {
    double d = player.getX() - this.getX();
    double e = player.getZ() - this.getZ();
    return !(Math.abs(e) > 9.999999747378752E-6) && !(Math.abs(d) > 9.999999747378752E-6) ?
           Optional.empty() :
           Optional.of((float) (Mth.atan2(d, e) * 57.2957763671875) - 180F);
  }

  private void dropItem(ItemStack stack) {
    ItemEntity itemEntity = new ItemEntity(this.level, this.getX(), this.getY(), this.getZ(), stack);
    this.level.addFreshEntity(itemEntity);
  }

  public boolean isWaxed() {
    return this.entityData.get(WAXED);
  }

  public void setWaxed(boolean waxed) {
    this.entityData.set(WAXED, waxed);
  }

  public void oxidize() {
    if (this.getOxidizationLevel() < 3)
      this.setOxidizationLevel(this.getOxidizationLevel() + 1);
    if (this.getOxidizationLevel() == 3)
      this.freeze();
  }

  public int getOxidizationLevel() {
    return this.entityData.get(OXIDIZATION_LEVEL);
  }

  public void setOxidizationLevel(int level) {
    this.entityData.set(OXIDIZATION_LEVEL, level);
  }

  public void freeze() {
    this.setOxidizationLevel(3);
    this.setNoAi(true);
    this.getNavigation().setSpeedModifier(0);
    this.getNavigation().stop();
    this.getMoveControl().setWantedPosition(this.getX(), this.getY(), this.getZ(), 0);
    this.getMoveControl().tick();
  }

  public void setPushingState(boolean state) {
    this.pushingState = state;
  }

  protected SoundEvent getHurtSound(DamageSource source) {
    return SoundEvents.IRON_GOLEM_HURT;
  }

  protected SoundEvent getDeathSound() {
    return SoundEvents.IRON_GOLEM_DEATH;
  }

  protected int decreaseAirSupply(int air) {
    return air;
  }

  @Override
  public void push(Entity entity) {
    if (this.isNotFrozen()) {
      super.push(entity);
    }
  }

  @Override
  public float getSpeed() {
    return super.getSpeed() * this.getOxidizationMultiplier();
  }

  protected void doPush(Entity entity) {
    if (this.isNotFrozen()) {
      super.doPush(entity);
    }
  }

  @Override
  public boolean isPushable() {
    return super.isPushable() && this.isNotFrozen();
  }

  @Override
  protected float getStandingEyeHeight(Pose poseIn, EntityDimensions sizeIn) {
    return 0.7F;
  }

  static class CopperGolemRandomLookAroundGoal extends RandomLookAroundGoal {
    private final CopperGolem mob;

    public CopperGolemRandomLookAroundGoal(CopperGolem mob) {
      super(mob);
      this.mob = mob;
    }

    public boolean canUse() {
      return this.mob.isNotFrozen() && super.canUse();
    }

    public boolean canContinueToUse() {
      return this.mob.isNotFrozen() && super.canContinueToUse();
    }
  }

  static class CopperGolemLookAtPlayerGoal extends LookAtPlayerGoal {
    private final CopperGolem mob;

    public CopperGolemLookAtPlayerGoal(CopperGolem mobEntity, Class<? extends LivingEntity> clazz, float f) {
      super(mobEntity, clazz, f);
      this.mob = mobEntity;
    }

    public boolean canUse() {
      return this.mob.isNotFrozen() && super.canUse();
    }

    public boolean canContinueToUse() {
      return this.mob.isNotFrozen() && super.canContinueToUse();
    }
  }

  static class CopperGolemRandomStrollGoal extends RandomStrollGoal {
    private final CopperGolem mob;

    public CopperGolemRandomStrollGoal(CopperGolem pathAwareEntity, double d, int i) {
      super(pathAwareEntity, d, i);
      this.mob = pathAwareEntity;
    }

    public boolean canUse() {
      return this.mob.isNotFrozen() && super.canUse();
    }

    public boolean canContinueToUse() {
      return this.mob.isNotFrozen() && super.canContinueToUse();
    }
  }

  class CopperGolemLookControl extends LookControl {
    public CopperGolemLookControl(Mob mobEntity) {
      super(mobEntity);
    }

    public void tick() {
      if (CopperGolem.this.isNotFrozen())
        super.tick();
    }
  }
}

