package one.hbn.outvoted.server.entity.ai;

import net.minecraft.core.BlockPos;
import net.minecraft.world.entity.PathfinderMob;
import net.minecraft.world.entity.ai.goal.Goal;
import net.minecraft.world.entity.ai.util.AirAndWaterRandomPos;
import net.minecraft.world.entity.ai.util.HoverRandomPos;
import net.minecraft.world.phys.Vec3;

import java.util.EnumSet;

public class RandomHoverGoal extends Goal {
  private static final int MAX_DISTANCE = 32;
  private final PathfinderMob mob;

  public RandomHoverGoal(PathfinderMob mob) {
    this.mob = mob;
    this.setFlags(EnumSet.of(Flag.MOVE));
  }

  public boolean canUse() {
    return this.mob.getNavigation().isDone() && this.mob.getRandom().nextInt(20) == 0;
  }

  public boolean canContinueToUse() {
    return this.mob.getNavigation().isInProgress();
  }

  public void start() {
    Vec3 vec3d = this.getRandomLocation();
    if (vec3d != null) {
      this.mob.getNavigation().moveTo(this.mob.getNavigation().createPath(new BlockPos(vec3d), 1), 1.0D);
    }
  }

  private Vec3 getRandomLocation() {
    Vec3 vec3d3 = this.mob.getViewVector(0.0F);
    Vec3 vec3d4 = HoverRandomPos.getPos(this.mob, 8, 7, vec3d3.x, vec3d3.z, 1.5707964F, 3, 1);
    return vec3d4 != null ?
           vec3d4 :
           AirAndWaterRandomPos.getPos(this.mob, 8, 4, -2, vec3d3.x, vec3d3.z, 1.5707963705062866D);
  }
}