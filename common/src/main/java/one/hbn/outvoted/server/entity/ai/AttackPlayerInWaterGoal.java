package one.hbn.outvoted.server.entity.ai;

import net.minecraft.world.damagesource.DamageSource;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.entity.ai.goal.Goal;
import one.hbn.outvoted.server.entity.monster.Barnacle;

import java.util.EnumSet;

public class AttackPlayerInWaterGoal extends Goal {
  private final Barnacle mob;
  private int tickCounter;
  private boolean hasAttacked;
  private final float cachedHealth;

  public AttackPlayerInWaterGoal(Barnacle entity) {
    this.mob = entity;
    this.cachedHealth = entity.getHealth();
    this.setFlags(EnumSet.of(Flag.MOVE, Flag.LOOK));
  }

  public boolean canUse() {
    LivingEntity target = this.mob.getTarget();
    return target != null && target.isAlive() && this.mob.waterCheck(target) && this.mob.distanceToSqr(
        this.mob.getTarget()) < 64.0D;
  }

  public boolean canContinueToUse() {
    LivingEntity target = this.mob.getTarget();
    return target != null && this.cachedHealth - target.getHealth() > 3.0F && this.mob.distanceToSqr(
        target) < 90.5D && this.mob.waterCheck(target);
  }

  public void start() {
    this.tickCounter = -1;
    this.mob.getNavigation().stop();
    LivingEntity target = this.mob.getTarget();
    if (target != null) {
      this.mob.getLookControl().setLookAt(this.mob.getTarget(), 90.0F, 90.0F);
    }
    this.mob.hasImpulse = true;
    this.hasAttacked = false;
  }

  public void stop() {
    this.mob.setTarget(null);
    this.mob.getWander().trigger();
    this.mob.setAttacking(0);
    this.hasAttacked = false;
  }

  public void tick() {
    LivingEntity target = this.mob.getTarget();
    if (target == null)
      return;

    this.mob.getNavigation().stop();
    this.mob.getLookControl().setLookAt(target, 90.0F, 90.0F);

    if (!this.mob.hasLineOfSight(target)) {
      this.mob.setTarget(null);
      return;
    }

    if (this.mob.getAttackPhase() < 1)
      this.mob.setAttacking(1);
    if (++this.tickCounter == 0) {
      this.mob.setTarget(target);
      return;
    }

    if (this.tickCounter < this.mob.getAttackDuration())
      return;
    if (this.tickCounter >= 600) {
      this.mob.setAttacking(2);
      if (this.tickCounter % 5 == 0)
        target.hurt(DamageSource.mobAttack(this.mob), 2.0F);
    } else if (this.tickCounter % 40 == 0 && this.mob.getAttackPhase() == 1) {
      this.mob.setAttacking(3);
    } else if (this.mob.getAttackPhase() == 3 && this.tickCounter % 6 == 0) {
      if (!this.hasAttacked) {
        target.hurt(DamageSource.mobAttack(this.mob), 2.0F);
      } else {
        this.mob.setAttacking(1);
      }
      this.hasAttacked = !this.hasAttacked;
    }
  }
}
