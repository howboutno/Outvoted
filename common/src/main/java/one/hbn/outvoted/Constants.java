package one.hbn.outvoted;

import net.minecraft.resources.ResourceLocation;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Constants {

  public static final String MOD_ID = "outvoted";
  public static final String MOD_NAME = "Outvoted";
  public static final Logger LOG = LogManager.getLogger(MOD_NAME);

  public static ResourceLocation loc(String path) {
    return new ResourceLocation(MOD_ID, path);
  }
}