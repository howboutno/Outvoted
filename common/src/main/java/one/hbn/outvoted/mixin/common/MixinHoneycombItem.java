package one.hbn.outvoted.mixin.common;

import com.google.common.collect.BiMap;
import com.google.common.collect.ImmutableBiMap;
import net.minecraft.world.item.HoneycombItem;
import net.minecraft.world.level.block.Block;
import one.hbn.outvoted.server.block.BlockRegister;
import org.spongepowered.asm.mixin.Dynamic;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(HoneycombItem.class)
public abstract class MixinHoneycombItem {
  @Dynamic
  @Inject(method = "method_34722", at = @At("RETURN"), remap = false, cancellable = true)
  private static void onBuildWaxables(CallbackInfoReturnable<BiMap<Block, Block>> cir) {
    var builder = ImmutableBiMap.<Block, Block>builder().putAll(cir.getReturnValue())
        .put(BlockRegister.COPPER_BUTTON.get(), BlockRegister.WAXED_COPPER_BUTTON.get())
        .put(BlockRegister.EXPOSED_COPPER_BUTTON.get(), BlockRegister.WAXED_EXPOSED_COPPER_BUTTON.get())
        .put(BlockRegister.WEATHERED_COPPER_BUTTON.get(), BlockRegister.WAXED_WEATHERED_COPPER_BUTTON.get())
        .put(BlockRegister.OXIDIZED_COPPER_BUTTON.get(), BlockRegister.WAXED_OXIDIZED_COPPER_BUTTON.get());
    cir.setReturnValue(builder.build());
  }
}
