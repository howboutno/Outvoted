package one.hbn.outvoted.mixin.common;

import net.minecraft.advancements.CriteriaTriggers;
import net.minecraft.core.BlockPos;
import net.minecraft.server.level.ServerPlayer;
import net.minecraft.world.level.Level;
import net.minecraft.world.level.LevelReader;
import net.minecraft.world.level.block.Block;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.block.CarvedPumpkinBlock;
import net.minecraft.world.level.block.state.BlockState;
import net.minecraft.world.level.block.state.pattern.BlockInWorld;
import net.minecraft.world.level.block.state.pattern.BlockPattern;
import net.minecraft.world.level.block.state.pattern.BlockPatternBuilder;
import net.minecraft.world.level.block.state.predicate.BlockMaterialPredicate;
import net.minecraft.world.level.block.state.predicate.BlockStatePredicate;
import net.minecraft.world.level.material.Material;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.entity.animal.CopperGolem;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

import java.util.function.Predicate;

@Mixin(CarvedPumpkinBlock.class)
public class MixinCarvedPumpkinBlock {
  @Unique
  private static final Predicate<BlockState> COPPER_PREDICATE;
  @Shadow
  @Final
  private static Predicate<BlockState> PUMPKINS_PREDICATE;

  static {
    COPPER_PREDICATE = (state) -> state != null && (state.is(Blocks.COPPER_BLOCK) || state.is(
        Blocks.WEATHERED_COPPER) || state.is(Blocks.EXPOSED_COPPER) || state.is(Blocks.OXIDIZED_COPPER));
  }

  @Unique
  private BlockPattern outvoted$copperGolemBase;
  @Unique
  private BlockPattern outvoted$copperGolemFull;

  @Inject(method = "canSpawnGolem", at = @At("TAIL"))
  private void inject(LevelReader levelReader, BlockPos pos, CallbackInfoReturnable<Boolean> cir) {
    Boolean b = cir.getReturnValue();
    cir.setReturnValue(b || this.outvoted$getOrCreateCopperGolemBase().find(levelReader, pos) != null);
  }

  @Unique
  private BlockPattern outvoted$getOrCreateCopperGolemBase() {
    if (this.outvoted$copperGolemBase == null) {
      this.outvoted$copperGolemBase = BlockPatternBuilder.start().aisle("~ ~", "$#$")
          .where('#', BlockInWorld.hasState(COPPER_PREDICATE))
          .where('$', BlockInWorld.hasState(BlockStatePredicate.forBlock(Blocks.LIGHTNING_ROD)))
          .where('~', BlockInWorld.hasState(BlockMaterialPredicate.forMaterial(Material.AIR))).build();
    }

    return this.outvoted$copperGolemBase;
  }

  @Inject(method = "trySpawnGolem", at = @At("TAIL"))
  private void inject(Level level, BlockPos pos, CallbackInfo ci) {
    BlockPattern.BlockPatternMatch match = this.outvoted$getOrCreateCopperGolemFull().find(level, pos);
    if (match != null) {
      for (int i = 0; i < this.outvoted$getOrCreateCopperGolemFull().getWidth(); ++i) {
        for (int j = 0; j < this.outvoted$getOrCreateCopperGolemFull().getHeight(); ++j) {
          BlockInWorld block = match.getBlock(i, j, 0);
          level.setBlock(block.getPos(), Blocks.AIR.defaultBlockState(), 2);
          level.levelEvent(2001, block.getPos(), Block.getId(block.getState()));
        }
      }

      BlockPos pos1 = match.getBlock(1, 1, 0).getPos();
      CopperGolem copperGolem = EntityRegister.COPPER_GOLEM.get().create(level);
      copperGolem.moveTo((double) pos1.getX() + 0.5, (double) pos1.getY() + 0.05, (double) pos1.getZ() + 0.5, 0.0F,
                         0.0F);
      level.addFreshEntity(copperGolem);

      for (ServerPlayer serverPlayer : level.getEntitiesOfClass(ServerPlayer.class,
                                                                copperGolem.getBoundingBox().inflate(5.0))) {
        CriteriaTriggers.SUMMONED_ENTITY.trigger(serverPlayer, copperGolem);
      }

      for (int i = 0; i < this.outvoted$getOrCreateCopperGolemFull().getWidth(); ++i) {
        for (int j = 0; j < this.outvoted$getOrCreateCopperGolemFull().getHeight(); ++j) {
          BlockInWorld block = match.getBlock(i, j, 0);
          level.blockUpdated(block.getPos(), Blocks.AIR);
        }
      }
    }
  }

  @Unique
  private BlockPattern outvoted$getOrCreateCopperGolemFull() {
    if (this.outvoted$copperGolemFull == null) {
      this.outvoted$copperGolemFull = BlockPatternBuilder.start().aisle("~^~", "$#$")
          .where('^', BlockInWorld.hasState(PUMPKINS_PREDICATE)).where('#', BlockInWorld.hasState(COPPER_PREDICATE))
          .where('$', BlockInWorld.hasState(BlockStatePredicate.forBlock(Blocks.LIGHTNING_ROD)))
          .where('~', BlockInWorld.hasState(BlockMaterialPredicate.forMaterial(Material.AIR))).build();
    }

    return this.outvoted$copperGolemFull;
  }
}
