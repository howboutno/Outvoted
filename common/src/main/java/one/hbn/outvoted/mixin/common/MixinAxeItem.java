package one.hbn.outvoted.mixin.common;

import net.minecraft.world.item.AxeItem;
import net.minecraft.world.level.block.state.BlockState;
import one.hbn.outvoted.server.block.WeatheringCopperButton;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.ModifyVariable;

import java.util.Optional;

@Mixin(AxeItem.class)
public abstract class MixinAxeItem {
  //  @Redirect(method = "useOn", at = @At(value = "INVOKE", target = "Lnet/minecraft/world/level/block/WeatheringCopper;getPrevious(Lnet/minecraft/world/level/block/state/BlockState;)Ljava/util/Optional;"))
  //  public Optional<BlockState> redirected(BlockState state) {
  //    return WeatheringCopperButton.getPrevious(state);
  //  }

  @ModifyVariable(method = "useOn", at = @At("STORE"), ordinal = 1)
  private Optional<BlockState> injected(Optional<BlockState> optionalBlockState) {
    return optionalBlockState.isPresent() ?
           WeatheringCopperButton.getPrevious(optionalBlockState.get()) :
           optionalBlockState;
  }
}
