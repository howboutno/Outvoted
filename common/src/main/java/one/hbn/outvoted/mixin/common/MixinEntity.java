package one.hbn.outvoted.mixin.common;

import net.minecraft.world.entity.Entity;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.LivingEntity;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.item.WildfireHelmetItem;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.Constant;
import org.spongepowered.asm.mixin.injection.ModifyConstant;

@Mixin(Entity.class)
public abstract class MixinEntity {
  @ModifyConstant(method = "baseTick", constant = @Constant(intValue = 20))
  private int delayFireTick(int value) {
    if ((Object) this instanceof LivingEntity entity) {
      if (entity.getItemBySlot(EquipmentSlot.HEAD).getItem() instanceof WildfireHelmetItem)
        return OutvotedConfig.COMMON.MISC.helmetFireTicks.get();
    }
    return value;
  }
}
