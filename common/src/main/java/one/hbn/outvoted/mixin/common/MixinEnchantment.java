package one.hbn.outvoted.mixin.common;

import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;
import net.minecraft.world.item.enchantment.MendingEnchantment;
import one.hbn.outvoted.server.item.WildfireHelmetItem;
import org.spongepowered.asm.mixin.Final;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Shadow;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

@Mixin(Enchantment.class)
public class MixinEnchantment {
  @Shadow
  @Final
  public EnchantmentCategory category;

  @Inject(method = "canEnchant", at = @At(value = "TAIL"), cancellable = true)
  private void canEnchantHelmet(ItemStack stack, CallbackInfoReturnable<Boolean> cir) {
    if ((Object) this instanceof MendingEnchantment)
      if (stack.getItem() instanceof WildfireHelmetItem)
        cir.setReturnValue(false);
  }
}
