package one.hbn.outvoted.mixin.client;

import net.minecraft.client.renderer.entity.BlazeRenderer;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.entity.monster.Blaze;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.EntityUtil;
import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.Unique;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfoReturnable;

/**
 * Replace Blaze textures with soul/blue variant when in Soul Sand Valleys, akin to Wildfires
 */
@Mixin(BlazeRenderer.class)
public abstract class MixinBlazeRenderer {
  @Unique
  private static final ResourceLocation BLAZE_TEXTURES_SOUL = Constants.loc("textures/entity/soul_blaze.png");

  @Inject(method = "getTextureLocation*", at = @At("RETURN"), cancellable = true)
  private void soulTextures(Blaze entity, CallbackInfoReturnable<ResourceLocation> cir) {
    if (!OutvotedConfig.CLIENT.variants.get())
      return;
    if (((EntityUtil.IBlazeVariant) entity).outvoted$getVariant() == 1)
      cir.setReturnValue(BLAZE_TEXTURES_SOUL);
  }
}
