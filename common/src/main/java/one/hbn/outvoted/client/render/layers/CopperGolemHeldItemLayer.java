package one.hbn.outvoted.client.render.layers;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.math.Vector3f;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.Minecraft;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.block.model.ItemTransforms;
import net.minecraft.client.renderer.entity.RenderLayerParent;
import net.minecraft.client.renderer.entity.layers.RenderLayer;
import net.minecraft.world.entity.HumanoidArm;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.item.ItemStack;
import one.hbn.outvoted.client.model.CopperGolemModel;
import one.hbn.outvoted.server.entity.animal.CopperGolem;

@Environment(EnvType.CLIENT)
public class CopperGolemHeldItemLayer extends RenderLayer<CopperGolem, CopperGolemModel> {
  public CopperGolemHeldItemLayer(RenderLayerParent<CopperGolem, CopperGolemModel> arg) {
    super(arg);
  }

  // TODO: Fix slightly offset item positioning/rotation
  @Override
  public void render(PoseStack matrixStack, MultiBufferSource buffer, int packedLight, CopperGolem livingEntity,
                     float limbSwing, float limbSwingAmount, float partialTicks, float ageInTicks, float netHeadYaw,
                     float headPitch) {
    ItemStack itemStack2;
    boolean bl = ((LivingEntity) livingEntity).getMainArm() == HumanoidArm.RIGHT;
    ItemStack itemStack = bl ? livingEntity.getOffhandItem() : (livingEntity).getMainHandItem();
    ItemStack itemStack3 = itemStack2 = bl ? livingEntity.getMainHandItem() : livingEntity.getOffhandItem();
    if (itemStack.isEmpty() && itemStack2.isEmpty()) {
      return;
    }
    matrixStack.pushPose();
    matrixStack.scale(0.5f, 0.5f, 0.5f);
    matrixStack.translate(0f, 2.25f, 0f);
    this.renderArmWithItem(livingEntity, itemStack2, ItemTransforms.TransformType.THIRD_PERSON_RIGHT_HAND,
                           HumanoidArm.RIGHT, matrixStack, buffer, packedLight);
    this.renderArmWithItem(livingEntity, itemStack, ItemTransforms.TransformType.THIRD_PERSON_LEFT_HAND,
                           HumanoidArm.LEFT, matrixStack, buffer, packedLight);
    matrixStack.popPose();
  }

  protected void renderArmWithItem(LivingEntity livingEntity, ItemStack itemStack,
                                   ItemTransforms.TransformType transformType, HumanoidArm arm, PoseStack poseStack,
                                   MultiBufferSource buffer, int packedLight) {
    if (itemStack.isEmpty()) {
      return;
    }
    poseStack.pushPose();
    ((ArmedModel) this.getParentModel()).translateToHand(arm, poseStack);
    poseStack.mulPose(Vector3f.XP.rotationDegrees(-90.0f));
    poseStack.mulPose(Vector3f.YP.rotationDegrees(180.0f));
    boolean bl = arm == HumanoidArm.LEFT;
    poseStack.translate((bl ? -1 : 1) * 0.5D, 0D, -1.25D);
    Minecraft.getInstance().getItemInHandRenderer()
        .renderItem(livingEntity, itemStack, transformType, bl, poseStack, buffer, packedLight);
    poseStack.popPose();
  }

  //    if (bone.getName().equals("righthand")) {
  //    stack.pushPose();
  //    stack.mulPose(Vector3f.XP.rotationDegrees(-90));
  //    stack.mulPose(Vector3f.YP.rotationDegrees(0));
  //    stack.mulPose(Vector3f.ZP.rotationDegrees(0));
  //    stack.translate(0.48D, 0.0D, 0.0D);
  //    stack.scale(1.0f, 1.0f, 1.0f);
  //    Minecraft.getInstance().getItemRenderer().renderStatic(mainHand, ItemTransforms.TransformType.THIRD_PERSON_RIGHT_HAND, packedLightIn, packedOverlayIn, stack, this.rtb, 0);
  //    stack.popPose();
  //    bufferIn = rtb.getBuffer(RenderType.entityTranslucent(whTexture));
  //  } else if (bone.getName().equals("lefthand")) {
  //    stack.pushPose();
  //    stack.mulPose(Vector3f.XP.rotationDegrees(0));
  //    stack.mulPose(Vector3f.YP.rotationDegrees(0));
  //    stack.mulPose(Vector3f.ZP.rotationDegrees(0));
  //    stack.translate(-0.48D, -0.225D, -0.275D);
  //    stack.scale(1.0f, 1.0f, 1.0f);
  //    Minecraft.getInstance().getItemRenderer().renderStatic(offHand, ItemTransforms.TransformType.THIRD_PERSON_LEFT_HAND, packedLightIn, packedOverlayIn, stack, this.rtb, 0);
  //    stack.popPose();
  //    bufferIn = rtb.getBuffer(RenderType.entityTranslucent(whTexture));
  //  }
}
