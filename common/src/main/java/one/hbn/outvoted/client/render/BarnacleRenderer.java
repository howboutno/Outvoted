package one.hbn.outvoted.client.render;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import com.mojang.math.Matrix3f;
import com.mojang.math.Matrix4f;
import com.mojang.math.Vector3f;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.renderer.MultiBufferSource;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.client.renderer.texture.OverlayTexture;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.LivingEntity;
import net.minecraft.world.phys.Vec3;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.client.model.BarnacleModel;
import one.hbn.outvoted.server.entity.monster.Barnacle;

@Environment(EnvType.CLIENT)
public class BarnacleRenderer extends MobRenderer<Barnacle, BarnacleModel> {
  private static final ResourceLocation BARNACLE_TONGUE_LOCATION = Constants.loc(
      "textures/entity/barnacle/barnacle_tongue.png");
  private static final RenderType RENDER_TYPE = RenderType.entityCutoutNoCull(BARNACLE_TONGUE_LOCATION);

  public BarnacleRenderer(EntityRendererProvider.Context ctx) {
    super(ctx, new BarnacleModel(ctx.bakeLayer(BarnacleModel.LAYER_LOCATION)), 1.0F);
  }

  @Override
  public ResourceLocation getTextureLocation(Barnacle entity) {
    return Constants.loc("textures/entity/barnacle/barnacle.png");
  }

  /**
   * Taken from the Guardian. It had no helpful variable names so I used ~~AI~~ to try to translate them.
   * <p>
   * In the Guardian it renders a beam from the mob pos to the player pos,
   * which is exactly what we need the tongue to do! Thanks Mojang
   * TODO: Somehow tweak end pos so tongue doesn't spear player
   */
  @Override
  public void render(Barnacle entity, float entityYaw, float partialTicks, PoseStack matrixStack,
                     MultiBufferSource buffer, int packedLight) {
    super.render(entity, entityYaw, partialTicks, matrixStack, buffer, packedLight);
    LivingEntity target = entity.getTargetedEntity(); // Original: livingEntity
    if (target != null) {
      float gameTime = (float) entity.level.getGameTime() + partialTicks; // Original: g
      float eyeHeight = entity.getEyeHeight(); // Original: i
      matrixStack.pushPose();
      matrixStack.translate(0.0, eyeHeight, 0.0);
      Vec3 targetPosition = this.getPosition(target, (double) target.getBbHeight() * 0.5,
                                             partialTicks); // Original: vec3
      Vec3 entityPosition = this.getPosition(entity, eyeHeight, partialTicks); // Original: vec32
      Vec3 positionDifference = targetPosition.subtract(entityPosition); // Original: vec33
      float length = (float) (positionDifference.length()); // Original: j
      positionDifference = positionDifference.normalize(); // Original: vec33
      float acosY = (float) Math.acos(positionDifference.y); // Original: k
      float atan2Zx = (float) Math.atan2(positionDifference.z, positionDifference.x); // Original: l
      matrixStack.mulPose(Vector3f.YP.rotationDegrees((1.5707964f - atan2Zx) * 57.295776f)); // Original: l
      matrixStack.mulPose(Vector3f.XP.rotationDegrees(acosY * 57.295776f)); // Original: k
      float negativeScaledGameTime = gameTime * 0.05f * -1.5f; // Original: n
      float w = Mth.cos(negativeScaledGameTime + 0.7853982f) * 0.1f;
      float x = Mth.sin(negativeScaledGameTime + 0.7853982f) * 0.1f;
      float length1 = length; // Original: ak
      VertexConsumer vertexConsumer = buffer.getBuffer(RENDER_TYPE);
      PoseStack.Pose pose = matrixStack.last();
      Matrix4f matrix4f = pose.pose();
      Matrix3f matrix3f = pose.normal();
      float radius = 0.05F;
      float rgb = 0.125f;
      renderPart(matrixStack, vertexConsumer, rgb, rgb, rgb, 1.0f, 0.0f, length1, 0.0f, radius, radius, 0.0f, -radius,
                 0.0f, 0.0f, -radius, 0.0f, 1.0f, 0.0F, 1.0F);

      vertex(vertexConsumer, matrix4f, matrix3f, 0.0F, length1, -radius, rgb, 0.0F,
             w); // Original: u, ak, v, p, q, r, 0.5f, ap + 0.5f
      vertex(vertexConsumer, matrix4f, matrix3f, -radius, length1, 0.0F, rgb, 1.0f,
             w); // Original: w, ak, x, p, q, r, 1.0f, ap + 0.5f
      vertex(vertexConsumer, matrix4f, matrix3f, 0.0F, length1, radius, rgb, 1.0f,
             x); // Original: aa, ak, ab, p, q, r, 1.0f, ap
      vertex(vertexConsumer, matrix4f, matrix3f, radius, length1, 0.0F, rgb, 0.0F,
             x); // Original: y, ak, z, p, q, r, 0.5f, ap
      matrixStack.popPose();
    }
  }

  private Vec3 getPosition(LivingEntity livingEntity, double yOffset, float partialTick) {
    double x = Mth.lerp(partialTick, livingEntity.xOld, livingEntity.getX());
    double y = Mth.lerp(partialTick, livingEntity.yOld, livingEntity.getY()) + yOffset;
    double z = Mth.lerp(partialTick, livingEntity.zOld, livingEntity.getZ());
    return new Vec3(x, y, z);
  }

  private static void renderPart(PoseStack poseStack, VertexConsumer consumer, float red, float green, float blue,
                                 float alpha, float minY, float maxY, float x0, float z0, float x1, float z1, float x2,
                                 float z2, float x3, float z3, float minU, float maxU, float minV, float maxV) {
    PoseStack.Pose pose = poseStack.last();
    Matrix4f matrix4f = pose.pose();
    Matrix3f matrix3f = pose.normal();
    renderQuad(matrix4f, matrix3f, consumer, red, green, blue, alpha, minY, maxY, x0, z0, x1, z1, minU, maxU, minV,
               maxV);
    renderQuad(matrix4f, matrix3f, consumer, red, green, blue, alpha, minY, maxY, x3, z3, x2, z2, minU, maxU, minV,
               maxV);
    renderQuad(matrix4f, matrix3f, consumer, red, green, blue, alpha, minY, maxY, x1, z1, x3, z3, minU, maxU, minV,
               maxV);
    renderQuad(matrix4f, matrix3f, consumer, red, green, blue, alpha, minY, maxY, x2, z2, x0, z0, minU, maxU, minV,
               maxV);
  }

  private static void vertex(VertexConsumer arg, Matrix4f arg2, Matrix3f arg3, float f, float g, float h, float rgb,
                             float l, float m) {
    arg.vertex(arg2, f, g, h).color(rgb, rgb, rgb, 1.0F).uv(l, m).overlayCoords(OverlayTexture.NO_OVERLAY).uv2(0xF000F0)
        .normal(arg3, 0.0f, 1.0f, 0.0f).endVertex();
  }

  private static void renderQuad(Matrix4f pose, Matrix3f normal, VertexConsumer consumer, float red, float green,
                                 float blue, float alpha, float minY, float maxY, float minX, float minZ, float maxX,
                                 float maxZ, float minU, float maxU, float minV, float maxV) {
    vertex(consumer, pose, normal, red, green, blue, alpha, maxY, minX, minZ, maxU, minV);
    vertex(consumer, pose, normal, red, green, blue, alpha, minY, minX, minZ, maxU, maxV);
    vertex(consumer, pose, normal, red, green, blue, alpha, minY, maxX, maxZ, minU, maxV);
    vertex(consumer, pose, normal, red, green, blue, alpha, maxY, maxX, maxZ, minU, minV);
  }

  private static void vertex(VertexConsumer consumer, Matrix4f pose, Matrix3f normal, float red, float green,
                             float blue, float alpha, float y, float x, float z, float u, float v) {
    consumer.vertex(pose, x, y, z).color(red, green, blue, alpha).uv(u, v).overlayCoords(OverlayTexture.NO_OVERLAY)
        .uv2(0xF000F0).normal(normal, 0.0f, 1.0f, 0.0f).endVertex();
  }
}