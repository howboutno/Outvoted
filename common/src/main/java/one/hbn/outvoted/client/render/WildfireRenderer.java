package one.hbn.outvoted.client.render;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.core.BlockPos;
import net.minecraft.resources.ResourceLocation;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.client.model.WildfireModel;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.monster.Wildfire;

@SuppressWarnings("unchecked")
@Environment(EnvType.CLIENT)
public class WildfireRenderer extends MobRenderer<Wildfire, WildfireModel> {
  private static final ResourceLocation DEFAULT = Constants.loc("textures/entity/wildfire/wildfire.png");
  private static final ResourceLocation SOUL = Constants.loc("textures/entity/wildfire/wildfire_soul.png");

  public WildfireRenderer(EntityRendererProvider.Context ctx) {
    super(ctx, new WildfireModel(ctx.bakeLayer(WildfireModel.LAYER_LOCATION)), 1.0F);
  }

  @Override
  protected int getBlockLightLevel(Wildfire arg, BlockPos arg2) {
    return 15;
  }

  @Override
  public ResourceLocation getTextureLocation(Wildfire entity) {
    if (!OutvotedConfig.CLIENT.variants.get() || entity.getVariant() == 0) {
      return DEFAULT;
    }
    return SOUL;
  }

  //  @Override
  //  protected void scale(T livingEntity, PoseStack matrixStack, float partialTickTime) {
  //    float f = 0.999f;
  //    matrixStack.scale(0.999f, 0.999f, 0.999f);
  //    matrixStack.translate(0.0, 0.001f, 0.0);
  //    float g = livingEntity.getSize();
  //    float h = Mth.lerp(partialTickTime, livingEntity.oSquish, livingEntity.squish) / (g * 0.5f + 1.0f);
  //    float i = 1.0f / (h + 1.0f);
  //    matrixStack.scale(i * g, 1.0f / i * g, i * g);
  //  }

  //  @Override
  //  public void render(T entity, float entityYaw, float partialTicks, PoseStack matrixStack, MultiBufferSource buffer, int packedLight) {
  //    matrixStack.pushPose();
  //    float scale = 1.1F;
  //
  //    matrixStack.scale(scale, scale, scale);
  //
  //    super.render(entity, entityYaw, partialTicks, matrixStack, buffer, packedLight);
  //    matrixStack.popPose();
  //  }
}