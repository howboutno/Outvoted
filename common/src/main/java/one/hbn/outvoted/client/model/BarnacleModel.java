package one.hbn.outvoted.client.model;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.entity.monster.Barnacle;

@Environment(EnvType.CLIENT)
public class BarnacleModel extends HierarchicalModel<Barnacle> {
  public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Constants.loc("barnacle"), "main");
  static final float RAD45 = 0.7854F;
  private final ModelPart tentul;
  private final ModelPart tentur;
  private final ModelPart tentbr;
  private final ModelPart tentbl;
  private final ModelPart head;
  private final ModelPart root;

  public BarnacleModel(ModelPart root) {
    super();
    this.root = root;
    this.head = this.root.getChild("head");
    this.tentul = head.getChild("tentul");
    this.tentur = head.getChild("tentur");
    this.tentbr = head.getChild("tentbr");
    this.tentbl = head.getChild("tentbl");
  }

  public static LayerDefinition createBodyLayer() {
    MeshDefinition meshDefinition = new MeshDefinition();
    PartDefinition partDefinition = meshDefinition.getRoot();

    PartDefinition head = partDefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 6)
        .addBox(-4.0F, -8.0F, -5.0F, 8.0F, 8.0F, 10.0F), PartPose.offset(0.0F, 24.0F, 0.0F));

    head.addOrReplaceChild("back", CubeListBuilder.create().texOffs(32, 0).addBox(-3.0F, -7.0F, 5.0F, 6.0F, 6.0F, 10.0F)
        .texOffs(0, -4).addBox(0.0F, -10.0F, 1.0F, 0.0F, 2.0F, 7.0F).texOffs(0, -6)
        .addBox(0.0F, 0.0F, 2.0F, 0.0F, 3.0F, 6.0F), PartPose.offset(0.0F, 0.0F, 0.0F));

    head.addOrReplaceChild("backx_r1", CubeListBuilder.create().texOffs(38, 16).mirror()
                               .addBox(-4.0F, 0.0F, -5.0F, 8.0F, 0.0F, 10.0F).mirror(false),
                           PartPose.offsetAndRotation(0.0F, -4.0F, 10.0F, 0.0F, 0.0F, -RAD45));
    head.addOrReplaceChild("backx_r2",
                           CubeListBuilder.create().texOffs(38, 16).addBox(-4.0F, 0.0F, -5.0F, 8.0F, 0.0F, 10.0F),
                           PartPose.offsetAndRotation(0.0F, -4.0F, 10.0F, 0.0F, 0.0F, RAD45));

    CubeListBuilder cubeListBuilderT = CubeListBuilder.create().texOffs(6, 24)
        .addBox(-7.0F, -7.0F, -20.0F, 7.0F, 7.0F, 22.0F);

    head.addOrReplaceChild("tentul", cubeListBuilderT, PartPose.offset(0.0F, -4.0F, -5.0F));
    head.addOrReplaceChild("tentur", cubeListBuilderT,
                           PartPose.offsetAndRotation(0.0F, -4.0F, -5.0F, 0.0F, 0.0F, Mth.HALF_PI));
    head.addOrReplaceChild("tentbr", cubeListBuilderT,
                           PartPose.offsetAndRotation(0.0F, -4.0F, -5.0F, 0.0F, 0.0F, -Mth.PI));
    head.addOrReplaceChild("tentbl", cubeListBuilderT,
                           PartPose.offsetAndRotation(0.0F, -4.0F, -5.0F, 0.0F, 0.0F, -Mth.HALF_PI));

    return LayerDefinition.create(meshDefinition, 64, 64);
  }

  @Override
  public void setupAnim(Barnacle entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
                        float headPitch) {
    this.head.yRot = netHeadYaw * (Mth.PI / 180F);
    this.head.xRot = headPitch * (Mth.PI / 180F) + RAD45 / 4F;
    float f = ageInTicks - (float) entity.tickCount;
    float h = entity.getTailAnimation(f);
    float g = entity.getTailAnimationSpeed() == 0.1F ? 1.0F : 3.0F;
    float max = 0.05F * g;
    float xRot = Mth.cos(h) * Mth.HALF_PI * max - 0.1F;
    float yRot = Mth.cos(h + Mth.PI) * Mth.HALF_PI * max + 0.1F;
    this.tentul.xRot = this.tentur.xRot = this.tentbr.xRot = this.tentbl.xRot = xRot; // so nice that this works
    this.tentul.yRot = this.tentur.yRot = this.tentbr.yRot = this.tentbl.yRot = yRot;
  }

  public ModelPart root() {
    return this.root;
  }
}