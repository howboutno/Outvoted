package one.hbn.outvoted.client.model;

import com.mojang.blaze3d.vertex.PoseStack;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ArmedModel;
import net.minecraft.client.model.HierarchicalModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.CubeListBuilder;
import net.minecraft.client.model.geom.builders.LayerDefinition;
import net.minecraft.client.model.geom.builders.MeshDefinition;
import net.minecraft.client.model.geom.builders.PartDefinition;
import net.minecraft.util.Mth;
import net.minecraft.world.entity.HumanoidArm;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.entity.animal.CopperGolem;

@Environment(EnvType.CLIENT)
public class CopperGolemModel extends HierarchicalModel<CopperGolem> implements ArmedModel {
  public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Constants.loc("copper_golem"), "main");
  private final ModelPart rightArm;
  private final ModelPart rightLeg;
  private final ModelPart leftArm;
  private final ModelPart leftLeg;
  private final ModelPart head;
  private final ModelPart root;

  public CopperGolemModel(ModelPart root) {
    super();
    this.root = root;
    this.head = this.root.getChild("head");
    this.rightArm = this.root.getChild("right_arm");
    this.leftArm = this.root.getChild("left_arm");
    this.rightLeg = this.root.getChild("right_leg");
    this.leftLeg = this.root.getChild("left_leg");
  }

  public static LayerDefinition createBodyLayer() {
    MeshDefinition meshDefinition = new MeshDefinition();
    PartDefinition partDefinition = meshDefinition.getRoot();

    partDefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0)
        .addBox(-6.0F, -7.125F, -4.0F, 12.0F, 7.0F, 8.0F).texOffs(40, 8).addBox(-2.0F, -3.125F, -5.0F, 4.0F, 5.0F, 2.0F)
        .texOffs(0, 1).addBox(-1.0F, -10.125F, -1.0F, 2.0F, 5.0F, 2.0F).texOffs(32, 0)
        .addBox(-2.0F, -14.125F, -2.0F, 4.0F, 4.0F, 4.0F), PartPose.offset(0.0F, -8.875F, 1.0F));

    partDefinition.addOrReplaceChild("body", CubeListBuilder.create().texOffs(0, 15)
        .addBox(-6.0F, -2.5F, -3.0F, 12.0F, 5.0F, 6.0F), PartPose.offset(0.0F, -6.5F, 1.0F));

    PartDefinition rightarm = partDefinition.addOrReplaceChild("right_arm", CubeListBuilder.create().texOffs(0, 26)
        .addBox(-2.5F, -1.0F, -2.0F, 3.0F, 10.0F, 4.0F), PartPose.offset(-6.5F, -9.0F, 1.0F));
    partDefinition.addOrReplaceChild("right_leg", CubeListBuilder.create().texOffs(28, 26)
        .addBox(-2.5F, 0.0F, -2.0F, 5.0F, 4.0F, 4.0F), PartPose.offset(-2.5F, -4.0F, 1.0F));
    PartDefinition leftarm = partDefinition.addOrReplaceChild("left_arm",
                                                              CubeListBuilder.create().texOffs(0, 26).mirror()
                                                                  .addBox(-0.5F, -1.0F, -2.0F, 3.0F, 10.0F, 4.0F)
                                                                  .mirror(false), PartPose.offset(6.5F, -9.0F, 1.0F));
    partDefinition.addOrReplaceChild("left_leg", CubeListBuilder.create().texOffs(28, 26).mirror()
        .addBox(-2.5F, 0.0F, -2.0F, 5.0F, 4.0F, 4.0F).mirror(false), PartPose.offset(2.5F, -4.0F, 1.0F));

    return LayerDefinition.create(meshDefinition, 64, 64);
  }

  @Override
  public void setupAnim(CopperGolem entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
                        float headPitch) {
    if (entity.getOxidizationLevel() < 3) {
      float oxidizeMult = entity.getOxidizationMultiplier();
      if (oxidizeMult < 1 && oxidizeMult > 0)
        oxidizeMult += 0.25F;
      head.xRot = headPitch * (Mth.PI / 180F);
      head.yRot = netHeadYaw * (Mth.PI / 330F);
      rightArm.xRot = Mth.cos(limbSwing * oxidizeMult) * 2.0F * limbSwingAmount;
      leftArm.xRot = Mth.cos(limbSwing * oxidizeMult + Mth.PI) * 2.0F * limbSwingAmount;
      rightLeg.xRot = Mth.cos(limbSwing * oxidizeMult + Mth.PI) * 2.0F * limbSwingAmount;
      leftLeg.xRot = Mth.cos(limbSwing * oxidizeMult) * 2.0F * limbSwingAmount;
    } else {
      long[] rot = entity.getRotations();
      head.xRot = rot[0] * -(Mth.PI / 180F);
      head.yRot = rot[1] * -(Mth.PI / 330F);
      rightArm.xRot = rot[2];
      leftArm.xRot = rot[3];
      rightLeg.xRot = rot[4];
      leftLeg.xRot = rot[5];
    }
  }

  @Override
  public ModelPart root() {
    return this.root;
  }

  @Override
  public void translateToHand(HumanoidArm side, PoseStack poseStack) {
    this.getArm(side).translateAndRotate(poseStack);
  }

  protected ModelPart getArm(HumanoidArm side) {
    if (side == HumanoidArm.LEFT) {
      return this.leftArm;
    }
    return this.rightArm;
  }
}
