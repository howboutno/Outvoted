package one.hbn.outvoted.client;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.renderer.Sheets;
import net.minecraft.client.resources.model.Material;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.OutvotedWoodType;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.platform.Services;

@Environment(EnvType.CLIENT)
public class OutvotedClient {
  public static void init() {
    Sheets.SIGN_MATERIALS.put(OutvotedWoodType.PALM,
                              new Material(Sheets.SIGN_SHEET, Constants.loc("entity/signs/palm")));

    //        ItemProperties.register(ItemRegister.WILDFIRE_SHIELD.get(), new ResourceLocation("blocking"),
    //            (itemStack, clientWorld, livingEntity, seed) ->
    //                livingEntity != null && livingEntity.isUsingItem() && livingEntity.getUseItem() == itemStack ? 1.0F : 0.0F);

    if (Services.PLATFORM.isModLoaded("patchouli"))
      vazkii.patchouli.api.PatchouliAPI.get()
          .setConfigFlag("outvoted:wildfirevariant", OutvotedConfig.CLIENT.variants.get());
  }
}
