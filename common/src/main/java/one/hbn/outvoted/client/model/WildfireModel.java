package one.hbn.outvoted.client.model;

import com.mojang.blaze3d.vertex.PoseStack;
import com.mojang.blaze3d.vertex.VertexConsumer;
import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.EntityModel;
import net.minecraft.client.model.geom.ModelLayerLocation;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.model.geom.PartPose;
import net.minecraft.client.model.geom.builders.*;
import net.minecraft.util.Mth;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.entity.monster.Wildfire;

import java.util.Arrays;

@Environment(EnvType.CLIENT)
public class WildfireModel extends EntityModel<Wildfire> {
  public static final ModelLayerLocation LAYER_LOCATION = new ModelLayerLocation(Constants.loc("wildfire"), "main");
  static final float RAD20 = 0.3491F;
  static final float RAD45 = 0.7854F;
  private final ModelPart root;
  private final ModelPart head;
  private final ModelPart center;
  private final ModelPart[] panels;
  private Wildfire.Status status = Wildfire.Status.IDLE;
  private float curSpinSpeed = 0.1F;
  private float scale = 1.0F;

  public WildfireModel(ModelPart root) {
    this.root = root;
    this.head = this.root.getChild("head");
    this.center = this.root.getChild("center");
    this.panels = new ModelPart[4];
    Arrays.setAll(this.panels, i -> this.root.getChild("panels").getChild(getPartName(i)));
  }

  // TODO: Get animations w/ scaling working properly + and add more particles too for shielding

  public static String getPartName(int index) {
    return "part" + index;
  }

  public static LayerDefinition createBodyLayer() {
    MeshDefinition meshDefinition = new MeshDefinition();
    PartDefinition partDefinition = meshDefinition.getRoot();

    PartDefinition head = partDefinition.addOrReplaceChild("head", CubeListBuilder.create().texOffs(0, 0)
        .addBox(-4.0F, -8.0F, -4.0F, 8.0F, 8.0F, 8.0F), PartPose.ZERO);
    head.addOrReplaceChild("helmet", CubeListBuilder.create().texOffs(16, 46)
        .addBox(-10.0F, -9.0F, 2.0F, 8.0F, 9.0F, 8.0F, new CubeDeformation(0.5F)), PartPose.offset(6.0F, 0.0F, -6.0F));

    partDefinition.addOrReplaceChild("center", CubeListBuilder.create().texOffs(0, 36)
        .addBox(-2.0F, 0.0F, -2.0F, 4.0F, 21.0F, 4.0F), PartPose.ZERO);

    PartDefinition panels = partDefinition.addOrReplaceChild("panels", CubeListBuilder.create(), PartPose.ZERO);

    CubeListBuilder cubeListBuilder = CubeListBuilder.create().texOffs(0, 16)
        .addBox(-4.0F, 2.0F, -15.0F, 10.0F, 18.0F, 2.0F);
    for (int i = 0; i < 4; i++) {
      panels.addOrReplaceChild(getPartName(i), cubeListBuilder, PartPose.rotation(-RAD20, i * Mth.HALF_PI, 0.0F));
    }

    return LayerDefinition.create(meshDefinition, 64, 64);
  }

  @Override
  public void setupAnim(Wildfire entity, float limbSwing, float limbSwingAmount, float ageInTicks, float netHeadYaw,
                        float headPitch) {
    int i;
    this.status = entity.getStatus();

    if (this.status == Wildfire.Status.ATTACK) {
      curSpinSpeed = Mth.lerp(1F, curSpinSpeed, 0.8F);
      this.panels[0].z = this.panels[1].x = this.panels[2].z = this.panels[3].x = 0.0F;

      float f = ageInTicks * Mth.HALF_PI * -0.1f;
      for (i = 0; i < 4; i++) {
        this.panels[i].xRot = Mth.lerp(0.5F, this.panels[i].xRot, -RAD45);
        this.panels[i].y = Mth.lerp(0.1F, this.panels[i].y, Mth.cos(f) * 3.0f + 2.0F);
        f += Mth.HALF_PI;
      }
    }
    if (this.status == Wildfire.Status.SHIELD) {
      curSpinSpeed = Mth.lerp(0.1F, curSpinSpeed, 0.2F);
      float dist = 6.0F;
      this.panels[0].z = this.panels[1].x = Mth.lerp(0.1F, this.panels[0].z, dist);
      this.panels[2].z = this.panels[3].x = Mth.lerp(0.1F, this.panels[2].z, -dist);
      for (i = 0; i < 4; i++) {
        this.panels[i].xRot = Mth.lerp(0.1F, this.panels[i].xRot, 0F);
        this.panels[i].y = Mth.lerp(0.1F, this.panels[i].y, -5.0F);
      }
    } else {
      curSpinSpeed = Mth.lerp(0.1F, curSpinSpeed, 0.1F);
      this.panels[0].z = this.panels[1].x = Mth.lerp(0.1F, this.panels[0].z, 0.0F);
      this.panels[2].z = this.panels[3].x = Mth.lerp(0.1F, this.panels[2].z, 0.0F);
      float f = ageInTicks * Mth.HALF_PI * -0.1f;
      for (i = 0; i < 4; ++i) {
        this.panels[i].xRot = Mth.lerp(0.1F, this.panels[i].xRot, -RAD20);
        this.panels[i].y = Mth.lerp(0.1F, this.panels[i].y, Mth.cos(f) * 3.0f);
        f += Mth.HALF_PI;
      }
    }

    this.root.getChild("panels").yRot = ageInTicks * curSpinSpeed;
    this.center.yRot = -(ageInTicks * curSpinSpeed);
    this.center.y = (Mth.cos(ageInTicks / 10F) * 0.8F) + 2F;
    this.head.yRot = netHeadYaw * Mth.DEG_TO_RAD;
    this.head.xRot = headPitch * Mth.DEG_TO_RAD;
  }

  @Override
  public void renderToBuffer(PoseStack poseStack, VertexConsumer buffer, int packedLight, int packedOverlay, float red,
                             float green, float blue, float alpha) {
    this.head.render(poseStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
    this.center.render(poseStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);

    //    this.root.getChild("panels").render(poseStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
    if (this.status == Wildfire.Status.SHIELD) {
      this.scale = Mth.lerp(0.05F, this.scale, 1.5F);
    } else {
      this.scale = Mth.lerp(0.05F, this.scale, 1.0F);
    }
    poseStack.pushPose();
    poseStack.scale(this.scale, this.scale, this.scale);
    //    for (int i = 0; i < 4; ++i) {
    //      this.panels[i].render(poseStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
    //    }
    this.root.getChild("panels").render(poseStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
    //    this.panels[0].render(poseStack, buffer, packedLight, packedOverlay, red, green, blue, alpha);
    poseStack.popPose();
  }
}
