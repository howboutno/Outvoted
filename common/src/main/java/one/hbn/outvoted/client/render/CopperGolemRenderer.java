package one.hbn.outvoted.client.render;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.renderer.entity.EntityRendererProvider;
import net.minecraft.client.renderer.entity.MobRenderer;
import net.minecraft.resources.ResourceLocation;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.client.model.CopperGolemModel;
import one.hbn.outvoted.client.render.layers.CopperGolemHeldItemLayer;
import one.hbn.outvoted.server.entity.animal.CopperGolem;

@Environment(EnvType.CLIENT)
public class CopperGolemRenderer extends MobRenderer<CopperGolem, CopperGolemModel> {
  private static final ResourceLocation UNAFFECTED = Constants.loc("textures/entity/copper_golem/copper_golem.png");
  private static final ResourceLocation EXPOSED = Constants.loc(
      "textures/entity/copper_golem/copper_golem_exposed.png");
  private static final ResourceLocation WEATHERED = Constants.loc(
      "textures/entity/copper_golem/copper_golem_weathered.png");
  private static final ResourceLocation OXIDIZED = Constants.loc(
      "textures/entity/copper_golem/copper_golem_oxidized.png");

  public CopperGolemRenderer(EntityRendererProvider.Context ctx) {
    super(ctx, new CopperGolemModel(ctx.bakeLayer(CopperGolemModel.LAYER_LOCATION)), 0.5F);
    this.addLayer(new CopperGolemHeldItemLayer(this));
  }

  @Override
  public ResourceLocation getTextureLocation(CopperGolem entity) {
    return switch (entity.getOxidizationLevel()) {
      case 3 -> OXIDIZED;
      case 2 -> WEATHERED;
      case 1 -> EXPOSED;
      default -> UNAFFECTED;
    };
  }
}
