package one.hbn.outvoted.client.model;

import net.fabricmc.api.EnvType;
import net.fabricmc.api.Environment;
import net.minecraft.client.model.ShieldModel;
import net.minecraft.client.model.geom.ModelPart;
import net.minecraft.client.resources.model.Material;
import net.minecraft.world.inventory.InventoryMenu;
import one.hbn.outvoted.Constants;

@Environment(EnvType.CLIENT)
public class WildfireShieldModel extends ShieldModel {
  public static final Material base = new Material(InventoryMenu.BLOCK_ATLAS,
                                                   Constants.loc("entity/shield/wildfire_shield_base"));
  public static final Material base_nopattern = new Material(InventoryMenu.BLOCK_ATLAS, Constants.loc(
      "entity/shield/wildfire_shield_base_nopattern"));

  public WildfireShieldModel(ModelPart $$0) {
    super($$0);
  }
}