package one.hbn.outvoted;

import net.minecraft.core.Registry;
import net.minecraft.network.chat.Component;
import net.minecraft.world.item.ItemStack;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.TooltipFlag;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.block.entity.BlockEntityRegister;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.item.ItemRegister;
import one.hbn.outvoted.server.loot.LootConditionRegister;
import one.hbn.outvoted.server.platform.Services;
import one.hbn.outvoted.server.recipe.RecipeSerializerRegister;
import one.hbn.outvoted.server.sounds.SoundRegister;
import one.hbn.outvoted.server.world.PoiRegister;
import one.hbn.outvoted.server.world.TagRegister;
import one.hbn.outvoted.server.world.feature.FeatureRegister;

import java.util.List;

public class OutvotedCommon {
  public static void init() {

    Constants.LOG.info("Hello from Common init on {}! we are currently in a {} environment!",
                       Services.PLATFORM.getPlatformName(),
                       Services.PLATFORM.isDevelopmentEnvironment() ? "development" : "production");
    Constants.LOG.info("Diamond Item >> {}", Registry.ITEM.getKey(Items.DIAMOND));

    ItemRegister.loadClass();
    BlockRegister.loadClass();
    EntityRegister.loadClass();
    BlockEntityRegister.loadClass();
    FeatureRegister.loadClass();
    LootConditionRegister.loadClass();
    PoiRegister.loadClass();
    RecipeSerializerRegister.loadClass();
    SoundRegister.loadClass();
    TagRegister.loadClass();
  }

//  public static void commonInit() {
//    Constants.LOG.info("Hello from Common POST init on {}!", Services.PLATFORM.getPlatformName());
//  }

  // This method serves as a hook to modify item tooltips. The vanilla game
  // has no mechanism to load tooltip listeners so this must be registered
  // by a mod loader like Forge or Fabric.
  public static void onItemTooltip(ItemStack stack, TooltipFlag context, List<Component> tooltip) {

    //        if (!stack.isEmpty()) {
    //
    //            final FoodProperties food = stack.getItem().getFoodProperties();
    //
    //            if (food != null) {
    //
    //                tooltip.add(new TextComponent("Nutrition: " + food.getNutrition()));
    //                tooltip.add(new TextComponent("Saturation: " + food.getSaturationModifier()));
    //            }
    //        }
  }
}