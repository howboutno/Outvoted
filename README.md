# Outvoted
[![](https://img.shields.io/curseforge/v/415057?label=CURSEFORGE&style=for-the-badge&logo=curseforge&color=de6a3b)](https://www.curseforge.com/minecraft/mc-mods/outvoted)[![](https://img.shields.io/modrinth/v/klAfKAj3?label=MODRINTH&style=for-the-badge&logo=modrinth)](https://modrinth.com/mod/outvoted)
[![](https://img.shields.io/discord/776261853932158977?label=Discord&logo=discord&style=for-the-badge)](https://discord.com/invite/FeBUNVtjmb)

[//]: # ([![]&#40;https://img.shields.io/github/workflow/status/How-Bout-No/Outvoted/Java%20CI%20with%20Gradle?style=for-the-badge&#41;]&#40;https://github.com/How-Bout-No/Outvoted/actions?query=workflow%3A%22Java+CI+with+Gradle%22&#41;)

Outvoted adds in mobs from previous mob votes that weren't able to make it into the game, trying to stay true to the original concepts.


This is the source code in case for some reason you wanted to look at it and/or build the source.
