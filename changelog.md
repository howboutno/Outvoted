### Progress
- [ ] Remove dependency on GeckoLib
    - [x] Wildfire
    - [x] Barnacle
    - [ ] Glutton
    - [x] Copper Golem
    - [ ] Glare
    - [ ] Meerkat
    - [ ] Ostrich
- [x] Remove dependency on Architectury (in production)
- [x] Improve data generators
- [x] Refactor codebase to `one.hbn.outvoted`
- [x] Update underlying dependencies


- [ ] Testing