package one.hbn.outvoted.forge.data.tags;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.ItemTagsProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.world.TagRegister;

public class ItemTags extends ItemTagsProvider {
  public ItemTags(final DataGenerator generatorIn, BlockTags blockTagProvider,
                  final ExistingFileHelper existingFileHelper) {
    super(generatorIn, blockTagProvider, Constants.MOD_ID, existingFileHelper);
  }

  @Override
  protected void addTags() {
    this.copy(TagRegister.BAOBAB_LOGS, TagRegister.BAOBAB_LOGS_ITEM);
    this.copy(TagRegister.PALM_LOGS, TagRegister.PALM_LOGS_ITEM);
  }
}
