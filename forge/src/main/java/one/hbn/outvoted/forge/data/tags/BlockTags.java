package one.hbn.outvoted.forge.data.tags;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.BlockTagsProvider;
import net.minecraft.world.level.block.Blocks;
import net.minecraftforge.common.data.ExistingFileHelper;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.world.TagRegister;

public class BlockTags extends BlockTagsProvider {
  public BlockTags(final DataGenerator generatorIn, final ExistingFileHelper existingFileHelper) {
    super(generatorIn, Constants.MOD_ID, existingFileHelper);
  }

  @Override
  protected void addTags() {
    tag(TagRegister.GLUTTON_CAN_BURROW).addTag(net.minecraft.tags.BlockTags.SAND)
        .add(Blocks.GRAVEL, Blocks.DIRT, Blocks.GRASS_BLOCK, Blocks.PODZOL, Blocks.COARSE_DIRT, Blocks.MYCELIUM);
    tag(TagRegister.COPPER_BUTTONS).add(BlockRegister.COPPER_BUTTON.get())
        .add(BlockRegister.EXPOSED_COPPER_BUTTON.get()).add(BlockRegister.WEATHERED_COPPER_BUTTON.get())
        .add(BlockRegister.OXIDIZED_COPPER_BUTTON.get()).add(BlockRegister.WAXED_COPPER_BUTTON.get())
        .add(BlockRegister.WAXED_EXPOSED_COPPER_BUTTON.get()).add(BlockRegister.WAXED_WEATHERED_COPPER_BUTTON.get())
        .add(BlockRegister.WAXED_OXIDIZED_COPPER_BUTTON.get());
    tag(net.minecraft.tags.BlockTags.BUTTONS).addTag(TagRegister.COPPER_BUTTONS);
    tag(TagRegister.BAOBAB_LOGS).add(BlockRegister.BAOBAB_LOG.get()).add(BlockRegister.BAOBAB_WOOD.get())
        .add(BlockRegister.STRIPPED_BAOBAB_LOG.get()).add(BlockRegister.STRIPPED_BAOBAB_WOOD.get());
    tag(TagRegister.PALM_LOGS).add(BlockRegister.PALM_LOG.get()).add(BlockRegister.PALM_WOOD.get())
        .add(BlockRegister.STRIPPED_PALM_LOG.get()).add(BlockRegister.STRIPPED_PALM_WOOD.get());
  }
}
