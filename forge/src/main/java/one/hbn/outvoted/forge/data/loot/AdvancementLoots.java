package one.hbn.outvoted.forge.data.loot;

import net.minecraft.nbt.CompoundTag;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.SetNbtFunction;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import one.hbn.outvoted.Constants;
import vazkii.patchouli.api.PatchouliAPI;

import java.util.function.BiConsumer;
import java.util.function.Consumer;

public class AdvancementLoots implements Consumer<BiConsumer<ResourceLocation, LootTable.Builder>> {
  public static final ResourceLocation BOOK = Constants.loc("rewards/book");

  @Override
  public void accept(BiConsumer<ResourceLocation, LootTable.Builder> biConsumer) {
    ResourceLocation location = Constants.loc("book");
    CompoundTag tag = new CompoundTag();
    tag.putString("patchouli:book", location.toString());
    biConsumer.accept(BOOK, LootTable.lootTable()
        .withPool(LootPool.lootPool()
                      .setRolls(ConstantValue.exactly(1.0f))
                      .add(LootItem.lootTableItem(PatchouliAPI.get().getBookStack(location).getItem())
                               .apply(SetNbtFunction.setTag(tag)))));
  }
}