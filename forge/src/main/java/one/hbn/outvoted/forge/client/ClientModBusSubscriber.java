package one.hbn.outvoted.forge.client;

import net.minecraft.client.renderer.ItemBlockRenderTypes;
import net.minecraft.client.renderer.RenderType;
import net.minecraft.client.renderer.blockentity.SignRenderer;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.client.event.EntityRenderersEvent;
import net.minecraftforge.client.event.TextureStitchEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.client.OutvotedClient;
import one.hbn.outvoted.client.model.BarnacleModel;
import one.hbn.outvoted.client.model.CopperGolemModel;
import one.hbn.outvoted.client.model.WildfireModel;
import one.hbn.outvoted.client.model.WildfireShieldModel;
import one.hbn.outvoted.client.render.BarnacleRenderer;
import one.hbn.outvoted.client.render.CopperGolemRenderer;
import one.hbn.outvoted.client.render.WildfireRenderer;
import one.hbn.outvoted.forge.server.config.ForgeOutvotedConfig;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.block.entity.BlockEntityRegister;
import one.hbn.outvoted.server.entity.EntityRegister;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, modid = Constants.MOD_ID, value = Dist.CLIENT)
public abstract class ClientModBusSubscriber {
  @SubscribeEvent
  public static void clientSetup(final FMLClientSetupEvent event) {
    ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, ForgeOutvotedConfig.CLIENT_CONFIG);

    OutvotedClient.init();

    ItemBlockRenderTypes.setRenderLayer(BlockRegister.PALM_DOOR.get(), RenderType.cutout());
    ItemBlockRenderTypes.setRenderLayer(BlockRegister.PALM_TRAPDOOR.get(), RenderType.cutout());
  }

  @SubscribeEvent
  public static void registerEntities(EntityRenderersEvent.RegisterRenderers event) {
    event.registerEntityRenderer(EntityRegister.WILDFIRE.get(), WildfireRenderer::new);
    //    event.registerEntityRenderer(ModEntities.GLUTTON, GluttonRenderer::new);
    event.registerEntityRenderer(EntityRegister.BARNACLE.get(), BarnacleRenderer::new);
    //    event.registerEntityRenderer(ModEntities.GLARE, GlareRenderer::new);
    event.registerEntityRenderer(EntityRegister.COPPER_GOLEM.get(), CopperGolemRenderer::new);
    //    event.registerEntityRenderer(ModEntities.MEERKAT, MeerkatRenderer::new);
    //    event.registerEntityRenderer(ModEntities.OSTRICH, OstrichRenderer::new);
    //      event.registerEntityRenderer(YourMod.YOUR_ARMOR_ENTITY_TYPE, renderManager -> new HelmetLayer(renderManager, new CustomArmorModel(), new CustomArmorModel(1.0F)));

    event.registerBlockEntityRenderer(BlockEntityRegister.SIGN.get(), SignRenderer::new);
  }

  @SubscribeEvent
  public static void registerLayers(EntityRenderersEvent.RegisterLayerDefinitions event) {
    event.registerLayerDefinition(BarnacleModel.LAYER_LOCATION, BarnacleModel::createBodyLayer);
    event.registerLayerDefinition(WildfireModel.LAYER_LOCATION, WildfireModel::createBodyLayer);
    event.registerLayerDefinition(CopperGolemModel.LAYER_LOCATION, CopperGolemModel::createBodyLayer);
  }

  public static class ShieldTex {
    @SubscribeEvent
    public void registerTextureAtlas(TextureStitchEvent.Pre event) {
      event.addSprite(WildfireShieldModel.base.texture());
      event.addSprite(WildfireShieldModel.base_nopattern.texture());
    }
  }
}
