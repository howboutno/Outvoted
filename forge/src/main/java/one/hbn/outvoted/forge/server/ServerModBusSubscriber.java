package one.hbn.outvoted.forge.server;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.SpawnPlacements;
import net.minecraft.world.entity.monster.Monster;
import net.minecraft.world.level.levelgen.Heightmap;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.event.entity.EntityAttributeCreationEvent;
import net.minecraftforge.event.entity.player.ItemTooltipEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.OutvotedCommon;
import one.hbn.outvoted.forge.server.config.ForgeOutvotedConfig;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.entity.animal.CopperGolem;
import one.hbn.outvoted.server.entity.animal.Glare;
import one.hbn.outvoted.server.entity.animal.Meerkat;
import one.hbn.outvoted.server.entity.animal.Ostrich;
import one.hbn.outvoted.server.entity.monster.Barnacle;
import one.hbn.outvoted.server.entity.monster.Glutton;
import one.hbn.outvoted.server.entity.monster.Wildfire;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, modid = Constants.MOD_ID)
public class ServerModBusSubscriber {
  @SubscribeEvent
  public static void onCommonSetup(final FMLCommonSetupEvent event) {
    ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ForgeOutvotedConfig.COMMON_CONFIG);
//    OutvotedCommon.init();
  }

  @SubscribeEvent(priority = EventPriority.LOWEST)
  public static void onPostRegisterEntities(final RegistryEvent.Register<EntityType<?>> event) {
    SpawnPlacements.register(EntityRegister.WILDFIRE.get(), SpawnPlacements.Type.ON_GROUND,
                             Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Monster::checkAnyLightMonsterSpawnRules);
    SpawnPlacements.register(EntityRegister.GLUTTON.get(), SpawnPlacements.Type.ON_GROUND,
                             Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Glutton::canSpawn);
    SpawnPlacements.register(EntityRegister.BARNACLE.get(), SpawnPlacements.Type.IN_WATER,
                             Heightmap.Types.MOTION_BLOCKING, Barnacle::canSpawn);
    SpawnPlacements.register(EntityRegister.GLARE.get(), SpawnPlacements.Type.ON_GROUND,
                             Heightmap.Types.MOTION_BLOCKING_NO_LEAVES, Glare::canSpawn);
    SpawnPlacements.register(EntityRegister.COPPER_GOLEM.get(), SpawnPlacements.Type.ON_GROUND,
                             Heightmap.Types.MOTION_BLOCKING, CopperGolem::canSpawn);
    SpawnPlacements.register(EntityRegister.OSTRICH.get(), SpawnPlacements.Type.ON_GROUND,
                             Heightmap.Types.MOTION_BLOCKING, Ostrich::canSpawn);
  }

  @SubscribeEvent
  public static void onAttributeCreate(EntityAttributeCreationEvent event) {
    event.put(EntityRegister.WILDFIRE.get(), Wildfire.setCustomAttributes().build());
    event.put(EntityRegister.GLUTTON.get(), Glutton.setCustomAttributes().build());
    event.put(EntityRegister.BARNACLE.get(), Barnacle.setCustomAttributes().build());
    event.put(EntityRegister.GLARE.get(), Glare.setCustomAttributes().build());
    event.put(EntityRegister.COPPER_GOLEM.get(), CopperGolem.setCustomAttributes().build());
    event.put(EntityRegister.MEERKAT.get(), Meerkat.setCustomAttributes().build());
    event.put(EntityRegister.OSTRICH.get(), Ostrich.setCustomAttributes().build());
  }

  // This method exists as a wrapper for the code in the Common project.
  // It takes Forge's event object and passes the parameters along to
  // the Common listener.
  private void onItemTooltip(ItemTooltipEvent event) {
    OutvotedCommon.onItemTooltip(event.getItemStack(), event.getFlags(), event.getToolTip());
  }
}
