package one.hbn.outvoted.forge.data;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.ExistingFileHelper;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.forge.event.lifecycle.GatherDataEvent;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.forge.data.advancements.Advancements;
import one.hbn.outvoted.forge.data.loot.LootTables;
import one.hbn.outvoted.forge.data.models.BlockModels;
import one.hbn.outvoted.forge.data.models.ItemModels;
import one.hbn.outvoted.forge.data.models.blockstates.BlockStates;
import one.hbn.outvoted.forge.data.recipes.Recipes;
import one.hbn.outvoted.forge.data.tags.BlockTags;
import one.hbn.outvoted.forge.data.tags.ConfiguredStructureTags;
import one.hbn.outvoted.forge.data.tags.ItemTags;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.MOD, modid = Constants.MOD_ID)
public class DataGenerators {
  @SubscribeEvent
  public static void gatherData(GatherDataEvent event) {
    DataGenerator generator = event.getGenerator();
    final ExistingFileHelper existingFileHelper = event.getExistingFileHelper();
    if (event.includeServer()) {
      generator.addProvider(new Recipes(generator));
      generator.addProvider(new LootTables(generator));
      generator.addProvider(new Advancements(generator, existingFileHelper));
      BlockTags blockTags = new BlockTags(generator, existingFileHelper);
      generator.addProvider(blockTags);
      generator.addProvider(new ItemTags(generator, blockTags, existingFileHelper));
      generator.addProvider(new ConfiguredStructureTags(generator, existingFileHelper));
    }
    if (event.includeClient()) {
      generator.addProvider(new BlockStates(generator, existingFileHelper));
      generator.addProvider(new BlockModels(generator, existingFileHelper));
      generator.addProvider(new ItemModels(generator, existingFileHelper));
    }
  }
}
