package one.hbn.outvoted.forge.data.advancements;

import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.FrameType;
import net.minecraft.advancements.RequirementsStrategy;
import net.minecraft.advancements.critereon.*;
import net.minecraft.core.Registry;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.advancements.AdvancementProvider;
import net.minecraft.nbt.CompoundTag;
import net.minecraft.network.chat.TranslatableComponent;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Items;
import net.minecraftforge.common.data.ExistingFileHelper;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.item.ItemRegister;

import java.util.function.Consumer;

public class Advancements extends AdvancementProvider {
  private final ExistingFileHelper fileHelper;

  public Advancements(final DataGenerator generatorIn, final ExistingFileHelper fileHelperIn) {
    super(generatorIn, fileHelperIn);
    fileHelper = fileHelperIn;
  }

  @Override
  protected void registerAdvancements(Consumer<Advancement> consumer, ExistingFileHelper fileHelper) {
    new ModAdvancements().accept(consumer);
  }

  class ModAdvancements implements Consumer<Consumer<Advancement>> {
    public void accept(Consumer<Advancement> consumer) {
      //      Advancement grant_book_on_first_join = Advancement.Builder.advancement()
      //          .addCriterion("tick",
      //              new TickTrigger.TriggerInstance(EntityPredicate.Composite.create(LootPatchouliCondition.patchouli().build())))
      //          .rewards(AdvancementRewards.Builder.loot(AdvancementLoots.BOOK))
      //          .save(consumer, Constants.loc("grant_book_on_first_join"), fileHelper);

      CompoundTag isBitten = new CompoundTag();
      isBitten.putInt("Bitten", 1);
      Advancement use_glutton = Advancement.Builder.advancement().parent(new ResourceLocation("story/enchant_item"))
          .display(Items.EXPERIENCE_BOTTLE, new TranslatableComponent("advancements.outvoted.use_glutton.title"),
                   new TranslatableComponent("advancements.outvoted.use_glutton.description"), null, FrameType.GOAL,
                   true, true, false).addCriterion("use_glutton", InventoryChangeTrigger.TriggerInstance.hasItems(
              ItemPredicate.Builder.item().hasNbt(isBitten).build()))
          .save(consumer, Constants.loc("story/use_glutton"), fileHelper);

      Advancement overenchant_glutton = createEnchantmentList().parent(use_glutton)
          .display(Items.ENCHANTED_BOOK, new TranslatableComponent("advancements.outvoted.overenchant_glutton.title"),
                   new TranslatableComponent("advancements.outvoted.overenchant_glutton.description"), null,
                   FrameType.CHALLENGE, true, false, false).requirements(RequirementsStrategy.OR)
          .save(consumer, Constants.loc("story/overenchant_glutton"), fileHelper);
      Advancement rename_glutton = Advancement.Builder.advancement().parent(use_glutton)
          .display(Items.NAME_TAG, new TranslatableComponent("advancements.outvoted.rename_glutton.title"),
                   new TranslatableComponent("advancements.outvoted.rename_glutton.description"), null, FrameType.GOAL,
                   true, true, true).addCriterion("rename_glutton_d",
                                                  PlayerInteractTrigger.TriggerInstance.itemUsedOnEntity(
                                                      EntityPredicate.Composite.ANY,
                                                      ItemPredicate.Builder.item().of(Items.NAME_TAG)
                                                          .hasNbt(createNameTagNBT("Dinnerbone")),
                                                      EntityPredicate.Composite.wrap(EntityPredicate.Builder.entity()
                                                                                         .of(EntityRegister.GLUTTON.get())
                                                                                         .build())))
          .addCriterion("rename_glutton_g",
                        PlayerInteractTrigger.TriggerInstance.itemUsedOnEntity(EntityPredicate.Composite.ANY,
                                                                               ItemPredicate.Builder.item()
                                                                                   .of(Items.NAME_TAG)
                                                                                   .hasNbt(createNameTagNBT("Grumm")),
                                                                               EntityPredicate.Composite.wrap(
                                                                                   EntityPredicate.Builder.entity()
                                                                                       .of(EntityRegister.GLUTTON.get())
                                                                                       .build())))
          .requirements(RequirementsStrategy.OR).save(consumer, Constants.loc("story/rename_glutton"), fileHelper);

      Advancement obtain_wildfire_piece = Advancement.Builder.advancement()
          .parent(new ResourceLocation("nether/obtain_blaze_rod")).display(ItemRegister.WILDFIRE_PIECE.get(),
                                                                           new TranslatableComponent(
                                                                               "advancements.outvoted.obtain_wildfire_piece.title"),
                                                                           new TranslatableComponent(
                                                                               "advancements.outvoted.obtain_wildfire_piece.description"),
                                                                           null, FrameType.GOAL, true, true, false)
          .addCriterion("get_piece", InventoryChangeTrigger.TriggerInstance.hasItems(ItemRegister.WILDFIRE_PIECE.get()))
          .save(consumer, Constants.loc("nether/obtain_wildire_piece"), fileHelper);
      Advancement obtain_wildfire_helmet = Advancement.Builder.advancement().parent(obtain_wildfire_piece)
          .display(ItemRegister.WILDFIRE_PIECE.get(),
                   new TranslatableComponent("advancements.outvoted.obtain_wildfire_helmet.title"),
                   new TranslatableComponent("advancements.outvoted.obtain_wildfire_helmet.description"), null,
                   FrameType.CHALLENGE, true, true, false).addCriterion("get_helmet",
                                                                        InventoryChangeTrigger.TriggerInstance.hasItems(
                                                                            ItemRegister.WILDFIRE_HELMET.get(),
                                                                            ItemRegister.WILDFIRE_HELMET_SOUL.get()))
          .save(consumer, Constants.loc("nether/obtain_wildfire_helmet"), fileHelper);
    }

    Advancement.Builder createEnchantmentList() {
      Advancement.Builder builder = Advancement.Builder.advancement();
      Registry.ENCHANTMENT.stream().sequential().forEach(
          enchantment -> builder.addCriterion(enchantment.getRegistryName().toString(),
                                              InventoryChangeTrigger.TriggerInstance.hasItems(
                                                  ItemPredicate.Builder.item().hasEnchantment(
                                                      new EnchantmentPredicate(enchantment, MinMaxBounds.Ints.exactly(
                                                          enchantment.getMaxLevel() + 1))).build())));
      return builder;
    }

    CompoundTag createNameTagNBT(String name) {
      CompoundTag display = new CompoundTag();
      CompoundTag displayName = new CompoundTag();
      CompoundTag displayNameText = new CompoundTag();
      displayNameText.putString("text", name);
      displayName.put("Name", displayNameText);
      display.put("display", displayName);
      return display;
    }
  }
}
