package one.hbn.outvoted.forge;

import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.IEventBus;
import net.minecraftforge.fml.DistExecutor;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.OutvotedCommon;
import one.hbn.outvoted.forge.client.ClientModBusSubscriber;

@Mod(Constants.MOD_ID)
public class OutvotedForge {
  public OutvotedForge() {
    IEventBus forgeBus = MinecraftForge.EVENT_BUS;
    //    ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, ForgeConfig.COMMON_CONFIG);

    // Use Forge to bootstrap the Common mod.
    Constants.LOG.info("Hello Forge world!");
    OutvotedCommon.init();

    // Some code like events require special initialization from the
    // loader specific code.
    //    forgeBus.addListener(this::onItemTooltip);

    DistExecutor.unsafeRunWhenOn(Dist.CLIENT, () -> () -> FMLJavaModLoadingContext.get().getModEventBus()
        .register(new ClientModBusSubscriber.ShieldTex()));
  }
}