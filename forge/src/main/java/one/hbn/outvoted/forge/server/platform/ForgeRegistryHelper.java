package one.hbn.outvoted.forge.server.platform;

import net.minecraft.world.entity.EntityType;
import net.minecraft.world.entity.Mob;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraftforge.common.ForgeSpawnEggItem;
import one.hbn.outvoted.server.item.ItemRegister;
import one.hbn.outvoted.server.platform.services.IRegistryHelper;
import one.hbn.outvoted.server.registry.RegistryObject;

public class ForgeRegistryHelper implements IRegistryHelper {
  @Override
  public SpawnEggItem createSpawnEgg(RegistryObject<? extends EntityType<? extends Mob>> type, int bgColor,
                                     int hlColor) {
    return new ForgeSpawnEggItem(type, bgColor, hlColor, ItemRegister.ITEM_PROPERTIES);
  }
}
