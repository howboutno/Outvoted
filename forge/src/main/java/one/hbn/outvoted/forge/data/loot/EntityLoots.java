package one.hbn.outvoted.forge.data.loot;

import com.google.common.collect.ImmutableSet;
import net.minecraft.data.loot.EntityLoot;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.item.Items;
import net.minecraft.world.level.block.Blocks;
import net.minecraft.world.level.storage.loot.LootPool;
import net.minecraft.world.level.storage.loot.LootTable;
import net.minecraft.world.level.storage.loot.entries.LootItem;
import net.minecraft.world.level.storage.loot.functions.LootingEnchantFunction;
import net.minecraft.world.level.storage.loot.functions.SetItemCountFunction;
import net.minecraft.world.level.storage.loot.predicates.LootItemKilledByPlayerCondition;
import net.minecraft.world.level.storage.loot.predicates.LootItemRandomChanceWithLootingCondition;
import net.minecraft.world.level.storage.loot.providers.number.ConstantValue;
import net.minecraft.world.level.storage.loot.providers.number.UniformGenerator;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.item.ItemRegister;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.List;

public class EntityLoots extends EntityLoot {
  static {
    SPECIAL_LOOT_TABLE_TYPES = ImmutableSet.of(EntityType.PLAYER, EntityType.ARMOR_STAND, EntityType.IRON_GOLEM,
                                               EntityType.SNOW_GOLEM, EntityType.VILLAGER,
                                               EntityRegister.COPPER_GOLEM.get());
  }

  @Override
  protected void addTables() {
    this.add(EntityRegister.WILDFIRE.get(), LootTable.lootTable().withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.BLAZE_ROD).apply(
            SetItemCountFunction.setCount(UniformGenerator.between(1.0F, 3.0F))).apply(
            LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0.0F, 1.0F))).when(
            LootItemKilledByPlayerCondition.killedByPlayer()))).withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(
            LootItem.lootTableItem(ItemRegister.WILDFIRE_PIECE.get())
                .when(LootItemRandomChanceWithLootingCondition.randomChanceAndLootingBoost(0.15F, 0.05F))
                .when(LootItemKilledByPlayerCondition.killedByPlayer()))).withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(
            LootItem.lootTableItem(ItemRegister.WILDFIRE_HELMET.get())
                .when(LootItemRandomChanceWithLootingCondition.randomChanceAndLootingBoost(0.05F, 0.025F))
                .when(LootItemKilledByPlayerCondition.killedByPlayer()))));
    this.add(EntityRegister.GLUTTON.get(), LootTable.lootTable().withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.EXPERIENCE_BOTTLE)
                                                                       .apply(SetItemCountFunction.setCount(
                                                                           UniformGenerator.between(0.0F, 1.0F))).apply(
                LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0.0F, 1.0F))).when(
                LootItemKilledByPlayerCondition.killedByPlayer()))).withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(ItemRegister.VOID_HEART.get())
                                                                       .when(
                                                                           LootItemRandomChanceWithLootingCondition.randomChanceAndLootingBoost(
                                                                               0.1F, 0.05F)).when(
                LootItemKilledByPlayerCondition.killedByPlayer()))));
    this.add(EntityRegister.BARNACLE.get(), LootTable.lootTable().withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(
            LootItem.lootTableItem(ItemRegister.BARNACLE_TOOTH.get())
                .apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F)))
                .apply(LootingEnchantFunction.lootingMultiplier(UniformGenerator.between(0.0F, 1.0F)))
                .when(LootItemKilledByPlayerCondition.killedByPlayer()))));
    this.add(EntityRegister.COPPER_GOLEM.get(), LootTable.lootTable().withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Blocks.ALLIUM).apply(
            SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F))))).withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.COPPER_INGOT).apply(
            SetItemCountFunction.setCount(UniformGenerator.between(2.0F, 4.0F))))));
    this.add(EntityRegister.GLARE.get(), LootTable.lootTable().withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.GLOW_BERRIES).apply(
            SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F))))));
    this.add(EntityRegister.MEERKAT.get(), LootTable.lootTable().withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.STICK).apply(
            SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F)))).add(
            LootItem.lootTableItem(Items.CACTUS)
                .apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F)))).add(
            LootItem.lootTableItem(Items.WHEAT_SEEDS)
                .apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F)))).add(
            LootItem.lootTableItem(Items.STRING)
                .apply(SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F))))));
    this.add(EntityRegister.OSTRICH.get(), LootTable.lootTable().withPool(
        LootPool.lootPool().setRolls(ConstantValue.exactly(1)).add(LootItem.lootTableItem(Items.EGG).apply(
            SetItemCountFunction.setCount(UniformGenerator.between(0.0F, 1.0F))))));
  }

  @Nonnull
  @Override
  protected Iterable<EntityType<?>> getKnownEntities() {
    List<EntityType<?>> list = new ArrayList<>();
    list.add(EntityRegister.WILDFIRE.get());
    list.add(EntityRegister.GLUTTON.get());
    list.add(EntityRegister.BARNACLE.get());
    list.add(EntityRegister.COPPER_GOLEM.get());
    list.add(EntityRegister.GLARE.get());
    list.add(EntityRegister.MEERKAT.get());
    list.add(EntityRegister.OSTRICH.get());
    return list;
  }
}