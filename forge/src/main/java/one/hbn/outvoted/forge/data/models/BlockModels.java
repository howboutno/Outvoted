package one.hbn.outvoted.forge.data.models;

import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockModelProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import one.hbn.outvoted.Constants;

public class BlockModels extends BlockModelProvider {
  public BlockModels(DataGenerator generator, ExistingFileHelper existingFileHelper) {
    super(generator, Constants.MOD_ID, existingFileHelper);
  }

  @Override
  protected void registerModels() {
    tree("palm");
    ResourceLocation planks = Constants.loc("block/palm_planks");
    cubeAll("palm_planks", planks);
    buttonModel("palm_button", planks);
    doorModel("palm_door");
    fenceGateModel("palm_fence_gate", planks);
    fenceModel("palm_fence", planks);
    pressurePlateModel("palm_pressure_plate", planks);
    sign("palm_sign", planks);
    slabModel("palm_slab", planks);
    stairsModel("palm_stairs", planks);
    trapdoorModel("palm_trapdoor", Constants.loc("block/palm_trapdoor"));

    tree("baobab");

    cubeTop("burrow", new ResourceLocation("block/sand"), Constants.loc("block/burrow"));
    buttonModel("copper_button", new ResourceLocation("block/copper_block"));
    buttonModel("exposed_copper_button", new ResourceLocation("block/exposed_copper"));
    buttonModel("weathered_copper_button", new ResourceLocation("block/weathered_copper"));
    buttonModel("oxidized_copper_button", new ResourceLocation("block/oxidized_copper"));
    buttonModel("waxed_copper_button", new ResourceLocation("block/copper_block"));
    buttonModel("waxed_exposed_copper_button", new ResourceLocation("block/exposed_copper"));
    buttonModel("waxed_weathered_copper_button", new ResourceLocation("block/weathered_copper"));
    buttonModel("waxed_oxidized_copper_button", new ResourceLocation("block/oxidized_copper"));
  }


  private void tree(String path) {
    String log = path + "_log";
    cross(path + "_sapling", Constants.loc("block/" + path + "_sapling"));
    singleTexture(path + "_leaves", new ResourceLocation("block/leaves"), "all",
                  Constants.loc("block/" + path + "_leaves"));
    cubeColumn(log, Constants.loc("block/" + log), Constants.loc("block/" + log + "_top"));
    cubeColumn(path + "_wood", Constants.loc("block/" + log), Constants.loc("block/" + log));
    strippedModels(path, Constants.loc("block/" + log));
  }

  private void buttonModel(String path, ResourceLocation texture) {
    button(path, texture);
    buttonInventory(path + "_inventory", texture);
    buttonPressed(path + "_pressed", texture);
  }

  private void doorModel(String path) {
    ResourceLocation bottom = Constants.loc("block/" + path + "_bottom");
    ResourceLocation top = Constants.loc("block/" + path + "_top");
    doorTopLeft(path + "_top", bottom, top);
    doorTopRight(path + "_top_hinge", bottom, top);
    doorBottomLeft(path + "_bottom", bottom, top);
    doorBottomRight(path + "_bottom_hinge", bottom, top);
  }

  private void fenceGateModel(String path, ResourceLocation texture) {
    fenceGate(path, texture);
    fenceGateOpen(path + "_open", texture);
    fenceGateWall(path + "_wall", texture);
    fenceGateWallOpen(path + "_wall_open", texture);
  }

  private void fenceModel(String path, ResourceLocation texture) {
    fencePost(path + "_post", texture);
    fenceSide(path + "_side", texture);
    fenceInventory(path + "_inventory", texture);
  }

  private void pressurePlateModel(String path, ResourceLocation texture) {
    pressurePlate(path, texture);
    pressurePlateDown(path + "_down", texture);
  }

  private void slabModel(String path, ResourceLocation texture) {
    slab(path, texture, texture, texture);
    slabTop(path + "_top", texture, texture, texture);
  }

  private void stairsModel(String path, ResourceLocation texture) {
    stairs(path, texture, texture, texture);
    stairsInner(path + "_inner", texture, texture, texture);
    stairsOuter(path + "_outer", texture, texture, texture);
  }

  private void trapdoorModel(String path, ResourceLocation texture) {
    trapdoorBottom(path + "_bottom", texture);
    trapdoorOpen(path + "_open", texture);
    trapdoorTop(path + "_top", texture);
  }

  private void strippedModels(String path, ResourceLocation texture) {
    cubeColumn(path + "_log", texture, Constants.loc(texture.getPath() + "_top"));
    cubeColumnHorizontal(path + "_log_horizontal", texture, Constants.loc(texture.getPath() + "_top"));
    cubeColumn(path + "_wood", texture, texture);
  }
}
