package one.hbn.outvoted.forge.data.recipes;

import com.google.common.collect.Lists;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import net.minecraft.advancements.Advancement;
import net.minecraft.advancements.AdvancementRewards;
import net.minecraft.advancements.CriterionTriggerInstance;
import net.minecraft.advancements.RequirementsStrategy;
import net.minecraft.advancements.critereon.RecipeUnlockedTrigger;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.data.recipes.RecipeBuilder;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.level.ItemLike;
import vazkii.patchouli.api.PatchouliAPI;
import vazkii.patchouli.common.recipe.ShapelessBookRecipe;

import javax.annotation.Nullable;
import java.util.List;
import java.util.function.Consumer;

public class ShapelessBookRecipeBuilder implements RecipeBuilder {
  private final Item result;
  private final List<Ingredient> ingredients = Lists.newArrayList();
  private final Advancement.Builder advancement = Advancement.Builder.advancement();
  @Nullable
  private String group;

  ShapelessBookRecipeBuilder(ItemLike arg) {
    this.result = arg.asItem();
  }

  public static ShapelessBookRecipeBuilder book(ResourceLocation location) {
    return new ShapelessBookRecipeBuilder(PatchouliAPI.get().getBookStack(location).getItem());
  }

  /**
   * Adds an ingredient of the given item.
   */
  public ShapelessBookRecipeBuilder requires(ItemLike item) {
    return this.requires(item, 1);
  }

  /**
   * Adds the given ingredient multiple times.
   */
  public ShapelessBookRecipeBuilder requires(ItemLike item, int quantity) {
    for (int i = 0; i < quantity; ++i) {
      this.requires(Ingredient.of(item));
    }
    return this;
  }

  /**
   * Adds an ingredient.
   */
  public ShapelessBookRecipeBuilder requires(Ingredient ingredient) {
    return this.requires(ingredient, 1);
  }

  /**
   * Adds an ingredient multiple times.
   */
  public ShapelessBookRecipeBuilder requires(Ingredient ingredient, int quantity) {
    for (int i = 0; i < quantity; ++i) {
      this.ingredients.add(ingredient);
    }
    return this;
  }

  @Override
  public ShapelessBookRecipeBuilder unlockedBy(String criterionName, CriterionTriggerInstance criterionTrigger) {
    this.advancement.addCriterion(criterionName, criterionTrigger);
    return this;
  }

  @Override
  public ShapelessBookRecipeBuilder group(@Nullable String groupName) {
    this.group = groupName;
    return this;
  }

  @Override
  public Item getResult() {
    return this.result;
  }

  @Override
  public void save(Consumer<FinishedRecipe> finishedRecipeConsumer, ResourceLocation recipeId) {
    this.ensureValid(recipeId);
    this.advancement.parent(new ResourceLocation("recipes/root"))
        .addCriterion("has_the_recipe", RecipeUnlockedTrigger.unlocked(recipeId))
        .rewards(AdvancementRewards.Builder.recipe(recipeId)).requirements(RequirementsStrategy.OR);
    finishedRecipeConsumer.accept(new Result(recipeId, this.ingredients, this.advancement,
                                             new ResourceLocation(recipeId.getNamespace(),
                                                                  "recipes/" + this.result.getItemCategory()
                                                                      .getRecipeFolderName() + "/" + recipeId.getPath())));
  }

  /**
   * Makes sure that this recipe is valid and obtainable.
   */
  private void ensureValid(ResourceLocation id) {
    if (this.advancement.getCriteria().isEmpty()) {
      throw new IllegalStateException("No way of obtaining recipe " + id);
    }
  }

  public static class Result implements FinishedRecipe {
    private final ResourceLocation id;
    private final List<Ingredient> ingredients;
    private final Advancement.Builder advancement;
    private final ResourceLocation advancementId;

    public Result(ResourceLocation arg, List<Ingredient> list, Advancement.Builder arg3, ResourceLocation arg4) {
      this.id = arg;
      this.ingredients = list;
      this.advancement = arg3;
      this.advancementId = arg4;
    }

    @Override
    public void serializeRecipeData(JsonObject json) {
      JsonArray jsonArray = new JsonArray();
      for (Ingredient ingredient : this.ingredients) {
        jsonArray.add(ingredient.toJson());
      }
      json.add("ingredients", jsonArray);
      json.addProperty("book", this.id.toString());
    }

    @Override
    public ResourceLocation getId() {
      return this.id;
    }

    @Override
    public RecipeSerializer<?> getType() {
      return ShapelessBookRecipe.SERIALIZER;
    }

    @Override
    @Nullable
    public JsonObject serializeAdvancement() {
      return this.advancement.serializeToJson();
    }

    @Override
    @Nullable
    public ResourceLocation getAdvancementId() {
      return this.advancementId;
    }
  }
}