package one.hbn.outvoted.forge.server.config;

import net.minecraftforge.common.ForgeConfigSpec;
import one.hbn.outvoted.server.config.OutvotedConfig;

import java.util.List;

public class ForgeOutvotedConfig extends OutvotedConfig {
  public static final ForgeConfigSpec.Builder COMMON_BUILDER = new ForgeConfigSpec.Builder();
  public static final ForgeConfigSpec.Builder CLIENT_BUILDER = new ForgeConfigSpec.Builder();

  public static ForgeConfigSpec COMMON_CONFIG;
  public static ForgeConfigSpec CLIENT_CONFIG;

  static {
    COMMON = new Common(COMMON_BUILDER);
    CLIENT = new Client(CLIENT_BUILDER);

    COMMON_CONFIG = COMMON_BUILDER.build();
    CLIENT_CONFIG = CLIENT_BUILDER.build();
  }

  public static class SpawnConfig extends OutvotedConfig.SpawnConfig {
    SpawnConfig(final ForgeConfigSpec.Builder builder, double maxHealth) {
      this(builder, maxHealth, null);
    }

    SpawnConfig(final ForgeConfigSpec.Builder builder, double maxHealth, List<? extends String> allowedBiomes) {
      this(builder, 0, maxHealth, allowedBiomes);
    }

    SpawnConfig(final ForgeConfigSpec.Builder builder, int spawnRate, double maxHealth,
                List<? extends String> allowedBiomes) {
      builder.push("spawn");
      this.spawnRate = builder.comment("Higher number corresponds to higher spawn chance, set to 0 to disable spawning")
          .defineInRange("spawn_rate", spawnRate, 0, Integer.MAX_VALUE);
      this.maxHealth = builder.comment("Override maximum mob health")
          .defineInRange("max_health", maxHealth, 0D, Double.POSITIVE_INFINITY);
      if (allowedBiomes == null)
        this.allowedBiomes = null;
      else
        this.allowedBiomes = builder.comment("Only allow spawning in these biomes; works with tags or paths")
            .defineList("biome_whitelist", allowedBiomes, b -> b instanceof String);
      builder.pop();
    }
  }

  public static class Wildfire extends OutvotedConfig.Wildfire {
    Wildfire(final ForgeConfigSpec.Builder builder) {
      builder.push("wildfire");
      spawnConfig = new SpawnConfig(builder, 1, 50D, List.of("minecraft:nether_wastes", "minecraft:basalt_deltas",
                                                             "minecraft:crimson_forest", "minecraft:soul_sand_valley"));

      builder.push("combat");

      fireballCount = builder.comment("Set the number of fireballs per attack")
          .defineInRange("fireball_count", 9, 1, Integer.MAX_VALUE);
      fireballOffsetAngle = builder.comment("fireball_offset_angle")
          .defineInRange("fireball_offset_angle", 4D, 1D, Double.POSITIVE_INFINITY);
      fireballMaxDepressAngle = builder.comment("fireball_max_depress")
          .defineInRange("fireball_max_depress", 50D, 1D, Double.POSITIVE_INFINITY);
      fireballExplode = builder.comment("Enable fireballs exploding on impact").define("fireball_explode", false);
      fireballExplodePower = builder.comment("Set the power of the explosion")
          .defineInRange("fireball_explode_power", 0.5D, 0D, 1D);
      builder.pop(2);
    }
  }

  public static class Glutton extends OutvotedConfig.Glutton {
    Glutton(final ForgeConfigSpec.Builder builder) {
      builder.push("glutton");
      spawnConfig = new SpawnConfig(builder, 5, 20D,
                                    List.of("#swamp", "#desert", "minecraft:badlands_plateau", "minecraft:badlands"));

      builder.push("mechanics");

      stealEnchants = builder.comment("Enable stealing enchantments on bite").define("steal_enchants", true);
      capEnchants = builder.comment(
              "Cap overenchants to 1 over vanilla max level, disable to have no upper limit on overenchants")
          .define("cap_enchantments", true);
      maxEnchants = builder.comment("Maximum number of enchantments to store").defineInRange("max_enchants", 5, 1, 100);

      builder.pop(2);
    }
  }

  public static class Barnacle extends OutvotedConfig.Barnacle {
    Barnacle(final ForgeConfigSpec.Builder builder) {
      builder.push("barnacle");
      spawnConfig = new SpawnConfig(builder, 2, 40D, List.of("minecraft:deep_warm_ocean", "minecraft:deep_ocean",
                                                             "minecraft:deep_cold_ocean",
                                                             "minecraft:deep_lukewarm_ocean"));

      builder.pop();
    }
  }

  public static class Meerkat extends OutvotedConfig.Meerkat {
    Meerkat(final ForgeConfigSpec.Builder builder) {
      builder.push("meerkat");
      spawnConfig = new SpawnConfig(builder, 10D, List.of("#desert"));

      burrowGenerationRate = builder.comment("Set the generation rate of burrows")
          .defineInRange("burrow_generation_rate", 40, 1, 100);

      builder.pop();
    }
  }

  public static class Ostrich extends OutvotedConfig.Ostrich {
    Ostrich(final ForgeConfigSpec.Builder builder) {
      builder.push("meerkat");
      spawnConfig = new SpawnConfig(builder, 2, 15D, List.of("#savanna"));

      builder.pop();
    }
  }

  public static class CopperGolem extends OutvotedConfig.CopperGolem {
    CopperGolem(final ForgeConfigSpec.Builder builder) {
      builder.push("copper_golem");
      spawnConfig = new SpawnConfig(builder, 25D);

      builder.push("mechanics");

      oxidationRate = builder.comment("Percentage chance for golem to oxidize per second, as a decimal")
          .defineInRange("oxidation_rate", 0.001D, 0D, 1D);

      builder.pop(2);
    }
  }

  public static class Glare extends OutvotedConfig.Glare {
    Glare(final ForgeConfigSpec.Builder builder) {
      builder.push("glare");
      spawnConfig = new SpawnConfig(builder, 10, 25D,
                                    List.of("#taiga", "#jungle", "#plains", "#savanna", "#forest", "#swamp",
                                            "#underground"));

      builder.push("mechanics");

      shouldInteract = builder.comment(
              "Enable/disable mob interacting with environment (picking up and placing blocks/items)")
          .define("should_interact", true);
      inventorySize = builder.comment("Max number of items that can be held")
          .defineInRange("inventory_size", 32, 1, 64);

      builder.pop(2);
    }
  }

  public static class Mobs extends OutvotedConfig.Mobs {
    Mobs(final ForgeConfigSpec.Builder builder) {
      builder.push("mobs");
      WILDFIRE = new Wildfire(builder);
      BARNACLE = new Barnacle(builder);
      GLUTTON = new Glutton(builder);
      COPPER_GOLEM = new CopperGolem(builder);
      GLARE = new Glare(builder);
      MEERKAT = new Meerkat(builder);
      OSTRICH = new Ostrich(builder);
      builder.pop();
    }
  }

  public static class Misc extends OutvotedConfig.Misc {
    Misc(final ForgeConfigSpec.Builder builder) {
      builder.push("misc");
      giveBook = builder.comment("Give Patchouli Book on login").define("give_book", true);
      helmetFireTicks = builder.comment("Helmet fire ticks")
          .defineInRange("helmet_fire_Ticks", 25, 0, Integer.MAX_VALUE);

      builder.pop();
    }
  }

  public static class Common extends OutvotedConfig.Common {
    public Common(final ForgeConfigSpec.Builder builder) {
      MOBS = new Mobs(builder);
      MISC = new Misc(builder);
    }
  }

  public static class Client extends OutvotedConfig.Client {
    public Client(final ForgeConfigSpec.Builder builder) {
      builder.push("mobs");

      variants = builder.comment("Enable Wildfire/Blaze soul variants").define("variants", true);

      builder.pop();
    }
  }
}
