package one.hbn.outvoted.forge.data.recipes;

import com.google.gson.JsonObject;
import net.minecraft.data.recipes.FinishedRecipe;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.crafting.RecipeSerializer;
import net.minecraft.world.item.crafting.SimpleRecipeSerializer;

import javax.annotation.Nullable;
import java.util.function.Consumer;

public class CustomSpecialRecipeBuilder {
  final SimpleRecipeSerializer<?> serializer;

  public CustomSpecialRecipeBuilder(SimpleRecipeSerializer<?> pSerializer) {
    this.serializer = pSerializer;
  }

  public static CustomSpecialRecipeBuilder special(SimpleRecipeSerializer<?> pSerializer) {
    return new CustomSpecialRecipeBuilder(pSerializer);
  }

  public void save(Consumer<FinishedRecipe> pFinishedRecipeConsumer, final ResourceLocation pId) {
    pFinishedRecipeConsumer.accept(new FinishedRecipe() {
      public void serializeRecipeData(JsonObject json) {
      }

      public ResourceLocation getId() {
        return pId;
      }

      public RecipeSerializer<?> getType() {
        return CustomSpecialRecipeBuilder.this.serializer;
      }

      @Nullable
      public JsonObject serializeAdvancement() {
        return null;
      }

      public ResourceLocation getAdvancementId() {
        return new ResourceLocation("");
      }
    });
  }
}