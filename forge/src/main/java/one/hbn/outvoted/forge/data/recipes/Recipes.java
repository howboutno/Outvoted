package one.hbn.outvoted.forge.data.recipes;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.recipes.*;
import net.minecraft.world.item.Items;
import net.minecraft.world.item.crafting.Ingredient;
import net.minecraft.world.level.block.Blocks;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.BlockRegister;
import one.hbn.outvoted.server.item.ItemRegister;
import one.hbn.outvoted.server.recipe.RecipeSerializerRegister;
import vazkii.patchouli.api.PatchouliAPI;

import java.util.function.Consumer;

public class Recipes extends RecipeProvider {
  public Recipes(DataGenerator generatorIn) {
    super(generatorIn);
  }

  @Override
  protected void buildCraftingRecipes(Consumer<FinishedRecipe> consumer) {
    UpgradeRecipeBuilder.smithing(Ingredient.of(Items.SHIELD), Ingredient.of(ItemRegister.WILDFIRE_SHIELD_PART.get()),
                                  ItemRegister.WILDFIRE_SHIELD.get())
        .unlocks("has_wildfire_part", has(ItemRegister.WILDFIRE_SHIELD_PART.get()))
        .save(consumer, ItemRegister.WILDFIRE_SHIELD.getId());
    ShapedRecipeBuilder.shaped(ItemRegister.WILDFIRE_SHIELD_PART.get()).pattern("MMM").pattern("MCM").pattern("MMM")
        .define('M', Blocks.POLISHED_BLACKSTONE).define('C', ItemRegister.WILDFIRE_PIECE.get())
        .unlockedBy("has_wildfire_piece", has(ItemRegister.WILDFIRE_PIECE.get()))
        .save(consumer, ItemRegister.WILDFIRE_SHIELD_PART.getId());
    ShapedRecipeBuilder.shaped(ItemRegister.PRISMARINE_ROD.get()).pattern("P").pattern("P").pattern("P")
        .define('P', Blocks.PRISMARINE_BRICKS).unlockedBy("has_prismarine", has(Blocks.PRISMARINE_BRICKS))
        .save(consumer);
    ShapedRecipeBuilder.shaped(Items.TRIDENT).pattern(" TT").pattern(" HT").pattern("R  ")
        .define('T', ItemRegister.BARNACLE_TOOTH.get()).define('H', Items.HEART_OF_THE_SEA)
        .define('R', ItemRegister.PRISMARINE_ROD.get()).unlockedBy("has_tooth", has(ItemRegister.BARNACLE_TOOTH.get()))
        .save(consumer, Constants.loc("trident"));
    CustomSpecialRecipeBuilder.special(RecipeSerializerRegister.REPAIR_COST.get())
        .save(consumer, RecipeSerializerRegister.REPAIR_COST.getId());
    CustomSpecialRecipeBuilder.special(RecipeSerializerRegister.SHIELD_DECO.get())
        .save(consumer, RecipeSerializerRegister.SHIELD_DECO.getId());
    ShapelessRecipeBuilder.shapeless(BlockRegister.COPPER_BUTTON.get()).requires(Items.COPPER_INGOT)
        .unlockedBy("has_copper", has(Items.COPPER_INGOT)).save(consumer, BlockRegister.COPPER_BUTTON.getId());
    ShapelessRecipeBuilder.shapeless(Items.BLAZE_POWDER, 4).requires(ItemRegister.WILDFIRE_PIECE.get())
        .unlockedBy("has_piece", has(ItemRegister.WILDFIRE_PIECE.get())).save(consumer, Constants.loc("blaze_powder"));

    //    ShapelessBookRecipeBuilder.book(Constants.loc("book"))
    //        .requires(Items.LAVA_BUCKET)
    //        .requires(Items.BOOK)
    //        .unlockedBy("has_book", has(Items.BOOK))
    //        .save(consumer, Constants.loc("book"));
        ShapelessRecipeBuilder.shapeless(PatchouliAPI.get().getBookStack(Constants.loc("tome")).getItem())
            .requires(Items.LAVA_BUCKET)
            .requires(Items.BOOK)
            .unlockedBy("has_book", has(Items.BOOK))
            .save(consumer, Constants.loc("tome"));
  }
}
