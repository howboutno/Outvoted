package one.hbn.outvoted.forge.data.models.blockstates;

import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraftforge.client.model.generators.BlockStateProvider;
import net.minecraftforge.common.data.ExistingFileHelper;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.block.BlockRegister;

public class BlockStates extends BlockStateProvider {
  public BlockStates(DataGenerator gen, ExistingFileHelper exFileHelper) {
    super(gen, Constants.MOD_ID, exFileHelper);
  }

  @Override
  protected void registerStatesAndModels() {
    // palm
    buttonBlock(BlockRegister.PALM_BUTTON.get(), Constants.loc("block/palm_planks"));
    doorBlock(BlockRegister.PALM_DOOR.get(), Constants.loc("block/palm_door_bottom"),
              Constants.loc("block/palm_door_top"));
    fenceGateBlock(BlockRegister.PALM_FENCE_GATE.get(), Constants.loc("block/palm_planks"));
    fenceBlock(BlockRegister.PALM_FENCE.get(), Constants.loc("block/palm_planks"));
    simpleBlock(BlockRegister.PALM_LEAVES.get());
    logBlock(BlockRegister.PALM_LOG.get());
    logBlock(BlockRegister.STRIPPED_PALM_LOG.get());
    axisBlock(BlockRegister.PALM_WOOD.get(), Constants.loc("block/palm_log"), Constants.loc("block/palm_log"));
    axisBlock(BlockRegister.STRIPPED_PALM_WOOD.get(), Constants.loc("block/stripped_palm_log"),
              Constants.loc("block/stripped_palm_log"));
    simpleBlock(BlockRegister.PALM_PLANKS.get());
    pressurePlateBlock(BlockRegister.PALM_PRESSURE_PLATE.get(), Constants.loc("block/palm_planks"));
    simpleBlock(BlockRegister.PALM_SAPLING.get(),
                this.models().cross("palm_sapling", Constants.loc("block/palm_sapling")));
    slabBlock(BlockRegister.PALM_SLAB.get(), Constants.loc("block/palm_planks"), Constants.loc("block/palm_planks"));
    stairsBlock(BlockRegister.PALM_STAIRS.get(), Constants.loc("block/palm_planks"));
    trapdoorBlock(BlockRegister.PALM_TRAPDOOR.get(), Constants.loc("block/palm_trapdoor"), true);
    signBlock(BlockRegister.PALM_SIGN.get(), BlockRegister.PALM_WALL_SIGN.get(), Constants.loc("block/palm_planks"));
    // baobab
    simpleBlock(BlockRegister.BAOBAB_LEAVES.get());
    logBlock(BlockRegister.BAOBAB_LOG.get());
    logBlock(BlockRegister.STRIPPED_BAOBAB_LOG.get());
    axisBlock(BlockRegister.BAOBAB_WOOD.get(), Constants.loc("block/baobab_log"), Constants.loc("block/baobab_log"));
    axisBlock(BlockRegister.STRIPPED_BAOBAB_WOOD.get(), Constants.loc("block/stripped_baobab_log"),
              Constants.loc("block/stripped_baobab_log"));
    simpleBlock(BlockRegister.BAOBAB_SAPLING.get(),
                this.models().cross("baobab_sapling", Constants.loc("block/baobab_sapling")));

    directionalBlock(BlockRegister.BURROW.get(), this.models()
        .orientableVertical("burrow", new ResourceLocation("block/sand"), Constants.loc("block/burrow")));
    buttonBlock(BlockRegister.COPPER_BUTTON.get(), new ResourceLocation("block/copper_block"));
    buttonBlock(BlockRegister.EXPOSED_COPPER_BUTTON.get(), new ResourceLocation("block/exposed_copper"));
    buttonBlock(BlockRegister.WEATHERED_COPPER_BUTTON.get(), new ResourceLocation("block/weathered_copper"));
    buttonBlock(BlockRegister.OXIDIZED_COPPER_BUTTON.get(), new ResourceLocation("block/oxidized_copper"));
    buttonBlock(BlockRegister.WAXED_COPPER_BUTTON.get(), new ResourceLocation("block/copper_block"));
    buttonBlock(BlockRegister.WAXED_EXPOSED_COPPER_BUTTON.get(), new ResourceLocation("block/exposed_copper"));
    buttonBlock(BlockRegister.WAXED_WEATHERED_COPPER_BUTTON.get(), new ResourceLocation("block/weathered_copper"));
    buttonBlock(BlockRegister.WAXED_OXIDIZED_COPPER_BUTTON.get(), new ResourceLocation("block/oxidized_copper"));
  }
}
