package one.hbn.outvoted.forge.data.tags;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.tags.ConfiguredStructureTagsProvider;
import net.minecraft.world.level.levelgen.structure.BuiltinStructures;
import net.minecraftforge.common.data.ExistingFileHelper;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.world.TagRegister;
import org.jetbrains.annotations.Nullable;

public class ConfiguredStructureTags extends ConfiguredStructureTagsProvider {
  public ConfiguredStructureTags(DataGenerator arg, @Nullable ExistingFileHelper existingFileHelper) {
    super(arg, Constants.MOD_ID, existingFileHelper);
  }

  @Override
  protected void addTags() {
    this.tag(TagRegister.IN_DESERT).add(BuiltinStructures.DESERT_PYRAMID).add(BuiltinStructures.VILLAGE_DESERT)
        .add(BuiltinStructures.BURIED_TREASURE).add(BuiltinStructures.RUINED_PORTAL_DESERT);
  }
}
