package one.hbn.outvoted.forge.data.models;

import net.minecraft.data.DataGenerator;
import net.minecraft.resources.ResourceLocation;
import net.minecraft.world.item.Item;
import net.minecraft.world.item.SpawnEggItem;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import net.minecraftforge.common.data.ExistingFileHelper;
import one.hbn.outvoted.Constants;
import one.hbn.outvoted.server.item.ItemRegister;

public class ItemModels extends ItemModelProvider {
  public ItemModels(DataGenerator generator, ExistingFileHelper existingFileHelper) {
    super(generator, Constants.MOD_ID, existingFileHelper);
  }

  @Override
  protected void registerModels() {
    wood("palm");
    tree("baobab");

    generatedItem("tome");
    generatedItem("baobab_fruit");
    generatedItem(ItemRegister.WILDFIRE_SHIELD_PART.get());
    generatedItem(ItemRegister.WILDFIRE_PIECE.get());
    generatedItem(ItemRegister.VOID_HEART.get());
    generatedItem(ItemRegister.BARNACLE_TOOTH.get());
    generatedItem(ItemRegister.PRISMARINE_ROD.get());
    egg(ItemRegister.WILDFIRE_SPAWN_EGG.get());
    egg(ItemRegister.GLUTTON_SPAWN_EGG.get());
    egg(ItemRegister.BARNACLE_SPAWN_EGG.get());
    egg(ItemRegister.GLARE_SPAWN_EGG.get());
    egg(ItemRegister.MEERKAT_SPAWN_EGG.get());
    egg(ItemRegister.OSTRICH_SPAWN_EGG.get());
    withExistingParent("burrow", Constants.loc("block/burrow"));
    withExistingParent("copper_button", Constants.loc("block/copper_button_inventory"));
    withExistingParent("exposed_copper_button", Constants.loc("block/exposed_copper_button_inventory"));
    withExistingParent("weathered_copper_button", Constants.loc("block/weathered_copper_button_inventory"));
    withExistingParent("oxidized_copper_button", Constants.loc("block/oxidized_copper_button_inventory"));
    withExistingParent("waxed_copper_button", Constants.loc("block/waxed_copper_button_inventory"));
    withExistingParent("waxed_exposed_copper_button", Constants.loc("block/waxed_exposed_copper_button_inventory"));
    withExistingParent("waxed_weathered_copper_button", Constants.loc("block/waxed_weathered_copper_button_inventory"));
    withExistingParent("waxed_oxidized_copper_button", Constants.loc("block/waxed_oxidized_copper_button_inventory"));

    singleTexture(ItemRegister.WILDFIRE_HELMET.getId().getPath(), new ResourceLocation("item/generated"), "layer0",
                  Constants.loc("item/wildfire_helmet")).override().predicate(Constants.loc("soul_texture"), 1)
        .model(new ModelFile(Constants.loc("item/wildfire_helmet_soul")) {
          @Override
          protected boolean exists() {
            return true;
          }
        });

    generatedItem(ItemRegister.WILDFIRE_HELMET.getId().getPath() + "_soul");

    singleTexture(ItemRegister.WILDFIRE_SHIELD.getId().getPath(), new ResourceLocation("item/shield"), "particles",
                  new ResourceLocation("block/nether_bricks")).override().predicate(new ResourceLocation("blocking"), 1)
        .model(new ModelFile(Constants.loc("item/wildfire_shield_blocking")) {
          @Override
          protected boolean exists() {
            return true;
          }
        });

    singleTexture(ItemRegister.WILDFIRE_SHIELD.getId().getPath() + "_blocking",
                  new ResourceLocation("item/shield_blocking"), "particles",
                  new ResourceLocation("block/nether_bricks"));
  }

  private void generatedItem(String path) {
    singleTexture(path, new ResourceLocation("item/generated"), "layer0", Constants.loc("item/" + path));
  }

  private void generatedBlock(String path) {
    singleTexture(path, new ResourceLocation("item/generated"), "layer0", Constants.loc("block/" + path));
  }

  private void generatedItem(Item item) {
    generatedItem(item.getRegistryName().getPath());
  }

  private void egg(SpawnEggItem obj) {
    String path = obj.getRegistryName().getPath();
    withExistingParent(path, new ResourceLocation("item/template_spawn_egg"));
  }

  private void wood(String path) {
    tree(path);
    generatedItem(path + "_door");
    withExistingParent(path + "_button", Constants.loc("block/" + path + "_button_inventory"));
    withExistingParent(path + "_fence", Constants.loc("block/" + path + "_fence_inventory"));
    withExistingParent(path + "_fence_gate", Constants.loc("block/" + path + "_fence_gate"));
    withExistingParent(path + "_planks", Constants.loc("block/" + path + "_planks"));
    withExistingParent(path + "_pressure_plate", Constants.loc("block/" + path + "_pressure_plate"));
    withExistingParent(path + "_slab", Constants.loc("block/" + path + "_slab"));
    withExistingParent(path + "_stairs", Constants.loc("block/" + path + "_stairs"));
    withExistingParent(path + "_trapdoor", Constants.loc("block/" + path + "_trapdoor_bottom"));
    singleTexture(path + "_sign", new ResourceLocation("item/generated"), "layer0",
                  new ResourceLocation("item/acacia_sign"));
    //        generatedItem(path + "_sign");
  }

  private void tree(String path) {
    generatedBlock(path + "_sapling");
    withExistingParent(path + "_leaves", Constants.loc("block/" + path + "_leaves"));
    withExistingParent(path + "_log", Constants.loc("block/" + path + "_log"));
    withExistingParent(path + "_wood", Constants.loc("block/" + path + "_wood"));
    withExistingParent("stripped_" + path + "_log", Constants.loc("block/stripped_" + path + "_log"));
    withExistingParent("stripped_" + path + "_wood", Constants.loc("block/stripped_" + path + "_wood"));
  }
}
