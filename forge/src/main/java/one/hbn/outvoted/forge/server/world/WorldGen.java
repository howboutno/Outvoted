package one.hbn.outvoted.forge.server.world;

import net.minecraft.core.Holder;
import net.minecraft.world.entity.EntityType;
import net.minecraft.world.level.biome.MobSpawnSettings;
import net.minecraft.world.level.levelgen.GenerationStep;
import net.minecraft.world.level.levelgen.placement.PlacedFeature;
import net.minecraftforge.event.world.BiomeLoadingEvent;
import net.minecraftforge.eventbus.api.EventPriority;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import one.hbn.outvoted.server.config.OutvotedConfig;
import one.hbn.outvoted.server.entity.EntityRegister;
import one.hbn.outvoted.server.world.feature.FeatureRegister;

import java.util.List;

@Mod.EventBusSubscriber(bus = Mod.EventBusSubscriber.Bus.FORGE)
public class WorldGen {
  @SubscribeEvent(priority = EventPriority.HIGH)
  public static void onBiomeLoading(final BiomeLoadingEvent event) {
    OutvotedConfig.Mobs mobConfig = OutvotedConfig.COMMON.MOBS;
    addSpawn(event, mobConfig.WILDFIRE.spawnConfig, EntityRegister.WILDFIRE.get(), 1, 1);
    addSpawn(event, mobConfig.GLUTTON.spawnConfig, EntityRegister.GLUTTON.get(), 1, 1);
    addSpawn(event, mobConfig.BARNACLE.spawnConfig, EntityRegister.BARNACLE.get(), 1, 1);
    addSpawn(event, mobConfig.GLARE.spawnConfig, EntityRegister.GLARE.get(), 1, 1);
    addSpawn(event, mobConfig.OSTRICH.spawnConfig, EntityRegister.OSTRICH.get(), 1, 3);

    switch (event.getCategory()) {
      case DESERT -> {
        addFeature(event, FeatureRegister.PLACED_BURROW.asHolder());
        addFeature(event, FeatureRegister.PLACED_PALM.asHolder());
      }
      case SAVANNA -> addFeature(event, FeatureRegister.PLACED_BAOBAB.asHolder());
    }
  }

  private static void addSpawn(final BiomeLoadingEvent event, OutvotedConfig.SpawnConfig spawnConfig,
                               EntityType<?> entityType, int j, int k) {
    if (spawnConfig.spawnRate.get() > 0 && validBiome(event, spawnConfig.allowedBiomes.get()))
      event.getSpawns().addSpawn(entityType.getCategory(),
                                 new MobSpawnSettings.SpawnerData(entityType, spawnConfig.spawnRate.get(), 1, k));
  }

  private static void addFeature(final BiomeLoadingEvent event, Holder<PlacedFeature> holder) {
    event.getGeneration().addFeature(GenerationStep.Decoration.VEGETAL_DECORATION, holder);
  }

  private static boolean validBiome(BiomeLoadingEvent biome, List<? extends String> filter) {
    if (biome.getName() == null)
      return false;
    return filter.contains(biome.getName().getPath()) || filter.contains("#" + biome.getCategory().getName());
  }
}